import React, {Component} from 'react';
import {
  Image,
  View,
  Text,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import AssetImages from '../../services/constants/asset_images';
import {inject, observer} from 'mobx-react';
import PushNotification from 'react-native-push-notification';
import ShortcutBadge from 'react-native-shortcut-badge';
import FirebaseConfig from '../../services/constants/firebase_config';
import firebase from 'react-native-firebase';
import {requestGetAPI} from '../../services/request_api';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    PushNotification.configure({
      onRegister: token => this.registerApp(token),
      onNotification: notif => this.setupPushComing(notif),
      largeIcon: 'ic_launcher',
      smallIcon: 'ic_notification',
      senderID: FirebaseConfig.FCM_SENDER_ID,
      popInitialNotification: true,
      requestPermissions: true,
    });
    //this.registerApp();
  }

  async registerApp(fcm_token) {
    // let fcm_token = await firebase.messaging().getToken();
    console.log('[FCM] token:::', fcm_token);
    //console.log('[] token:::', token.token);
    this.props.systemStore.api_header['X-PUSH-TOKEN'] = fcm_token;
    await requestGetAPI(
      this.props.systemStore.getApiHeader,
      'metadata/push_token',
    );
    this.props.systemAction.startApp();
  }

  setupPushComing(notif) {
    this.props.systemAction.callRosterApi();

    console.log('notifsssa', notif);
    ShortcutBadge.setCount(parseInt(notif.badge));
  }

  render() {
    const {is_connected_internet} = this.props.systemStore;

    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ImageBackground
          source={AssetImages.app_splash}
          resizeMode="cover"
          style={{
            alignItems: 'center',
            width: Dimensions.get('window').width,
            height: '100%',
          }}>
          <View style={style.containIndicator}>
            <Text style={{fontSize: 16}}>
              {is_connected_internet
                ? '通信中'
                : 'インターネット設定を確認して下さい。'}
            </Text>
            <Image style={style.image} source={AssetImages.straight_loader} />
          </View>
          <Text
            style={{
              position: 'absolute',
              left: 40,
              bottom: 35,
              color: 'black',
            }}>
            Version {DeviceInfo.getVersion()}
          </Text>
        </ImageBackground>
      </View>
    );
  }
}

const style = StyleSheet.create({
  containIndicator: {
    position: 'absolute',
    bottom: 30,
    left: 0,
    flexDirection: 'column',
    width: Dimensions.get('window').width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 25,
    resizeMode: 'contain',
  },
});

export default inject('systemStore', 'systemAction')(observer(LoginScreen));
