import React, {Component} from 'react';
import {FlatList} from 'react-native';
import {observer, inject} from 'mobx-react';
import {Container} from 'native-base';
import PointItem from '../../components/point_item';
import RNIap, {
  purchaseErrorListener,
  purchaseUpdatedListener,
} from 'react-native-iap';
import * as Helper from '../../../services/helpers';

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

class PointScreen extends Component {
  constructor(props) {
    super(props);

    props.navigation.addListener('willFocus', async () => {
      console.log(
        'last_screen',
        this.props.systemStore.nav_list.last_screen[0],
      );
      let isGoBack = this.props.systemStore.nav_list.last_screen[0] == 'MyPage';
      this.props.systemStore.header_app = {
        back_button: isGoBack,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'ポイント追加',
      };

      await this.props.pointAction.getPoint();
      await this.props.pointAction.checkAvailablePurchases();
    });

    props.navigation.addListener('willBlur', () => {});
  }

  async componentDidMount() {
    try {
      const result = await RNIap.initConnection();
      console.log('is connect:', result);
      let flushFailedPurchase = await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
      console.log(
        'flushFailedPurchasesCachedAsPendingAndroid::',
        flushFailedPurchase,
      );
    } catch (err) {
      console.log('connect to IAP err: ', err);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(async purchase => {
      console.log('purchaseUpdatedListener', purchase);
      /* * check order_id co ton tai hay ko
       * neu ko ton tai => NEW purchase
       * neu ton tai => OLD purchase, ko xu ly o day, function checkAvailablePurchases se xu ly
       * */

      let arrOrderID = await Helper.getArrOrderPaymentID();
      if (arrOrderID == null || !arrOrderID.includes(purchase.transactionId)) {
        this.props.socketIO.sendPaymentLog(
          `playstore respond purchase success: ${JSON.stringify(purchase)}`,
        );

        if (purchase.transactionReceipt) {
          await Helper.setArrOrderPaymentID(purchase.transactionId);
          this.props.pointAction.finishPurchase(purchase);
        }
      }
    });

    purchaseErrorSubscription = purchaseErrorListener(error => {
      this.props.socketIO.sendPaymentLog(
        `playstore respond purchase error: ${JSON.stringify(error)}`,
      );
      this.props.systemStore.loading = false;
      console.log('purchaseErrorListener', error);
    });
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  render() {
    const {getPointData} = this.props.pointStore;
    let data = getPointData.result || [];

    return (
      <Container>
        <FlatList
          style={{padding: 10}}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => {
            return <PointItem index={index} item={item} />;
          }}
        />
      </Container>
    );
  }
}

export default inject(
  'systemAction',
  'systemStore',
  'pointAction',
  'pointStore',
  'socketIO',
)(observer(PointScreen));
