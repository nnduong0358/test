import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  TextInput,
  Image,
  ScrollView,
  Alert,
  ActivityIndicator,
  Modal,
  TouchableWithoutFeedback,
} from 'react-native';
import {Button} from 'native-base';
import {inject, observer} from 'mobx-react';
import Icon from 'react-native-vector-icons/FontAwesome';
import RNPickerSelect from 'react-native-picker-select';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import NavigatorService from '../../services/navigator/navigation_services';
import AppConfig from '../../services/constants/app_config';
import AssetImage from '../../services/constants/asset_images';
import MyColor from '../../assets/MyColor';

let screenWidth = Dimensions.get('window').width;

class RegisterScreen extends Component {
  constructor(props) {
    super(props);

    this.state = this.props.systemStore.register_data;
    this.state.modal_change_device = false;
    this.state.email = '';
    this.state.password_change_device = '';

    this.state.disable_submit = false;
  }

  componentDidMount() {
    this.props.systemAction.resetLastScreen('Register');
  }

  onPickerAreaChange = value => {
    this.setState({
      area: value,
      city: '',
    });
    if (value === '') {
      this.setState({cities: []});
    } else {
      this.props.systemAction.getCities(value).then(cities_result => {
        this.setState({cities: cities_result});
      });
    }
  };

  onSubmitRegister = () => {
    this.setState({disable_submit: true});

    this.checkDataValid()
      .then(() => {
        // Transfer data to FormData and send to API Register
        this.props.systemAction.registerUser(
          {
            carriername: this.state.carriername,
            mobilenetworkcode: this.state.mobilenetworkcode,
            displayname: this.state.name,
            area: this.state.area,
            sex: this.state.gender,
            age: this.state.age,
            city: this.state.city,
            password: this.state.password,
          },
          this.props.headers,
        );
      })
      .catch(() => {
        this.setState({disable_submit: false});
        if (!this.state.checked_age_18) {
          Alert.alert(
            '',
            '１８歳以上ですか？',
            [{text: 'OK', onPress: () => console.log('OK')}],
            {cancelable: false},
          );
        } else if (!this.state.checked_term) {
          Alert.alert(
            '',
            '利用規約に同意する必要があります。',
            [{text: 'OK', onPress: () => console.log('OK')}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            '',
            '全ての項目を入力してください。',
            [{text: 'OK', onPress: () => console.log('OK')}],
            {cancelable: false},
          );
        }
      });
  };

  checkDataValid = () => {
    const states = this.state;

    return new Promise((resolve, reject) => {
      let is_valid;
      is_valid =
        states.name !== '' &&
        states.gender !== '' &&
        states.age !== '' &&
        states.area !== '' &&
        states.city !== '' &&
        states.checked_age_18 &&
        states.checked_term;
      return is_valid ? resolve(is_valid) : reject(is_valid);
    });
  };

  _transferChangeDevice = () => {
    this.setState({modal_change_device: false});
    this.props.systemAction.tranferChangeDevice(
      this.state.email,
      this.state.password_change_device,
    );
  };

  render() {
    const {server_state} = this.props.systemStore;

    let areas = server_state.user_profile_list.area;
    let area_keys = Object.keys(areas);
    let sex = server_state.user_profile_list.sex;
    let sex_keys = Object.keys(sex);
    let age = server_state.user_profile_list.age;
    let age_keys = Object.keys(age);

    return (
      <View style={{flex: 1, backgroundColor: MyColor.background_color}}>
        <Modal
          onRequestClose={() => {
            this.setState({
              modal_change_device: false,
              password_change_device: '',
              email: '',
            });
          }}
          transparent={true}
          visible={this.state.modal_change_device}>
          <TouchableWithoutFeedback onPress={dismissKeyboard}>
            <View
              style={{
                backgroundColor: 'rgba(85, 85, 85, 0.4)',
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  borderRadius: 10,
                  backgroundColor: 'white',
                  padding: 20,
                  flexDirection: 'column',
                  width: '90%',
                }}>
                <View style={{marginBottom: 10}}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontWeight: 'bold',
                      fontSize: 16,
                    }}>
                    {' '}
                    データ引継ぎ情報登録{' '}
                  </Text>
                </View>
                <View style={{marginTop: 10, marginBottom: 10}}>
                  <Text> メールアドレス </Text>
                </View>
                <View>
                  <TextInput
                    style={style.input}
                    underlineColorAndroid="transparent"
                    value={this.state.email}
                    keyboardType={'email-address'}
                    inputAccessoryViewID={'input'}
                    onChangeText={text => this.setState({email: text})}
                  />
                </View>
                <View style={{marginTop: 10, marginBottom: 10}}>
                  <Text> パスワード </Text>
                </View>
                <View>
                  <TextInput
                    style={style.input}
                    underlineColorAndroid="transparent"
                    value={this.state.password_change_device}
                    secureTextEntry={true}
                    inputAccessoryViewID={'input'}
                    onChangeText={text =>
                      this.setState({password_change_device: text})
                    }
                  />
                </View>
                <View style={style.buttonContainer}>
                  <Button
                    transparent
                    style={[
                      style.center,
                      {
                        borderRightColor: '#d9d9d9',
                        borderRightWidth: 0.5,
                        height: 55,
                        width: '50%',
                      },
                    ]}
                    disabled={
                      this.state.email == '' ||
                      this.state.password_change_device == ''
                    }
                    onPress={() => this._transferChangeDevice()}>
                    <Text style={[style.textButton]}>実行</Text>
                  </Button>
                  <Button
                    transparent
                    style={[style.center, {height: 55, width: '50%'}]}
                    onPress={() => {
                      this.setState({
                        modal_change_device: false,
                        password_change_device: '',
                        email: '',
                      });
                    }}>
                    <Text style={[style.textButton, {fontWeight: 'bold'}]}>
                      キャンセル
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <View style={[style.header_container, {position: 'relative'}]}>
          <Text style={{color: 'black', fontSize: 20, fontWeight: '600'}}>
            プロフィール登録
          </Text>
          <View
            style={{
              position: 'absolute',
              right: 0,
              top: 0,
              height: '100%',
              justifyContent: 'center',
            }}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({modal_change_device: true})}>
              <Image
                source={AssetImage.ic_setting}
                style={{height: '50%'}}
                resizeMode="contain"
              />
            </TouchableWithoutFeedback>
          </View>
        </View>

        <ScrollView>
          <View style={style.formInput}>
            <View
              style={{
                padding: 10,
                backgroundColor: MyColor.form_register_color,
              }}>
              <View style={[style.picker_container, {marginTop: 5}]}>
                <View style={style.left_content}>
                  <TextInput
                    style={style.textRight}
                    maxLength={150}
                    placeholder={'ニックネーム'}
                    placeholderTextColor={MyColor.secondary_color}
                    underlineColorAndroid="transparent"
                    value={this.state.name}
                    autoFocus={true}
                    onChangeText={text => this.setState({name: text})}
                    inputAccessoryViewID={'input'}
                  />
                </View>
                <View style={style.right_content} />
              </View>
              <View style={style.picker_container}>
                <View style={style.left_content}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '性別',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.state.gender}
                    onValueChange={value => this.setState({gender: value})}
                    items={sex_keys.map(value => {
                      return {
                        label: sex[value]['name'],
                        value: sex[value]['value'],
                        key: sex[value]['field_id'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={style.right_content}>
                  <Image
                    source={AssetImage.ic_arrow_right}
                    style={{height: '30%'}}
                    resizeMode="contain"
                  />
                </View>
              </View>

              <View style={style.picker_container}>
                <View style={style.left_content}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '年代',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.state.age}
                    onValueChange={value => this.setState({age: value})}
                    items={age_keys.map(value => {
                      return {
                        label: age[value]['name'],
                        value: age[value]['value'],
                        key: age[value]['field_id'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={style.right_content}>
                  <Image
                    source={AssetImage.ic_arrow_right}
                    style={{height: '30%'}}
                    resizeMode="contain"
                  />
                </View>
              </View>

              <View style={style.picker_container}>
                <View style={style.left_content}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '都道府県',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.state.area}
                    onValueChange={value => this.onPickerAreaChange(value)}
                    items={area_keys.map(value => {
                      return {
                        label: areas[value]['name'],
                        value: areas[value]['value'],
                        key: areas[value]['field_id'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={style.right_content}>
                  <Image
                    source={AssetImage.ic_arrow_right}
                    style={{height: '30%'}}
                    resizeMode="contain"
                  />
                </View>
              </View>
              <View style={style.picker_container}>
                <View style={style.left_content}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '市区町村',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.state.city}
                    onValueChange={value => this.setState({city: value})}
                    items={this.state.cities.map(value => {
                      return {
                        label: value['name'],
                        value: value['value'],
                        key: value['field_id'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={style.right_content}>
                  <Image
                    source={AssetImage.ic_arrow_right}
                    style={{height: '30%'}}
                    resizeMode="contain"
                  />
                </View>
              </View>
            </View>

            <View style={{paddingLeft: 15, marginTop: 30}}>
              <View style={[style.listItem]}>
                <View style={{marginRight: 10}}>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      this.setState({
                        checked_age_18: !this.state.checked_age_18,
                      });
                    }}>
                    {this.state.checked_age_18 ? (
                      <Icon
                        name="check-square"
                        color={MyColor.main_color}
                        size={30}
                      />
                    ) : (
                      <Icon name="square" color="#DBDBDB" size={30} />
                    )}
                  </TouchableWithoutFeedback>
                </View>
                <Text style={style.black}> 18歳以上です</Text>
              </View>

              <View style={[style.listItem]}>
                <View style={{marginRight: 15}}>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      this.setState({checked_term: !this.state.checked_term});
                    }}>
                    {this.state.checked_term ? (
                      <Icon
                        name="check-square"
                        color={MyColor.main_color}
                        size={30}
                      />
                    ) : (
                      <Icon name="square" color="#DBDBDB" size={30} />
                    )}
                  </TouchableWithoutFeedback>
                </View>
                <TouchableWithoutFeedback
                  onPress={() => {
                    NavigatorService.navigate('Webview', {
                      uri: AppConfig.CMS_URL + 'Terms-of-service',
                      title: '利用規約',
                    });
                  }}>
                  <View
                    style={{
                      borderBottomColor: MyColor.main_color,
                      borderBottomWidth: 1,
                      height: 20,
                      justifyContent: 'center',
                    }}>
                    <Text style={[style.black, {color: MyColor.main_color}]}>
                      利用規約
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <Text style={style.black}>に同意しますか？</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 60,
                justifyContent: 'center',
                padding: 15,
                marginTop: 15,
                marginBottom: 20,
              }}>
              {this.state.disable_submit ? (
                <ActivityIndicator size="large" color={MyColor.main_color} />
              ) : (
                <View style={{width: '100%', height: 50, overflow: 'hidden'}}>
                  <Button
                    transparent
                    style={{width: '100%', height: 50}}
                    onPress={this.onSubmitRegister}>
                    <View
                      style={{height: 50, width: '100%', overflow: 'hidden'}}>
                      <View style={style.background_image_button}>
                        <Text style={style.button_text}>
                          プロフィールを登録する
                        </Text>
                      </View>
                    </View>
                  </Button>
                </View>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const pickerStyle = {
  inputAndroid: {
    height: '100%',
    width: (screenWidth / 6) * 5,
    backgroundColor: 'transparent',
    fontSize: 16,
    color: 'black',
    marginLeft: 25,
    fontWeight: 'bold',
  },
  inputIOS: {
    width: '100%',
    height: '100%',
    fontSize: 18,
    color: MyColor.secondary_color,
    fontWeight: 'bold',
    marginLeft: 15,
  },
  iconContainer: {
    top: 9,
    right: 15,
  },
};

const style = StyleSheet.create({
  formInput: {
    flex: 90,
  },
  input: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#dddddd',
    height: 45,
    color: 'black',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  black: {
    color: MyColor.secondary_color,
    fontSize: 15,
    fontWeight: 'bold',
  },
  textRight: {
    textAlign: 'left',
    height: '100%',
    width: '100%',
    paddingLeft: 15,
    backgroundColor: 'white',
    padding: 10,
    color: 'black',
    fontSize: 18,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  listItem: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  picker_container: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    height: 55,
    width: '100%',
    marginTop: 10,
    marginHorizontal: 5,
    alignSelf: 'center',
    borderRadius: 8,
  },
  background_image: {
    flexDirection: 'row',
    width: '100%',
    height: '100%',
    backgroundColor: '#FFFFFF',
  },
  background_image_button: {
    height: '100%',
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: MyColor.main_color,
  },
  button_text: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  header_container: {
    backgroundColor: '#F9F9F9',
    alignItems: 'center',
    justifyContent: 'center',
    height: 55,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: 'grey',
  },
  buttonContainer: {
    marginTop: 10,
    flexDirection: 'row',
    height: 55,
    borderTopWidth: 0.5,
    borderTopColor: '#d9d9d9',
  },
  textButton: {
    color: '#1971BA',
  },
  left_content: {
    flex: 85,
    justifyContent: 'center',
    alignItems: 'center',
  },
  right_content: {
    flex: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default inject('systemStore', 'systemAction')(observer(RegisterScreen));
