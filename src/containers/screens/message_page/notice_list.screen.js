import React, {Component} from 'react';
import {FlatList, Text, RefreshControl, StyleSheet} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Container} from 'native-base';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import NoticeItem from '../../components/notice_item';
import MyColor from '../../../assets/MyColor';

const ITEM_HEIGHT = 80;

class NoticeListScreen extends Component {
  constructor(props) {
    super(props);

    this.itemLayout = (data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    });

    this.emptyList = () => {
      return <Text>お知らせはありません</Text>;
    };

    // go in page
    props.navigation.addListener('willFocus', () => {
      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());

      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: true,
        refresh_action: events,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'お知らせ',
      };

      this.props.socketIO.current_action = events;
      this._onRefresh();
    });
  }

  _onRefresh() {
    this.props.messageStore.notice_list.page = -1;
    this.props.messageStore.notice_list.no_more_data = false;
    this.props.messageStore.notice_list.data = [];
    this._getData();
  }

  _getData() {
    this.props.messageAction.getNoticeList();
  }

  render() {
    const styleEmptyList = this.props.messageStore.getNoticeData.length
      ? null
      : style.styleEmptyList;

    return (
      <Container>
        {this.props.systemStore.loading &&
        !this.props.messageStore.getNoticeData.length ? null : (
          <FlatList
            style={{flex: 1}}
            data={this.props.messageStore.getNoticeData}
            renderItem={({item, index}) => (
              <NoticeItem item={item} index={index} />
            )}
            keyExtractor={(item, index) =>
              index.toString() + item.id.toString()
            }
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={() => this._onRefresh()}
                title="引っ張って更新"
                tintColor={MyColor.main_color}
                titleColor={MyColor.main_color}
              />
            }
            onEndReachedThreshold={0.1}
            onEndReached={() => this._getData()}
            getItemLayout={null}
            contentContainerStyle={styleEmptyList}
            ListEmptyComponent={this.emptyList}
          />
        )}
      </Container>
    );
  }
}

export default inject(
  'systemStore',
  'messageAction',
  'messageStore',
  'socketIO',
)(observer(NoticeListScreen));

const style = StyleSheet.create({
  styleEmptyList: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
});
