import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  FlatList,
  TouchableWithoutFeedback,
  Alert,
  Keyboard,
  PermissionsAndroid,
  TextInput,
  AppState,
} from 'react-native';
import {Container, Button} from 'native-base';
import {observer, inject} from 'mobx-react';
import ReportPopup from '../../components/popupReportUser';
import PopupActionSheet from '../../components/popupActionSheet';
import MapPopup from '../../components/mapModal';
import ImagePopup from '../../components/imageModal';
import ImagePicker from 'react-native-image-picker';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import ChatBubbleItem from '../../components/chat_bubble_item';
import AppConfig from '../../../services/constants/app_config';
import * as Progress from 'react-native-progress';
import AndroidOpenSettings from 'react-native-android-open-settings';
import Geolocation from '@react-native-community/geolocation';
import * as ShowAlert from '../../../services/show_alert';
import MyColor from '../../../assets/MyColor';
import AssetsImages from '../../../services/constants/asset_images';

class ChattingScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      current_offset_chatting_list: 0,
      message_input: '',
    };

    // go in page
    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,
        open_profile_button: this.props.navigation.state.params.show_btn_profile
          ? true
          : false,
        open_profile_action: this.props.navigation.state.params.show_btn_profile
          ? this._viewProfile
          : undefined,
        title: this.props.navigation.state.params.username,
      };

      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());
      this.props.socketIO.current_action = events;
      this.props.messageStore.is_in_chatting = true;
      if (this.props.navigation.getParam('reload')) this._onRefresh();

      // set params popup actionsheet
      this.props.userStore.list_report_popup = {
        user_code: this.props.messageStore.getChattingUser.user_code,
        id: this.props.messageStore.getChattingUser.id,
        no_block: this.props.messageStore.getChattingUser.id,
        screen: 'chat',
        keijiban_id: '',
      };
    });

    // leave page
    props.navigation.addListener('willBlur', () => {
      this.props.socketIO.typingMessage(
        this.props.messageStore.getChattingUser.id,
        0,
      );
      this.props.messageStore.message_chatting_list.no_more_history = false;
      this.props.messageStore.upload_image_process = {
        process: 0,
        show: false,
      };
      this.setState({
        message_input: '',
      });
      this.props.messageStore.is_in_chatting = false;
    });
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (nextAppState == 'active') {
      this.props.systemStore.app_state_active = true;
      if (this.props.messageStore.is_in_chatting) {
        this._onRefresh();
      }
    } else {
      this.props.systemStore.app_state_active = false;
    }
  };

  _changeText = text => {
    if (text == '') {
      // clear text -> set typing = false
      this.props.socketIO.typingMessage(
        this.props.messageStore.getChattingUser.id,
        0,
      );
    } else {
      // begin typing
      this.props.socketIO.typingMessage(
        this.props.messageStore.getChattingUser.id,
        1,
      );
    }

    this.setState({message_input: text});
  };

  _sendMessage = () => {
    if (
      this.props.pointAction.checkUserEnoughPoint('chat_message_text') ||
      this.props.userStore.user_profile.unlimit_point == 1 ||
      this.props.systemStore.server_state.supporter.id ==
        this.props.messageStore.getChattingUser.id ||
      this.props.userStore.user_profile.free_chat == '1'
    ) {
      this.props.messageAction.sendMessage(this.state.message_input.trim());
      this.setState({message_input: ''});

      this.props.socketIO.typingMessage(
        this.props.messageStore.getChattingUser.id,
        0,
      );

      if (this.props.messageStore.getChattingList.length)
        this.chattingList.scrollToIndex({animated: true, index: 0});
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  };

  _viewProfile = () => {
    if (this.props.pointAction.checkUserEnoughPoint('view_profile')) {
      this.props.userAction.getUserProfile(
        this.props.messageStore.getChattingUser.id,
      );
      this.props.systemAction.saveLastScreen('Chatting');
      this.props.messageStore.message_chatting_list.last_msg = null;
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  };

  _handleScrollChattingList = event => {
    this.setState({
      current_offset_chatting_list: event.nativeEvent.contentOffset.y,
    });
    if (event.nativeEvent.contentOffset.y < 10)
      this.props.messageStore.new_message_comming = false;
  };

  _onRefresh = () => {
    this.props.messageStore.message_chatting_list.no_more_history = false;
    this.props.messageAction.loadMessageHistory(
      this.props.messageStore.getChattingUser.user_code,
      this.props.messageStore.getChattingUser.id,
    );
  };

  _loadMoreMessageHistory = () => {
    this.props.messageAction.loadMessageHistory(
      this.props.messageStore.getChattingUser.user_code,
      this.props.messageStore.getChattingUser.id,
      this.props.messageStore.getChattingList[
        this.props.messageStore.getChattingList.length - 1
      ].msg_id,
    );
  };

  _sendImageMessage = async () => {
    if (
      this.props.pointAction.checkUserEnoughPoint('chat_media_image') ||
      this.props.systemStore.server_state.supporter.id ==
        this.props.messageStore.getChattingUser.id
    ) {
      let permission = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: '画像の読み取りが無効です',
          message:
            '画像の読み取りが無効です。設定から' +
            AppConfig.APP_NAME +
            'を開き、「写真」を「読み出しと書き込み」に変更することで、端末内の画像を送信することができます。',
        },
      );
      if (permission === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.launchImageLibrary(
          {
            rotation: 360,
          },
          response => {
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('Image picker not have permission');
            } else {
              this.props.messageAction.sendMessageImage(response);
            }
          },
        );
      } else {
        ShowAlert.alertRequestPermissionPhotos();
      }
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  };

  _sendLocationMessage = () => {
    console.log('[screen] _sendLocationMessage');
    if (this.props.pointAction.checkUserEnoughPoint('chat_media_location')) {
      this.props.messageStore.can_send_message = false;

      Geolocation.getCurrentPosition(
        position => {
          this.props.messageAction.sendMessageLocation(
            position.coords.latitude,
            position.coords.longitude,
          );
        },
        error => {
          console.log('error location permission:::', error);
          Keyboard.dismiss();
          this.props.messageStore.can_send_message = true;

          if (error.code == 1) {
            Alert.alert(
              '位置情報サービスがオフです',
              '\n位置情報サービスが無効です。設定から' +
                AppConfig.APP_NAME +
                'を開き、「位置情報」を「常に許可」または「使用中のみ許可」に変更することで、ユーザーの位置情報を受け取ることができます。',
              [
                {
                  text: '設定',
                  onPress: () => AndroidOpenSettings.appDetailsSettings(),
                },
                {
                  text: 'OK',
                  onPress: () => console.log('Close'),
                  style: 'cancel',
                },
              ],
              {cancelable: true},
            );
          } else {
            Alert.alert(
              '',
              '位置情報の設定画面で「Google 位置情報の精度」をONにして下さい。',
              [
                {
                  text: '設定を開く',
                  onPress: () => AndroidOpenSettings.locationSourceSettings(),
                },
                {
                  text: 'キャンセル',
                  onPress: () => console.log('Close'),
                  style: 'cancel',
                },
              ],
              {cancelable: true},
            );
          }
        },
        {enableHighAccuracy: false, timeout: 5000, maximumAge: 10000},
      );
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  };

  _showAlertBuyUnlimitPoint = async () => {
    await this.props.pointAction.getPointFee();
    if (this.props.pointAction.checkUserEnoughPoint('unlimit_point')) {
      Keyboard.dismiss();
      Alert.alert(
        AppConfig.APP_NAME,
        '\n' + this.props.pointStore.unlimit_description,
        [
          {
            text: 'はい',
            onPress: () => {
              this.props.pointAction.postUnlimitPoint(
                this.props.messageStore.getChattingUser.id,
                this.props.messageStore.getChattingUser.user_code,
              );
            },
          },
          {
            text: 'いいえ',
            onPress: () => console.log('Cancel'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  };

  render() {
    return (
      <Container style={{backgroundColor: 'white', height: '100%'}}>
        <ReportPopup />
        <PopupActionSheet />
        {this.props.systemStore.map_popup.show ? <MapPopup /> : null}
        <ImagePopup />
        {this.props.systemStore.server_state.supporter.id ==
        this.props.messageStore.getChattingUser.id ? null : (
          <View style={[style.content_button_header]}>
            <Button
              onPress={() => {
                if (this.props.userStore.user_profile.favorite_status == 1) {
                  ShowAlert.favoriteStatus();
                } else {
                  this.props.userAction.postFavorite(
                    this.props.messageStore.getChattingUser.id,
                    this.props.messageStore.getChattingUser.user_code,
                  );
                }
              }}
              style={{
                flex: 3,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
                borderColor: MyColor.secondary_color,
                borderWidth: 1,
                backgroundColor: 'white',
                height: 45,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  color: MyColor.secondary_color,
                  fontWeight: 'bold',
                }}>
                お気に入り登録
              </Text>
            </Button>
            <Button
              onPress={this._showAlertBuyUnlimitPoint}
              style={{
                flex: 3,
                height: 45,
                borderColor: MyColor.secondary_color,
                borderWidth: 1,
                borderLeftWidth: 0,
                backgroundColor: 'white',
                justifyContent: 'center',
              }}
              disabled={this.props.userStore.user_profile.unlimit_point == 1}>
              <Text
                style={{
                  fontSize: 14,
                  color: MyColor.secondary_color,
                  fontWeight: 'bold',
                }}>
                リミット解除
              </Text>
            </Button>
            <Button
              onPress={() => (this.props.userStore.popup_action_sheet = true)}
              style={{
                flex: 3,
                height: 45,
                borderBottomRightRadius: 8,
                borderTopRightRadius: 8,
                borderWidth: 1,
                borderLeftWidth: 0,
                borderColor: MyColor.secondary_color,
                backgroundColor: 'white',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  color: MyColor.secondary_color,
                  fontWeight: 'bold',
                }}>
                通報
              </Text>
            </Button>
          </View>
        )}
        {this.props.messageStore.upload_image_process.show ? (
          <Progress.Bar
            progress={this.props.messageStore.upload_image_process.process}
            color={MyColor.main_color}
            unfilledColor="#cccccc"
            width={screenWidth}
            height={3}
            style={{width: screenWidth, height: 3}}
          />
        ) : null}
        <View style={style.chat_container}>
          <FlatList
            style={{flexGrow: 0, overflow: 'visible'}}
            onScroll={this._handleScrollChattingList}
            ref={c => {
              this.chattingList = c;
            }}
            showsVerticalScrollIndicator={false}
            data={this.props.messageStore.getChattingList}
            keyExtractor={(item, index) =>
              index.toString() + item.time.toString()
            }
            renderItem={({item, index}) => (
              <ChatBubbleItem
                item={item}
                index={index}
                avatar_url={this.props.navigation.getParam('avatar_url')}
              />
            )}
            onEndReachedThreshold={0.1}
            onEndReached={() => this._loadMoreMessageHistory()}
            inverted
          />
          {this.props.messageStore.isTargetUserTyping ? (
            <View style={style.typing}>
              <Text style={style.message_text}>タイピング中。。。</Text>
            </View>
          ) : null}
          {this.props.messageStore.new_message_comming &&
          this.state.current_offset_chatting_list > 10 ? (
            <TouchableWithoutFeedback
              onPress={() =>
                this.chattingList.scrollToIndex({animated: true, index: 0})
              }>
              <View style={style.new_message_comming}>
                <Text style={[style.message_text, {fontSize: 16}]}>
                  新しいメッセージ
                </Text>
              </View>
            </TouchableWithoutFeedback>
          ) : null}
        </View>
        <View style={style.message_container}>
          {this.props.socketIO.no_connection_server ? (
            // bi mat ket noi socket thi khong cho nguoi dung chat va gui message
            <View style={[style.message_content, {justifyContent: 'center'}]}>
              <Text>インターネット接続がありません。</Text>
            </View>
          ) : (
            <View style={style.message_content}>
              <Button
                transparent
                style={[style.camera_content, {marginRight: 5}]}
                onPress={() => this._sendImageMessage()}
                disabled={
                  this.props.userStore.user_profile.id == undefined ||
                  this.props.messageStore.upload_image_process.show ||
                  !this.props.messageStore.can_send_message
                }>
                <Image
                  style={style.message_icon}
                  resizeMode="contain"
                  source={AssetsImages.ic_image}
                />
              </Button>

              <Button
                transparent
                disabled={
                  this.props.userStore.user_profile.id == undefined ||
                  !this.props.messageStore.can_send_message
                }
                style={[
                  style.camera_content,
                  this.props.systemStore.server_state.supporter.id ==
                  this.props.messageStore.getChattingUser.id
                    ? {display: 'none'}
                    : {marginRight: 5},
                ]}
                onPress={() => this._sendLocationMessage()}>
                <Image
                  style={style.message_icon}
                  resizeMode="contain"
                  source={AssetsImages.ic_location}
                />
              </Button>

              <View style={style.message_input_content}>
                <TextInput
                  onChangeText={text => this._changeText(text)}
                  style={style.message_input}
                  multiline={true}
                  placeholder="メッセージを送信する"
                  ref={ref => {
                    this.input = ref;
                  }}
                  editable={this.props.userStore.user_profile.id != undefined}
                  underlineColorAndroid="transparent"
                  value={this.state.message_input}
                  inputAccessoryViewID={'input'}
                />
              </View>

              <Button
                onPress={() => this._sendMessage()}
                transparent
                style={style.content_item}
                disabled={
                  this.state.message_input.trim() == '' ||
                  !this.props.messageStore.can_send_message
                }>
                <Text
                  style={{color: 'white', fontSize: 14, fontWeight: 'bold'}}>
                  送信
                </Text>
              </Button>
            </View>
          )}
        </View>
      </Container>
    );
  }
}

export default inject(
  'systemStore',
  'systemAction',
  'userStore',
  'userAction',
  'messageStore',
  'messageAction',
  'pointStore',
  'pointAction',
  'socketIO',
)(observer(ChattingScreen));

let screenWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  content_button_header: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: MyColor.secondary_color,
    zIndex: 8,
  },
  message_icon: {
    height: 20,
    width: 20,
  },
  message_container: {
    height: 60,
    width: '100%',
    justifyContent: 'center',
    borderTopWidth: 1.5,
    borderColor: MyColor.secondary_color,
    backgroundColor: 'white',
  },
  message_content: {
    flexDirection: 'row',
    alignItems: 'center',
    height: '100%',
    paddingHorizontal: 5,
  },
  camera_content: {
    height: 40,
    justifyContent: 'center',
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: MyColor.main_color,
  },
  message_input_content: {
    flex: 1,
    marginRight: 5,
  },
  message_input: {
    backgroundColor: '#F4F4F4',
    fontSize: 14,
    height: 45,
    color: 'black',
    paddingLeft: 5,
    paddingTop: 12,
  },
  content_item: {
    justifyContent: 'center',
    height: 40,
    borderRadius: 5,
    paddingHorizontal: 10,
    width: 50,
    backgroundColor: MyColor.main_color,
  },
  chat_container: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    position: 'relative',
  },
  message_text: {
    color: 'white',
    fontSize: 13,
  },
  typing: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    padding: 5,
    backgroundColor: 'rgba(224, 93, 148, 1)',
  },
  new_message_comming: {
    position: 'absolute',
    bottom: 30,
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    alignSelf: 'center',
    borderRadius: 20,
    backgroundColor: 'rgba(224, 93, 148, 1)',
  },
});
