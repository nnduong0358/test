import React, {Component} from 'react';
import {
  FlatList,
  View,
  Text,
  AppState,
  Image,
  RefreshControl,
  StyleSheet,
  TouchableHighlight,
  Dimensions,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Container} from 'native-base';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import RosterItem from '../../components/roster_item';
import PushNotification from 'react-native-push-notification';
import Swipeout from 'react-native-swipeout';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AndroidOpenSettings from 'react-native-android-open-settings';
import MyColor from '../../../assets/MyColor';

const screenWidth = Dimensions.get('window').width;
const ITEM_HEIGHT = 80;

class RosterListScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasNotifPermission: true,
      rowIndex: null,
      scrollEnabled: true,
    };

    this.emptyList = () => {
      return <Text>メッセージはありません</Text>;
    };

    this.itemLayout = (data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    });

    props.navigation.addListener('willFocus', () => {
      this._checkNotificationsPermission();
      AppState.addEventListener('change', this._checkNotificationsPermission);

      // pass event to other component
      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());

      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: true,
        refresh_action: events,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'チャット',
      };

      this.props.socketIO.current_action = events;
      this._onRefresh();
    });

    props.navigation.addListener('willBlur', () => {
      AppState.removeEventListener(
        'change',
        this._checkNotificationsPermission,
      );
    });
  }

  _checkNotificationsPermission = () => {
    // Check permission receive notification
    PushNotification.checkPermissions(permission => {
      this.setState({hasNotifPermission: permission.alert});
    });
  };

  _onRefresh() {
    this.props.messageStore.roster_list.page = -1;
    this.props.messageStore.roster_list.no_more_data = false;
    this.props.messageStore.roster_list.data = [];
    this._getData();
  }

  _getData() {
    this.props.messageAction.getRosterList();
  }

  _openSetting = () => {
    AndroidOpenSettings.appNotificationSettings();
  };

  _onSwipeOpen(rowIndex) {
    this.setState({
      rowIndex: rowIndex,
    });
  }

  _onSwipeClose(rowIndex) {
    if (rowIndex === this.state.rowIndex) {
      this.setState({rowIndex: null});
    }
  }

  render() {
    const {server_state} = this.props.systemStore;
    const styleEmptyList = this.props.messageStore.getRosterData.length
      ? null
      : style.styleEmptyList;

    return (
      <Container style={{backgroundColor: 'white'}}>
        {this.state.hasNotifPermission ? null : (
          <TouchableHighlight
            underlayColor={'rgba(255, 255, 255, 0)'}
            onPress={() => this._openSetting()}>
            <View
              style={{
                flexDirection: 'row',
                borderTopWidth: 1.5,
                borderColor: '#d3d3d3',
                backgroundColor: 'white',
                paddingVertical: 10,
              }}>
              <View style={{alignItems: 'center', paddingHorizontal: 5}}>
                {server_state.push_off_image ? (
                  <Image
                    resizeMode="contain"
                    style={{
                      width: screenWidth / 5,
                      height: screenWidth / 5,
                      backgroundColor: 'transparent',
                    }}
                    source={{uri: server_state.push_off_image}}
                  />
                ) : null}
              </View>
              <View
                style={{
                  paddingHorizontal: 5,
                  flexDirection: 'column',
                  width: '70%',
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    color: MyColor.secondary_color,
                  }}>
                  {server_state.push_off_title}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    color: MyColor.secondary_color,
                  }}>
                  {server_state.push_off_content}
                </Text>
              </View>
            </View>
          </TouchableHighlight>
        )}

        {this.props.systemStore.loading &&
        !this.props.messageStore.getRosterData.length ? null : (
          <View style={{flex: 1, borderTopWidth: 1.5, borderColor: '#d3d3d3'}}>
            <FlatList
              data={this.props.messageStore.getRosterData}
              scrollEnabled={this.state.scrollEnabled}
              keyExtractor={(item, index) =>
                index.toString() + item.user_id.toString() + item.user_code
              }
              renderItem={({item, index}) => {
                return (
                  <View>
                    {server_state.supporter.id == item.user_id ? (
                      <Swipeout
                        scroll={scrollEnabled => this.setState({scrollEnabled})}
                        autoClose={true}
                        right={[
                          {
                            onPress: () =>
                              this.props.messageAction.deleteRoster(
                                item.user_id,
                              ),
                            component: (
                              <View style={style.containSwipeButton}>
                                <Icon size={26} name="delete" />
                              </View>
                            ),
                            type: 'delete',
                          },
                        ]}
                        onOpen={() => this._onSwipeOpen(index)}
                        close={this.state.rowIndex !== index}
                        onClose={() => this._onSwipeClose(index)}>
                        <RosterItem index={index} item={item} />
                      </Swipeout>
                    ) : (
                      <Swipeout
                        scroll={scrollEnabled => this.setState({scrollEnabled})}
                        autoClose={true}
                        right={[
                          {
                            onPress: () =>
                              this.props.messageAction.pinRoster(
                                item.user_id,
                                item.is_pin_chat,
                              ),
                            component:
                              item.is_pin_chat != 0 ? (
                                <View style={style.containSwipeButton}>
                                  <Icon size={26} name="pin-off" />
                                </View>
                              ) : (
                                <View style={style.containSwipeButton}>
                                  <Icon size={26} name="pin" />
                                </View>
                              ),
                          },
                          {
                            onPress: () =>
                              this.props.messageAction.deleteRoster(
                                item.user_id,
                              ),
                            component: (
                              <View style={style.containSwipeButton}>
                                <Icon size={26} name="delete" />
                              </View>
                            ),
                            type: 'delete',
                          },
                        ]}
                        onOpen={() => this._onSwipeOpen(index)}
                        close={this.state.rowIndex !== index}
                        onClose={() => this._onSwipeClose(index)}>
                        <RosterItem index={index} item={item} />
                      </Swipeout>
                    )}
                  </View>
                );
              }}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => {
                    this._onRefresh();
                  }}
                  title="引っ張って更新"
                  tintColor={MyColor.main_color}
                  titleColor={MyColor.main_color}
                />
              }
              onEndReachedThreshold={0.1}
              onEndReached={() => this._getData()}
              getItemLayout={null}
              contentContainerStyle={styleEmptyList}
              ListEmptyComponent={this.emptyList}
            />
          </View>
        )}
      </Container>
    );
  }
}

const style = StyleSheet.create({
  styleEmptyList: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
  containSwipeButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default inject(
  'systemStore',
  'messageAction',
  'messageStore',
  'socketIO',
)(observer(RosterListScreen));
