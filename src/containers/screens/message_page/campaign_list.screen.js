import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, RefreshControl, View} from 'react-native';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import {observer, inject} from 'mobx-react';
import CampaignItem from '../../components/campaign_item';
import {Container} from 'native-base';
import MyColor from '../../../assets/MyColor';

const ITEM_HEIGHT = 66;

class CampaignListScreen extends Component {
  constructor(props) {
    super(props);

    this.itemLayout = (data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    });

    this.emptyList = () => {
      return <Text>開催中のキャンペーンはありません</Text>;
    };

    props.navigation.addListener('willFocus', () => {
      // Pass event to other component
      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());

      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: true,
        refresh_action: events,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'メッセージ',
      };

      this.props.socketIO.current_action = events;
      this._onRefresh();
    });
  }

  _onRefresh = () => {
    this.props.messageStore.campaign_list.page = -1;
    this.props.messageStore.campaign_list.no_more_data = false;
    this.props.messageStore.campaign_list.data = [];
    this._getData();
  };

  _getData = () => {
    this.props.messageAction.getCampaignList();
  };

  render() {
    const styleEmptyList = this.props.messageStore.getCampaignData.length
      ? null
      : style.styleEmptyList;

    return (
      <Container style={{backgroundColor: 'white'}}>
        <View style={{flex: 1}}>
          {this.props.systemStore.loading &&
          !this.props.messageStore.getCampaignData.length ? null : (
            <FlatList
              style={{flex: 1}}
              data={this.props.messageStore.getCampaignData}
              keyExtractor={(item, index) => item.id}
              renderItem={({item, index}) => {
                return (
                  <CampaignItem
                    index={index}
                    item={item}
                    navigation={this.props.navigation}
                  />
                );
              }}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => {
                    this._onRefresh();
                  }}
                  title="引っ張って更新"
                  tintColor={MyColor.main_color}
                  titleColor={MyColor.main_color}
                />
              }
              getItemLayout={null}
              onEndReachedThreshold={0.1}
              onEndReached={() => this._getData()}
              contentContainerStyle={styleEmptyList}
              ListEmptyComponent={this.emptyList}
            />
          )}
        </View>
      </Container>
    );
  }
}

export default inject(
  'systemStore',
  'messageAction',
  'messageStore',
  'socketIO',
)(observer(CampaignListScreen));

const style = StyleSheet.create({
  styleEmptyList: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
});
