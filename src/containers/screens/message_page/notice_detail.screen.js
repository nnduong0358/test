import React, {Component} from 'react';
import {
  ScrollView,
  Dimensions,
  StyleSheet,
  View,
  Image,
  Text,
} from 'react-native';
import {Container} from 'native-base';
import {observer, inject} from 'mobx-react';
import MyColor from '../../../assets/MyColor';

class NoticeDetailScreen extends Component {
  constructor(props) {
    super(props);

    // go in page
    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'お知らせ',
      };
    });
  }

  render() {
    let detail = this.props.navigation.getParam('detail');
    let image = JSON.parse(detail.images);

    return (
      <Container
        style={{borderTopColor: MyColor.secondary_color, borderTopWidth: 1}}>
        <ScrollView style={style.message_scroll_view}>
          <View style={style.chat_container}>
            <View style={style.chat_block}>
              <View style={style.avatar_content}>
                {image[0].path ? (
                  <Image
                    style={style.footer_avatar}
                    source={{uri: image[0].path}}
                  />
                ) : null}
              </View>
              <View style={style.flexStart}>
                {/* <Text>{detail.created_at + ' ' + detail.display_name}</Text> */}
                <View style={style.arrow} />
                <View style={style.message}>
                  <Text style={style.message_text}>{detail.content}</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default inject('systemStore')(observer(NoticeDetailScreen));

let screenWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  message_scroll_view: {
    marginBottom: 45,
  },
  chat_container: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 7,
  },
  footer_avatar: {
    height: screenWidth / 9,
    width: screenWidth / 9,
    borderRadius: screenWidth / 9 / 2,
  },
  chat_block: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  avatar_content: {
    marginRight: 10,
  },
  message: {
    maxWidth: screenWidth / 1.5,
    borderRadius: 8,
    padding: 5,
    marginTop: 10,
    marginLeft: 7,
    position: 'relative',
    zIndex: 1,
    backgroundColor: '#FFFDCE',
    paddingLeft: 10,
  },
  message_text: {
    color: 'black',
    fontSize: 15,
    lineHeight: parseInt(15 * 1.5),
  },
  arrow: {
    height: 14,
    width: 14,
    transform: [{rotateX: '45deg'}, {rotateZ: '0.785398rad'}],
    position: 'relative',
    zIndex: 8,
    marginBottom: -20,
    backgroundColor: '#FFFDCE',
    top: 12,
  },
  flexStart: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
});
