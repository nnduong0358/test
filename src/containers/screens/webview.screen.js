import React, {Component} from 'react';
import {ActivityIndicator, View} from 'react-native';
import {inject, observer} from 'mobx-react';
import WebView from 'react-native-webview';
import MyColor from '../../assets/MyColor';
import AppConfig from '../../services/constants/app_config';

class WebViewScreen extends Component {
  constructor(props) {
    super(props);

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: this.props.navigation.getParam('title')
          ? this.props.navigation.getParam('title')
          : AppConfig.APP_NAME,
      };
    });
  }

  renderLoadingView = () => {
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <ActivityIndicator size="large" color={MyColor.main_color} />
      </View>
    );
  };

  render() {
    return (
      <WebView
        key={this.props.navigation.getParam('uri')}
        source={{uri: this.props.navigation.getParam('uri')}}
        renderLoading={this.renderLoadingView}
        startInLoadingState={true}
      />
    );
  }
}

export default inject('systemStore')(observer(WebViewScreen));
