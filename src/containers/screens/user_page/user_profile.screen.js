import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  Dimensions,
  ScrollView,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Container, Button} from 'native-base';
import ReportPopup from '../../components/popupReportUser';
import ImagePopup from '../../components/imageModal';
import PopupActionSheet from '../../components/popupActionSheet';
import * as ShowAlert from '../../../services/show_alert';
import MyColor from '../../../assets/MyColor';
import AssetImage from '../../../services/constants/asset_images.js';
import AppConfig from '../../../services/constants/app_config';
import {checkCacheImage} from '../../../services/helpers';

const width_screen = Dimensions.get('window').width;

class UserProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      position_image: 0,
      images: [],
    };

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: true,
        message_action: this._onpenChattingPage,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: this.props.userStore.user_profile.display_name,
      };

      this.getCacheImages();

      this.props.userStore.list_report_popup = {
        user_code: this.props.userStore.user_profile.user_code,
        id: this.props.userStore.user_profile.id,
        no_block: this.props.userStore.user_profile.id,
        screen: 'profile',
        keijiban_id: '',
      };
    });

    props.navigation.addListener('willBlur', () => {
      this.setState({position_image: 0, images: []});
    });
  }

  async getCacheImages() {
    this.setState({
      images: [
        await checkCacheImage(this.props.userStore.user_profile.image[0].path),
        await checkCacheImage(this.props.userStore.user_profile.image[1].path),
        await checkCacheImage(this.props.userStore.user_profile.image[2].path),
      ],
    });
  }

  _onpenChattingPage = () => {
    this.props.systemAction.saveLastScreen('UserProfile');
    this.props.messageAction.openChatting(
      this.props.userStore.user_profile.id,
      this.props.userStore.user_profile.user_code,
      this.props.userStore.user_profile.display_name,
      this.props.userStore.user_profile.image[0].path,
    );
  };

  _showAlertBuyUnlimitPoint = async () => {
    await this.props.pointAction.getPointFee();
    if (this.props.pointAction.checkUserEnoughPoint('unlimit_point')) {
      Alert.alert(
        AppConfig.APP_NAME,
        '\n' + this.props.pointStore.unlimit_description,
        [
          {
            text: 'はい',
            onPress: () => {
              this.props.pointAction.postUnlimitPoint(
                this.props.userStore.user_profile.id,
                this.props.userStore.user_profile.user_code,
              );
            },
          },
          {
            text: 'いいえ',
            onPress: () => console.log('Cancel'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  };

  render() {
    const {user_profile} = this.props.userStore;
    const {server_state} = this.props.systemStore;
    console.log('render');

    return (
      <Container style={{backgroundColor: 'white'}}>
        <PopupActionSheet />
        <ReportPopup />
        <ImagePopup />

        <View style={[style.content_button_header]}>
          <Button
            onPress={() => {
              if (user_profile.favorite_status == 1) {
                ShowAlert.favoriteStatus();
              } else {
                this.props.userAction.postFavorite(
                  user_profile.id,
                  user_profile.user_code,
                );
              }
            }}
            style={{
              flex: 3,
              borderTopLeftRadius: 8,
              borderBottomLeftRadius: 8,
              borderColor: MyColor.secondary_color,
              borderWidth: 1,
              backgroundColor: 'white',
              height: 45,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 14,
                color: MyColor.secondary_color,
                fontWeight: 'bold',
              }}>
              お気に入り登録
            </Text>
          </Button>
          <Button
            onPress={this._showAlertBuyUnlimitPoint}
            style={{
              flex: 3,
              height: 45,
              borderColor: MyColor.secondary_color,
              borderWidth: 1,
              borderLeftWidth: 0,
              backgroundColor: 'white',
              justifyContent: 'center',
            }}
            disabled={user_profile.unlimit_point == 1}>
            <Text
              style={{
                fontSize: 14,
                color: MyColor.secondary_color,
                fontWeight: 'bold',
              }}>
              リミット解除
            </Text>
          </Button>
          <Button
            onPress={() => (this.props.userStore.popup_action_sheet = true)}
            style={{
              flex: 3,
              height: 45,
              borderBottomRightRadius: 8,
              borderTopRightRadius: 8,
              borderWidth: 1,
              borderLeftWidth: 0,
              borderColor: MyColor.secondary_color,
              backgroundColor: 'white',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 14,
                color: MyColor.secondary_color,
                fontWeight: 'bold',
              }}>
              通報
            </Text>
          </Button>
        </View>

        <ScrollView ref={c => (this.scroll = c)}>
          <TouchableWithoutFeedback
            onPress={() =>
              this.props.userAction.viewImageProfile(
                this.state.position_image,
                this.state.images[this.state.position_image],
              )
            }>
            <View
              style={{
                flexDirection: 'row',
                width: width_screen,
                height: width_screen,
                position: 'relative',
              }}>
              <Image
                style={{width: '100%', height: '100%'}}
                defaultSource={
                  user_profile.sex == '0'
                    ? AssetImage.default_man
                    : AssetImage.default_woman
                }
                source={{uri: this.state.images[this.state.position_image]}}
                resizeMode="cover"
              />

              <View
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                  padding: 10,
                  backgroundColor: 'rgba(0, 0, 0, 0.7)',
                  flexDirection: 'column',
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>
                  {user_profile.display_name || ''}
                </Text>
                <Text style={{color: 'white', fontWeight: 'bold'}}>
                  {user_profile.area_name || '未設定'} |{' '}
                  {user_profile.age
                    ? server_state.user_profile_list.age[user_profile.age].name
                    : '未設定'}
                </Text>
                <Text style={{color: 'white', fontWeight: 'bold'}}>
                  {user_profile.user_status || 'よろしくお願いします。'}
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>

          <View
            style={{
              paddingVertical: 5,
              flexDirection: 'row',
              height: width_screen / 3 - 4,
            }}>
            {this.state.images.map((item, index) => {
              return (
                <TouchableWithoutFeedback
                  key={index}
                  onPress={() => this.setState({position_image: index})}>
                  <View
                    style={[
                      style.thumnail_box,
                      this.state.position_image == index
                        ? {borderColor: MyColor.main_color}
                        : {borderColor: 'white'},
                      index == 0
                        ? {marginLeft: 3}
                        : index == 2
                        ? {marginRight: 3}
                        : null,
                    ]}>
                    <Image
                      style={{width: '100%', height: '100%'}}
                      defaultSource={
                        user_profile.sex == '0'
                          ? AssetImage.default_man
                          : AssetImage.default_woman
                      }
                      source={{uri: item}}
                      resizeMode="cover"
                    />
                  </View>
                </TouchableWithoutFeedback>
              );
            })}
          </View>

          <View style={style.user_container}>
            <View style={style.header_label}>
              <Text style={style.text_label}>詳細プロフィール</Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>ニックネーム</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.display_name
                  ? user_profile.display_name
                  : '未設定'}
              </Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>性別</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.sex
                  ? server_state.user_profile_list.sex[user_profile.sex].name
                  : '未設定'}
              </Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>年代</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.age
                  ? server_state.user_profile_list.age[user_profile.age].name
                  : '未設定'}
              </Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>登録地域</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.area_name ? user_profile.area_name : '未設定'}
                {user_profile.city_name ? user_profile.city_name : '未設定'}
              </Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>身長</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.height
                  ? server_state.user_profile_list.height[user_profile.height]
                      .name
                  : '未設定'}
              </Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>体型</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.style
                  ? server_state.user_profile_list.style[user_profile.style]
                      .name
                  : '未設定'}
              </Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>職業</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.job
                  ? server_state.user_profile_list.job[user_profile.job].name
                  : '未設定'}
              </Text>
            </View>
            <View style={style.info_content}>
              <Text style={style.info_text}>年収</Text>
              <Text
                style={[
                  style.info_text,
                  {color: MyColor.main_color, fontWeight: '600'},
                ]}>
                {user_profile.income
                  ? server_state.user_profile_list.income[user_profile.income]
                      .name
                  : '未設定'}
              </Text>
            </View>

            <View style={style.content_button}>
              <Button
                onPress={() => this._onpenChattingPage()}
                style={{
                  borderRadius: 30,
                  width: '85%',
                  height: 55,
                  backgroundColor: MyColor.main_color,
                  justifyContent: 'center',
                }}>
                <Image
                  source={AssetImage.ic_message}
                  style={{height: 20, width: 28}}
                  resizeMode="contain"
                />
                <Text style={style.text_button}>メッセージを送る</Text>
              </Button>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default inject(
  'systemAction',
  'systemStore',
  'userAction',
  'userStore',
  'pointAction',
  'messageAction',
  'pointStore',
)(observer(UserProfile));

const style = StyleSheet.create({
  header_label: {
    width: '100%',
    padding: 10,
    borderBottomColor: MyColor.secondary_color,
    borderBottomWidth: 1,
  },
  text_label: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 14,
  },
  user_container: {
    flexDirection: 'column',
  },
  info_content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    paddingLeft: 20,
  },
  info_text: {
    fontSize: 16,
    color: MyColor.secondary_color,
    fontWeight: 'bold',
    flex: 5,
  },
  content_button: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
  },
  text_button: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold',
  },
  content_button_header: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomWidth: 1.5,
    borderBottomColor: MyColor.form_register_color,
  },
  thumnail_box: {
    borderWidth: 3,
    width: width_screen / 3 - 4,
    height: width_screen / 3 - 4,
    marginHorizontal: 1.5,
  },
});
