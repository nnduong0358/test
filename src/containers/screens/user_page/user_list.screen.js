import React, {Component} from 'react';
import {
  FlatList,
  RefreshControl,
  Dimensions,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import UserItem from '../../components/user_item';
import PopupReportUser from '../../components/popupReportUser';
import {Container, Button} from 'native-base';
import MyColor from '../../../assets/MyColor';

const screenWidth = Dimensions.get('window').width;
const ITEM_HEIGHT = screenWidth / 4.2 + 60;

class UserListScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list: false,
      numColumns: 4,
      random_key: new Date().getTime(),
    };

    this.itemLayout = (data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    });

    this.emptyList = () => {
      return <Text>検索結果０件です</Text>;
    };

    props.navigation.addListener('willFocus', () => {
      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());
      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: true,
        refresh_action: events,
        search_button: true,
        search_action: 'user_search',
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: this.state.list ? 'リストで表示' : '写真で検索',
      };

      this.props.socketIO.current_action = events;
      this._onRefresh();
      this.props.systemStore.nav_list.current_screen = 'UserList';
    });

    props.navigation.addListener('willBlur', () => {
      this.props.userStore.user_list_condition.result_find = false;

      this.props.socketIO.current_action = undefined;
      this.props.systemStore.nav_list.current_screen = '';
    });
  }

  _onRefresh() {
    this.props.userStore.user_list_data = [];
    this.props.userStore.user_list_condition.page = 0;
    this.props.userStore.user_list_condition.no_more_data = false;
    this._getData();
  }

  _getData() {
    if (this.props.userStore.user_list_condition.result_find) {
      this.props.userAction.getUserList('search');
    } else {
      this.props.userAction.getUserList('all');
    }
  }

  renderItem = ({item, index}) => {
    let check = this.props.userStore.array_id_block_list.indexOf(item.id);

    if (check === -1)
      return this.state.list ? (
        <UserItem item={item} index={index} is_list={true} />
      ) : (
        <UserItem item={item} index={index} is_list={false} />
      );
    else {
      this.props.userStore.user_list_data.splice(index, 1);
      return null;
    }
  };

  render() {
    const {loading} = this.props.systemStore;
    let {getUserListData} = this.props.userStore;

    const styleEmptyList = getUserListData.length ? null : style.styleEmptyList;

    return (
      <Container style={{backgroundColor: MyColor.background_color}}>
        <PopupReportUser />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={style.navigation_header}>
            <View style={[style.container_block, {alignItems: 'flex-end'}]}>
              <View style={[style.container_button, {width: '90%'}]}>
                <Button
                  transparent
                  style={style.container_button}
                  onPress={() => {
                    this.props.systemStore.header_app.title = '写真で検索';
                    this.setState({
                      list: false,
                      numColumns: 4,
                      random_key: new Date().getTime(),
                    });
                  }}>
                  <View style={style.container_button}>
                    <View
                      style={[
                        style.background_button,
                        !this.state.list
                          ? {backgroundColor: MyColor.main_color}
                          : {backgroundColor: 'white'},
                        {
                          borderTopRightRadius: 0,
                          borderBottomRightRadius: 0,
                          borderRightWidth: 0.5,
                        },
                      ]}>
                      <Text
                        style={[
                          style.button_text,
                          !this.state.list
                            ? {}
                            : {color: MyColor.secondary_color},
                        ]}>
                        写真で表示
                      </Text>
                    </View>
                  </View>
                </Button>
              </View>
            </View>
            <View style={[style.container_block, {alignItems: 'flex-start'}]}>
              <View style={[style.container_button, {width: '90%'}]}>
                <Button
                  transparent
                  style={style.container_button}
                  onPress={() => {
                    this.props.systemStore.header_app.title = 'リストで表示';
                    this.setState({
                      list: true,
                      numColumns: 1,
                      random_key: new Date().getTime(),
                    });
                  }}>
                  <View style={style.container_button}>
                    <View
                      style={[
                        style.background_button,
                        this.state.list
                          ? {backgroundColor: MyColor.main_color}
                          : {backgroundColor: 'white'},
                        {
                          borderTopLeftRadius: 0,
                          borderBottomLeftRadius: 0,
                          borderLeftWidth: 0.5,
                        },
                      ]}>
                      <Text
                        style={[
                          style.button_text,
                          this.state.list
                            ? {}
                            : {color: MyColor.secondary_color},
                        ]}>
                        リストで表示
                      </Text>
                    </View>
                  </View>
                </Button>
              </View>
            </View>
          </View>

          {loading && !getUserListData.length ? null : (
            <FlatList
              style={{flex: 1}}
              key={this.state.random_key}
              data={getUserListData}
              renderItem={this.renderItem}
              keyExtractor={(item, index) => index.toString() + item.user_code}
              initialNumToRender={32}
              numColumns={this.state.numColumns}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this._onRefresh()}
                  title="引っ張って更新"
                  tintColor={MyColor.main_color}
                  titleColor={MyColor.main_color}
                />
              }
              onEndReached={() => this._getData()}
              onEndReachedThreshold={0.1}
              getItemLayout={null}
              contentContainerStyle={styleEmptyList}
              ListEmptyComponent={this.emptyList}
            />
          )}
        </View>
      </Container>
    );
  }
}

const style = StyleSheet.create({
  styleEmptyList: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
  navigation_header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  container_block: {
    flex: 5,
  },
  container_button: {
    height: 40,
    width: '100%',
  },
  background_button: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: MyColor.secondary_color,
    borderRadius: 8,
  },
  button_text: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default inject(
  'systemAction',
  'systemStore',
  'userAction',
  'userStore',
  'socketIO',
)(observer(UserListScreen));
