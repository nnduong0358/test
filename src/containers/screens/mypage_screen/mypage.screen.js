import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  Dimensions,
  ScrollView,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Container, Button} from 'native-base';
import MyColor from '../../../assets/MyColor';
import AssetImages from '../../../services/constants/asset_images';
import NavigatorService from '../../../services/navigator/navigation_services';
import AppConfig from '../../../services/constants/app_config';

const width_screen = Dimensions.get('window').width;

class MyPageScreen extends Component {
  constructor(props) {
    super(props);

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'マイページ',
      };
      this.props.myPageAction.getMyPage();
    });

    props.navigation.addListener('willBlur', () => {});
  }

  render() {
    const {getMyProfile} = this.props.myPageStore;
    const {server_state} = this.props.systemStore;
    console.log('getMyProfile', getMyProfile);

    return (
      <Container>
        <ScrollView>
          <View
            style={{
              flexDirection: 'row',
              width: width_screen,
              height: width_screen * 0.95,
              position: 'relative',
              borderTopColor: MyColor.secondary_color,
              borderTopWidth: 1,
            }}>
            {getMyProfile.image ? (
              <Image
                style={{width: '100%', height: '100%'}}
                defaultSource={
                  getMyProfile.sex == '0'
                    ? AssetImages.default_man
                    : AssetImages.default_woman
                }
                source={{
                  uri: getMyProfile.image[0].path,
                }}
                resizeMode="contain"
              />
            ) : null}

            <View
              style={{
                position: 'absolute',
                bottom: 0,
                width: '100%',
                padding: 10,
                backgroundColor: 'rgba(0, 0, 0, 0.7)',
                flexDirection: 'column',
              }}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                {getMyProfile.display_name || ''}
              </Text>
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                {getMyProfile.area_name || '未設定'} |{' '}
                {getMyProfile.age
                  ? server_state.user_profile_list.age[getMyProfile.age].name
                  : '未設定'}
              </Text>
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                {getMyProfile.user_status || 'よろしくお願いします。'}
              </Text>
            </View>
          </View>

          <View
            style={{
              paddingVertical: 10,
              paddingHorizontal: 5,
              alignItems: 'center',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.text_point}>所持ポイント </Text>
              <Text style={[styles.text_point, {color: MyColor.main_color}]}>
                {getMyProfile.point}
              </Text>
              <Text style={styles.text_point}> ポイント </Text>
            </View>

            <View style={styles.containButton}>
              <View style={[styles.button, {paddingRight: 5}]}>
                <Button
                  transparent
                  onPress={() => {
                    this.props.systemAction.resetLastScreen('MyPage');
                    NavigatorService.navigate('EditProfile');
                  }}>
                  <View style={{height: 45, width: '100%', overflow: 'hidden'}}>
                    <View style={styles.background_button}>
                      <Text style={styles.text_button}>プロフ編集</Text>
                    </View>
                  </View>
                </Button>
              </View>
              <View style={[styles.button, {paddingLeft: 5}]}>
                <Button
                  transparent
                  onPress={() => {
                    this.props.systemAction.resetLastScreen('MyPage');
                    NavigatorService.navigate('PointScreen');
                  }}>
                  <View style={{height: 45, width: '100%', overflow: 'hidden'}}>
                    <View style={styles.background_button}>
                      <Text style={styles.text_button}>ポイント購入</Text>
                    </View>
                  </View>
                </Button>
              </View>
            </View>
          </View>

          <View
            style={[
              styles.content,
              {paddingHorizontal: 0, flexDirection: 'column'},
            ]}>
            <View style={styles.viewSetting}>
              <Button
                style={styles.buttonSetting}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('SettingScreen');
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_setting}
                />
              </Button>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('PointTable');
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_point_table}
                />
              </Button>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('Favoritefootprint', {
                    favorite: true,
                  });
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_favorite}
                />
              </Button>
            </View>

            <View style={styles.viewSetting}>
              <Button
                style={styles.buttonSetting}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('Favoritefootprint', {
                    favorite: false,
                  });
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_footprint}
                />
              </Button>
              <Button
                style={styles.buttonSetting}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('Notice');
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_notice}
                />
              </Button>
              <Button
                style={styles.buttonSetting}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('Campaign');
                }}>
                <Image
                  style={[styles.iconButton]}
                  resizeMode="contain"
                  source={AssetImages.btn_campaign}
                />
              </Button>
            </View>

            <View style={styles.viewSetting}>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('TransferDevice');
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_transfer_device}
                />
              </Button>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('Webview', {
                    uri: AppConfig.CMS_URL + 'privacy_policy',
                    title: 'プライバシーポリシー',
                  });
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_privacy}
                />
              </Button>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('QaScreen');
                  NavigatorService.navigate('Webview', {
                    uri: AppConfig.CMS_URL + 'help_page',
                    title: 'よくある質問',
                  });
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_QA}
                />
              </Button>
            </View>

            <View style={styles.viewSetting}>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('QaScreen');
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_contact}
                />
              </Button>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('Webview', {
                    uri: AppConfig.CMS_URL + 'company_page',
                    title: '会社概要',
                  });
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_company}
                />
              </Button>
              <Button
                style={[styles.buttonSetting]}
                onPress={() => {
                  this.props.systemAction.resetLastScreen('MyPage');
                  NavigatorService.navigate('Webview', {
                    uri: AppConfig.CMS_URL + 'Terms-of-service',
                    title: '利用規約',
                  });
                }}>
                <Image
                  style={styles.iconButton}
                  resizeMode="contain"
                  source={AssetImages.btn_ToS}
                />
              </Button>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default inject(
  'systemAction',
  'systemStore',
  'myPageAction',
  'myPageStore',
  'pointStore',
)(observer(MyPageScreen));

const styles = StyleSheet.create({
  text_point: {color: 'black', fontSize: 14, fontWeight: '700'},
  containButton: {
    height: 50,
    flexDirection: 'row',
    paddingHorizontal: 8,
  },
  button: {
    flex: 5,
    paddingTop: 20,
    height: 45,
  },
  background_button: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: MyColor.main_color,
  },
  text_button: {
    marginLeft: 5,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
  content: {
    flexDirection: 'row',
    marginVertical: 20,
  },
  viewSetting: {
    flexDirection: 'row',
  },
  buttonSetting: {
    padding: 0,
    margin: 0,
    backgroundColor: 'white',
    width: width_screen / 3,
    height: (width_screen / 3 / 9) * 7,
  },
  iconButton: {
    width: '100%',
  },
});
