import React, {Component} from 'react';
import {View, TextInput, ScrollView, Text, StyleSheet} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Container, Button} from 'native-base';
import MyColor from '../../../assets/MyColor';

class TransferDeviceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'データ引き継ぎ情報登録',
      };
    });

    props.navigation.addListener('willBlur', () => {});
  }

  _changeDevice = () => {
    console.log('prepare change device:::', this.state);
    this.props.systemAction.prepareChangeDevice(this.state);
  };

  render() {
    return (
      <Container style={{backgroundColor: 'white'}}>
        <ScrollView>
          <View style={{padding: 15, paddingHorizontal: 0}}>
            <View style={styles.container}>
              <View style={styles.header_container}>
                <Text style={styles.textStyle}>
                  メールアドレス、パスワードをご登録頂きますと機種変更された際に現在のデータを継続してご利用できます。
                  {'\n'}
                  引継ぎを行った際は現在のアカウント情報は削除され、使用できなくなります。
                  {'\n'}
                  ※パスワードご登録時は8文字以上で大文字を含むアルファベット、数字の入力が必要となります。
                </Text>
              </View>
              <View style={styles.note_container}>
                <Text style={styles.textStyle}>メールアドレス</Text>
              </View>

              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                value={this.state.email}
                keyboardType={'email-address'}
                inputAccessoryViewID={'input'}
                onChangeText={text => this.setState({email: text})}
              />

              <View style={styles.note_container}>
                <Text style={styles.textStyle}>パスワード</Text>
              </View>

              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                value={this.state.password}
                secureTextEntry={true}
                inputAccessoryViewID={'input'}
                onChangeText={text => this.setState({password: text})}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 50,
                justifyContent: 'center',
                marginTop: 30,
                marginBottom: 20,
              }}>
              <View style={{width: '65%', height: 50, overflow: 'hidden'}}>
                <Button
                  transparent
                  style={{height: 50, width: '100%'}}
                  disabled={this.state.email == '' || this.state.password == ''}
                  onPress={() => {
                    this._changeDevice();
                  }}>
                  <View style={{height: 50, width: '100%', overflow: 'hidden'}}>
                    <View style={styles.background_image_button}>
                      <Text style={styles.button_text}>実行</Text>
                    </View>
                  </View>
                </Button>
              </View>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default inject('systemStore', 'systemAction')(
  observer(TransferDeviceScreen),
);

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  header_container: {
    marginBottom: 15,
  },
  note_container: {
    marginTop: 20,
    marginBottom: 20,
  },
  input: {
    borderWidth: 1.5,
    borderColor: MyColor.secondary_color,
    height: 45,
    backgroundColor: 'white',
    color: 'black',
    paddingLeft: 8,
    borderRadius: 8,
  },
  button_container: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 25,
  },
  background_image: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DF007D',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#D6D6D6',
  },
  text: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
  },
  background_image_button: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: MyColor.main_color,
  },
  button_text: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  textStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: MyColor.secondary_color,
  },
});
