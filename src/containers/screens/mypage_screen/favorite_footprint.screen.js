import React, {Component} from 'react';
import {FlatList, View, Text, StyleSheet, RefreshControl} from 'react-native';
import {inject, observer} from 'mobx-react';
import FavoriteItem from '../../components/favorite_footprint_item';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import {Container, Button} from 'native-base';
import MyColor from '../../../assets/MyColor';

const ITEM_HEIGHT = 66;

class FavoritefootprintScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      favorite: true,
    };

    this.itemLayout = (data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    });

    this.emptyList = () => {
      return (
        <Text>
          {this.props.navigation.getParam('favorite')
            ? 'まだお気に入りはいません。'
            : '足跡はありません'}
        </Text>
      );
    };

    props.navigation.addListener('willFocus', () => {
      if (this.props.navigation.getParam('favorite') != null) {
        this.setState({
          favorite: this.props.navigation.getParam('favorite'),
        });
      }

      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: this.props.navigation.getParam('favorite')
          ? 'お気に入り'
          : '足あと',
      };

      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());
      this.props.socketIO.current_action = events;
      this._onRefresh();
    });

    props.navigation.addListener('willBlur', () => {
      this.props.socketIO.current_action = undefined;
    });
  }

  _onRefresh() {
    this.props.myPageStore.list_condition.data = [];
    this.props.myPageStore.list_condition.page = 0;
    this.props.myPageStore.list_condition.no_more_data = false;
    this._getData();
  }

  _getData() {
    this.props.myPageAction.getFavoriteFootprint(
      this.props.navigation.getParam('favorite'),
    );
  }

  render() {
    let {getListFavoriteFootprint} = this.props.myPageStore;
    let styleEmptyList = getListFavoriteFootprint.length
      ? null
      : style.styleEmptyList;

    return (
      <Container
        style={{borderTopColor: MyColor.secondary_color, borderTopWidth: 1}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          {this.props.systemStore.loading &&
          !getListFavoriteFootprint.length ? null : (
            <FlatList
              style={{flex: 1}}
              key={new Date().getTime()}
              data={getListFavoriteFootprint}
              keyExtractor={(item, index) =>
                index.toString() + item.id.toString()
              }
              renderItem={({item, index}) => {
                return <FavoriteItem index={index} item={item} />;
              }}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this._onRefresh()}
                  title="お気に入り"
                  tintColor={MyColor.primary_color}
                  titleColor={MyColor.primary_color}
                />
              }
              getItemLayout={this.itemLayout}
              onEndReachedThreshold={0.1}
              onEndReached={() => this._getData()}
              contentContainerStyle={styleEmptyList}
              ListEmptyComponent={this.emptyList}
            />
          )}
        </View>
      </Container>
    );
  }
}

export default inject('systemStore', 'myPageAction', 'myPageStore', 'socketIO')(
  observer(FavoritefootprintScreen),
);

const style = StyleSheet.create({
  styleEmptyList: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
});
