import React, {Component} from 'react';
import {FlatList, View, Text, StyleSheet, RefreshControl} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Container} from 'native-base';
import BlockItem from '../../components/block_item';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import MyColor from '../../../assets/MyColor';

class BlockListScreen extends Component {
  constructor(props) {
    super(props);

    this.emptyList = () => {
      return <Text>ブロックしたユーザーはいません。</Text>;
    };

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'ブロック済みユーザー一覧',
      };

      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());

      this.props.socketIO.current_action = events;
      this._onRefresh();
    });

    props.navigation.addListener('willBlur', () => {
      this.props.socketIO.current_action = undefined;
    });
  }

  async _onRefresh() {
    await this.props.myPageAction.getBlockList();
  }

  render() {
    const {getBlockListData} = this.props.myPageStore;
    console.log(getBlockListData);
    const {loading} = this.props.systemStore;
    const styleEmptyList = getBlockListData.length
      ? null
      : style.styleEmptyList;

    return (
      <Container
        style={{borderTopColor: MyColor.secondary_color, borderTopWidth: 1}}>
        <View style={{flex: 1}}>
          {loading && !getBlockListData.length ? null : (
            <FlatList
              style={{flex: 1}}
              data={getBlockListData}
              keyExtractor={(item, index) => index.toString() + item.user_code}
              renderItem={({item, index}) => {
                return <BlockItem index={index} item={item} />;
              }}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this._onRefresh()}
                  title="引っ張って更新"
                  tintColor={MyColor.main_color}
                  titleColor={MyColor.main_color}
                />
              }
              contentContainerStyle={styleEmptyList}
              ListEmptyComponent={this.emptyList}
            />
          )}
        </View>
      </Container>
    );
  }
}

export default inject('systemStore', 'myPageAction', 'myPageStore', 'socketIO')(
  observer(BlockListScreen),
);

const style = StyleSheet.create({
  styleEmptyList: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
});
