import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  Alert,
  Platform,
  Keyboard,
  PermissionsAndroid,
  ToastAndroid,
} from 'react-native';
import {Container, Button, Textarea} from 'native-base';
import {inject, observer} from 'mobx-react';
import ImageResizer from 'react-native-image-resizer';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import MyColor from '../../../assets/MyColor';
import RNPickerSelect from 'react-native-picker-select';
import * as ShowAlert from '../../../services/show_alert';
import AppConfig from '../../../services/constants/app_config';
import AssetImages from '../../../services/constants/asset_images';

const screenWidth = Dimensions.get('window').width;

class EditProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message_input: '',
    };

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'プロフィール編集',
      };

      this.setState({
        message_input: this.props.myPageStore.getMyProfile.user_status_tmp,
      });
      this._setValueUpdate();
    });

    props.navigation.addListener('willBlur', () => {
      this.props.systemStore.ng_words_alert = false;
    });
  }

  _setValueUpdate = () => {
    let popup_images = this.props.myPageStore.getMyProfile.image;
    popup_images.forEach(image => {
      image.path = image.path;
    });
    this.props.myPageStore.popup_images = popup_images;
    this.props.myPageStore.edit_profile.height = this.props.myPageStore.getMyProfile.height;
    this.props.myPageStore.edit_profile.style = this.props.myPageStore.getMyProfile.style;
    this.props.myPageStore.edit_profile.job = this.props.myPageStore.getMyProfile.job;
    this.props.myPageStore.edit_profile.income = this.props.myPageStore.getMyProfile.income;
  };

  _checkProfileChange = () => {
    try {
      this.props.myPageStore.getChangeEditProfile.height === false &&
      this.props.myPageStore.getChangeEditProfile.style === false &&
      this.props.myPageStore.getChangeEditProfile.job === false &&
      this.props.myPageStore.getChangeEditProfile.income === false &&
      this.props.myPageStore.getChangeEditProfile.status === false &&
      this.props.myPageStore.getChangeEditProfile.image === false
        ? (this.props.myPageStore.change_edit_profile.result = false)
        : (this.props.myPageStore.change_edit_profile.result = true);
      const ng_words = this.props.systemStore.server_state.ng_words.result;
      ng_words.forEach(ng_word => {
        if (ng_word) {
          let regEx = new RegExp(ng_word, 'ig');
          if (regEx.test(this.state.message_input)) {
            throw 'ng_words';
          } else {
            this.props.systemStore.ng_words_alert = false;
          }
        }
      });
    } catch (e) {
      console.log(e);
      this.props.systemStore.ng_words_alert = true;
    }
  };

  _saveUpdateProfile = () => {
    this.props.myPageStore.edit_profile.user_status = this.state.message_input;
    this.props.myPageAction.updateProfile();
    this.props.myPageAction.updateAvatar();
    this.props.myPageStore.change_edit_profile.result = false;
  };

  _changeAvatar = async index => {
    let permission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: '画像の読み取りが無効です',
        message:
          '画像の読み取りが無効です。設定から' +
          AppConfig.APP_NAME +
          'を開き、「写真」を「読み出しと書き込み」に変更することで、端末内の画像を送信することができます。',
      },
    );

    if (permission == PermissionsAndroid.RESULTS.GRANTED) {
      ImagePicker.launchImageLibrary(
        {
          rotation: 360,
        },
        response => {
          console.log('response choose image', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            ImageResizer.createResizedImage(response.uri, 761, 761, 'JPEG', 100)
              .then(async newImage => {
                console.log('ImageResizer', newImage);
                if (newImage.size <= 5e6) {
                  // value file size is bytes
                  let file = {
                    ...newImage,
                    type: 'image/jpeg',
                  };
                  file.data = await RNFetchBlob.fs.readFile(
                    file.path,
                    'base64',
                  );

                  this.props.myPageStore.popup_images[index].path =
                    newImage.uri;
                  this.props.myPageStore.files[index] = file;
                  this.props.myPageStore.change_edit_profile.image = true;
                  this._checkProfileChange();
                  // khai bao nguoi dung da sua hinh de show toast doi admin xet duyet sau khi nguoi dung an button save
                  this.props.myPageStore.edit_image_status = true;
                } else {
                  ToastAndroid.show(
                    ' ファイルサイズ > 5MB ',
                    ToastAndroid.LONG,
                  );
                }
              })
              .catch(err => {
                ToastAndroid.show(
                  ' このファイルタイプは送信できません。 ',
                  ToastAndroid.LONG,
                );
              });
          }
        },
      );
    } else {
      ShowAlert.alertRequestPermissionPhotos();
    }
  };

  _confirmEditProfile = () => {
    const ng_words = this.props.systemStore.server_state.ng_words;
    try {
      ng_words.result.forEach(ng_word => {
        let regEx = new RegExp(ng_word, 'ig');
        if (regEx.test(this.state.message_input)) throw 'ng_words';
      });

      Alert.alert(
        '',
        AppConfig.APP_NAME +
          'はお客様の写真、プロフィール、居住する都道府県等の情報を他のユーザーと共有します。公開する情報はプロフィールページで編集できます。',
        [
          {
            text: '拒否',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: '承認', onPress: () => this._saveUpdateProfile()},
        ],
        {cancelable: false},
      );
    } catch (e) {
      console.log(e);
      Alert.alert(
        '',
        '投稿内容にNGワードが含まれているため、書き込めません。もう一度入力してください。',
        [{text: 'OK', onPress: () => console.log('Cancel Pressed')}],
        {cancelable: false},
      );
    }
  };

  render() {
    const {server_state} = this.props.systemStore;
    const {popup_images, getMyProfile} = this.props.myPageStore;

    let height = server_state.user_profile_list.height;
    let height_keys = Object.keys(height);
    let body_style = server_state.user_profile_list.style;
    let style_keys = Object.keys(body_style);
    let job = server_state.user_profile_list.job;
    let job_keys = Object.keys(job);
    let income = server_state.user_profile_list.income;
    let income_keys = Object.keys(income);

    return (
      <Container
        style={{borderTopColor: MyColor.secondary_color, borderTopWidth: 1}}>
        <ScrollView
          ref={ref => {
            this.scroll = ref;
          }}
          keyboardShouldPersistTaps="always">
          <View style={styles.modal_container}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={styles.avatar_container}>
                <Button
                  transparent
                  style={styles.user_avatar}
                  onPress={() => {
                    this._changeAvatar(0);
                  }}>
                  {popup_images ? (
                    <Image
                      style={styles.user_avatar}
                      defaultSource={
                        getMyProfile.sex == '0'
                          ? AssetImages.default_man
                          : AssetImages.default_woman
                      }
                      source={{uri: popup_images[0].path}}
                    />
                  ) : null}
                </Button>
                <Button
                  transparent
                  style={[styles.user_avatar, styles.mid_avatar]}
                  onPress={() => {
                    this._changeAvatar(1);
                  }}>
                  {popup_images ? (
                    <Image
                      style={styles.user_avatar}
                      defaultSource={
                        getMyProfile.sex == '0'
                          ? AssetImages.default_man
                          : AssetImages.default_woman
                      }
                      source={{uri: popup_images[1].path}}
                    />
                  ) : null}
                </Button>
                <Button
                  transparent
                  style={styles.user_avatar}
                  onPress={() => {
                    this._changeAvatar(2);
                  }}>
                  {popup_images ? (
                    <Image
                      style={styles.user_avatar}
                      defaultSource={
                        getMyProfile.sex == '0'
                          ? AssetImages.default_man
                          : AssetImages.default_woman
                      }
                      source={{uri: popup_images[2].path}}
                    />
                  ) : null}
                </Button>
              </View>
              <Text
                style={{
                  fontSize: 10,
                  fontWeight: 'bold',
                  color: MyColor.secondary_color,
                }}>
                (1 枚目の画像が一覧に表示されます)
              </Text>
            </View>
            <View style={styles.user_container}>
              <View style={styles.header_label}>
                <Text style={styles.text_label}>詳細プロフィール</Text>
              </View>
              <View style={styles.info_content}>
                <Text style={styles.info_text}>ニックネーム</Text>
                <Text style={styles.info_text}>
                  {getMyProfile.display_name
                    ? getMyProfile.display_name
                    : '未設定'}
                </Text>
              </View>
              <View style={styles.info_content}>
                <Text style={styles.info_text}>性別</Text>
                <Text style={styles.info_text}>
                  {getMyProfile.sex
                    ? server_state.user_profile_list.sex[getMyProfile.sex].name
                    : '未設定'}
                </Text>
              </View>
              <View style={styles.info_content}>
                <Text style={styles.info_text}>年代</Text>
                <Text style={styles.info_text}>
                  {getMyProfile.age
                    ? server_state.user_profile_list.age[getMyProfile.age].name
                    : '未設定'}
                </Text>
              </View>
              <View style={styles.info_content}>
                <Text style={styles.info_text}>登録地域</Text>
                <Text style={styles.info_text}>
                  {getMyProfile.area_name ? getMyProfile.area_name : '未設定'}
                  {getMyProfile.city_name ? getMyProfile.city_name : '未設定'}
                </Text>
              </View>

              <View style={styles.picker_container}>
                <View
                  style={[styles.center_picker, {alignItems: 'flex-start'}]}>
                  <Text style={styles.info_text}>身長</Text>
                </View>
                <View style={styles.center_picker}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '指定なし',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.props.myPageStore.getEditProfile.height}
                    onValueChange={itemValue =>
                      (this.props.myPageStore.edit_profile.height = itemValue)
                    }
                    items={height_keys.map(value => {
                      return {
                        label: height[value]['name'],
                        value: height[value]['value'],
                        key: height[value]['value'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={styles.icon_picker}>
                  <Image
                    source={AssetImages.ic_arrow_right}
                    style={styles.sizeIcon}
                    resizeMode="contain"
                  />
                </View>
              </View>
              <View style={styles.picker_container}>
                <View
                  style={[styles.center_picker, {alignItems: 'flex-start'}]}>
                  <Text style={styles.info_text}>体型</Text>
                </View>
                <View style={styles.center_picker}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '指定なし',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.props.myPageStore.getEditProfile.style}
                    onValueChange={itemValue =>
                      (this.props.myPageStore.edit_profile.style = itemValue)
                    }
                    items={style_keys.map(value => {
                      return {
                        label: body_style[value]['name'],
                        value: body_style[value]['value'],
                        key: body_style[value]['value'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={styles.icon_picker}>
                  <Image
                    source={AssetImages.ic_arrow_right}
                    style={styles.sizeIcon}
                    resizeMode="contain"
                  />
                </View>
              </View>
              <View style={styles.picker_container}>
                <View
                  style={[styles.center_picker, {alignItems: 'flex-start'}]}>
                  <Text style={styles.info_text}>職業</Text>
                </View>
                <View style={styles.center_picker}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '指定なし',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.props.myPageStore.getEditProfile.job}
                    onValueChange={itemValue =>
                      (this.props.myPageStore.edit_profile.job = itemValue)
                    }
                    items={job_keys.map(value => {
                      return {
                        label: job[value]['name'],
                        value: job[value]['value'],
                        key: job[value]['value'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={styles.icon_picker}>
                  <Image
                    source={AssetImages.ic_arrow_right}
                    style={styles.sizeIcon}
                    resizeMode="contain"
                  />
                </View>
              </View>
              <View style={styles.picker_container}>
                <View
                  style={[styles.center_picker, {alignItems: 'flex-start'}]}>
                  <Text style={styles.info_text}>年収</Text>
                </View>
                <View style={styles.center_picker}>
                  <RNPickerSelect
                    style={pickerStyle}
                    placeholder={{
                      label: '指定なし',
                      value: '',
                      color: MyColor.secondary_color,
                    }}
                    placeholderTextColor={MyColor.secondary_color}
                    value={this.props.myPageStore.getEditProfile.income}
                    onValueChange={itemValue =>
                      (this.props.myPageStore.edit_profile.income = itemValue)
                    }
                    items={income_keys.map(value => {
                      return {
                        label: income[value]['name'],
                        value: income[value]['value'],
                        key: income[value]['value'],
                      };
                    })}
                    doneText={'閉じる'}
                    hideIcon={true}
                    useNativeAndroidPickerStyle={false}
                  />
                </View>
                <View style={styles.icon_picker}>
                  <Image
                    source={AssetImages.ic_arrow_right}
                    style={styles.sizeIcon}
                    resizeMode="contain"
                  />
                </View>
              </View>
              <View style={styles.header_label}>
                <Text style={styles.text_label}>自己紹介</Text>
              </View>
              <View style={styles.pr_container}>
                <Textarea
                  maxLength={100}
                  style={[styles.info_pr, {backgroundColor: 'white'}]}
                  rowSpan={10}
                  onChangeText={value => {
                    this.setState({message_input: value});
                    if (
                      this.props.myPageStore.getMyProfile.user_status_tmp !==
                      value
                    ) {
                      this.props.myPageStore.change_edit_profile.status = true;
                      // khai bao nguoi dung da sua status de show toast doi admin xet duyet sau khi nguoi dung an button save
                      this.props.myPageStore.edit_image_status = true;
                    } else {
                      this.props.myPageStore.change_edit_profile.status = false;
                    }
                    this._checkProfileChange();
                  }}
                  value={this.state.message_input}
                  ref={ref => {
                    this.input = ref;
                  }}
                  onFocus={() => this.scroll.scrollToEnd({animated: true})}
                  inputAccessoryViewID={'input'}
                />
              </View>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: 30,
                }}>
                <Button
                  onPress={this._confirmEditProfile}
                  style={{
                    borderRadius: 30,
                    width: '60%',
                    height: 45,
                    backgroundColor: MyColor.main_color,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>
                    保存
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default inject('systemStore', 'myPageAction', 'myPageStore')(
  observer(EditProfileScreen),
);

const pickerStyle = {
  inputAndroid: {
    height: 40,
    width: screenWidth / 2 - 20,
    backgroundColor: 'transparent',
    fontSize: 14,
    color: MyColor.secondary_color,
    fontWeight: 'bold',
  },
  inputIOS: {
    flex: 5,
    fontSize: 14,
    color: MyColor.secondary_color,
    fontWeight: 'bold',
  },
  iconContainer: {
    top: 9,
    right: 15,
  },
};

const styles = StyleSheet.create({
  avatar_container: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  user_avatar: {
    height: 120,
    width: 85,
  },
  mid_avatar: {
    marginLeft: 8,
    marginRight: 8,
  },
  header_label: {
    width: '100%',
    paddingLeft: 15,
    paddingVertical: 8,
    borderBottomColor: MyColor.secondary_color,
    borderBottomWidth: 1.5,
  },
  text_label: {
    color: 'black',
    fontWeight: '700',
    fontSize: 14,
  },
  user_container: {
    marginTop: 20,
    flexDirection: 'column',
  },
  info_content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    paddingLeft: 20,
  },
  info_text: {
    fontSize: 16,
    color: MyColor.secondary_color,
    fontWeight: 'bold',
    flex: 5,
  },
  picker_content: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon_picker: {
    position: 'absolute',
    right: 0,
    height: 47,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sizeIcon: {
    height: 14,
    width: 30,
  },
  pr_container: {
    paddingHorizontal: 8,
    marginTop: 10,
    marginBottom: 15,
  },
  info_pr: {
    borderWidth: 1,
    borderColor: MyColor.form_register_color,
    borderRadius: 10,
    padding: 5,
  },
  picker_container: {
    flexDirection: 'row',
    padding: 15,
    paddingLeft: 20,
    justifyContent: 'space-between',
    height: 55,
  },
  center_picker: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 5,
  },
});
