import React, {Component} from 'react';
import {View, StyleSheet, Keyboard, Alert, Platform} from 'react-native';
import {Container, Text, Textarea, Button} from 'native-base';
import {inject, observer} from 'mobx-react';
import AppConfig from '../../../services/constants/app_config';
import MyColor from '../../../assets/MyColor';

class QaScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message_input: '',
    };

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'お問い合わせ',
      };
    });

    props.navigation.addListener('willBlur', () => {});
  }

  _sendQuestion() {
    this.props.socketIO.sendMessage(
      this.state.message_input,
      this.props.systemStore.server_state.supporter.id,
    );
    this.setState({message_input: ''});
    Keyboard.dismiss();
    Alert.alert(
      AppConfig.APP_NAME,
      '\nお問い合わせのメッセージを送信しました。返信が来るまで少々お待ち下さい。',
      [{text: 'OK', onPress: () => {}}],
      {cancelable: false},
    );
  }

  render() {
    return (
      <Container>
        <View style={styles.header_label}>
          <Text style={styles.text_label}>お問い合わせ内容</Text>
        </View>
        <View style={styles.content}>
          <Textarea
            style={styles.info_pr}
            value={this.state.message_input}
            onChangeText={text => this.setState({message_input: text})}
            inputAccessoryViewID={'input'}
          />

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 20,
              marginBottom: 10,
            }}>
            <Button
              onPress={() => this._sendQuestion()}
              disabled={this.state.message_input == ''}
              style={{
                borderRadius: 30,
                width: '65%',
                height: 45,
                backgroundColor: MyColor.main_color,
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>
                送信
              </Text>
            </Button>
          </View>
        </View>
      </Container>
    );
  }
}
export default inject('systemStore', 'socketIO')(observer(QaScreen));

const styles = StyleSheet.create({
  header_label: {
    width: '100%',
    padding: 5,
    backgroundColor: MyColor.main_color,
  },
  text_label: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
  },
  content: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  info_pr: {
    borderRadius: 5,
    borderWidth: 1.5,
    borderColor: '#d3d3d3',
    minHeight: 200,
    padding: 5,
    marginTop: 10,
    backgroundColor: 'white',
  },
  button_container: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
  },
  background_button: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundDisable: {
    backgroundColor: '#A7D5F0',
  },
  backgroundEnable: {
    backgroundColor: '#2584C2',
  },
  icon_right: {
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    width: 15,
    height: 15,
    right: 5,
  },
});
