import React, {Component} from 'react';
import {AppState, View, Image, TouchableWithoutFeedback} from 'react-native';
import {
  Container,
  List,
  ListItem,
  Left,
  Text,
  Right,
  Icon,
  Switch,
} from 'native-base';
import {inject, observer} from 'mobx-react';
import NavigatorService from '../../../services/navigator/navigation_services';
import DeviceInfo from 'react-native-device-info';
import AssetImages from '../../../services/constants/asset_images';
import AndroidOpenSettings from 'react-native-android-open-settings';
import PushNotification from 'react-native-push-notification';
import MyColor from '../../../assets/MyColor';

class SettingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      switch_value: false,
    };

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: true,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: '設定',
      };

      // Check permission receive notification
      this._checkNotificationsPermission();
      AppState.addEventListener('change', this._checkNotificationsPermission);
    });

    props.navigation.addListener('willBlur', () => {
      AppState.removeEventListener(
        'change',
        this._checkNotificationsPermission,
      );
    });
  }

  _checkNotificationsPermission = () => {
    // Check permission receive notification
    PushNotification.checkPermissions(permission => {
      this.setState({switch_value: permission.alert});
    });
  };

  _linkSetting = () => {
    AndroidOpenSettings.appNotificationSettings();
  };

  render() {
    return (
      <Container
        style={{borderTopColor: MyColor.secondary_color, borderTopWidth: 1}}>
        <List>
          <ListItem style={{borderBottomWidth: 0}}>
            <Left>
              <Text style={{color: MyColor.secondary_color}}>
                プッシュ受信設定
              </Text>
            </Left>
            <Right>
              <Switch
                onValueChange={value => this._linkSetting(value)}
                value={this.state.switch_value}
              />
            </Right>
          </ListItem>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.systemAction.saveLastScreen('SettingScreen');
              NavigatorService.navigate('BlockList');
            }}>
            <ListItem style={{borderBottomWidth: 0}}>
              <Left>
                <Text style={{color: MyColor.secondary_color}}>
                  ブロック済みユーザー一覧
                </Text>
              </Left>
              <Right>
                <Image
                  source={AssetImages.ic_arrow_right}
                  style={{width: 15, height: 15}}
                  resizeMode="contain"
                />
              </Right>
            </ListItem>
          </TouchableWithoutFeedback>
          <ListItem style={{borderBottomWidth: 0}}>
            <Left>
              <Text style={{color: MyColor.secondary_color}}>バージョン</Text>
            </Left>
            <Right>
              <Text>{DeviceInfo.getVersion()}</Text>
            </Right>
          </ListItem>
        </List>
      </Container>
    );
  }
}
export default inject('systemStore', 'systemAction')(observer(SettingScreen));
