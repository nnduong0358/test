import React, {Component} from 'react';
import {FlatList} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Container} from 'native-base';
import PointTableItem from '../../components/point_table_item';

class PointTableScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: 'ポイント表',
      };

      this.props.pointAction.getPointTable();
    });

    props.navigation.addListener('willBlur', () => {});
  }

  render() {
    const {getPointTableData} = this.props.pointStore;

    return (
      <Container style={{backgroundColor: 'white'}}>
        <FlatList
          style={{
            flex: 1,
            backgroundColor: '#ECEFF1',
            paddingLeft: 10,
            paddingRight: 10,
          }}
          data={getPointTableData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => {
            return <PointTableItem index={index} item={item} />;
          }}
        />
      </Container>
    );
  }
}

export default inject('systemStore', 'pointAction', 'pointStore')(
  observer(PointTableScreen),
);
