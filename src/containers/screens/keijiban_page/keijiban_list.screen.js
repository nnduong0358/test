import React, {Component} from 'react';
import {
  FlatList,
  RefreshControl,
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import KeijibanItem from '../../components/keijiban_item';
import {Container} from 'native-base';
import MyColor from '../../../assets/MyColor';
import AssetImages from '../../../services/constants/asset_images';
import NavigatorService from '../../../services/navigator/navigation_services';

const ITEM_HEIGHT = 70;

class KeijibanListScreen extends Component {
  constructor(props) {
    super(props);

    this.itemLayout = (data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    });

    this.emptyList = () => {
      return <Text>投稿がありません。</Text>;
    };

    props.navigation.addListener('willFocus', () => {
      let events = new EventEmitter();
      events.addListener('refresh', () => this._onRefresh());

      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: true,
        refresh_action: events,
        search_button: true,
        search_action: 'keijiban_search',
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,
        open_profile_button: undefined,
        open_profile_action: undefined,
        title: '掲示板',
      };

      this.props.socketIO.current_action = events;
      this._onRefresh();
    });

    props.navigation.addListener('willBlur', () => {
      this.props.keijibanStore.keijiban_list_condition.result_find = false;
    });
  }

  async _onRefresh() {
    this.props.keijibanStore.keijiban_list_data = [];
    this.props.keijibanStore.keijiban_list_condition.page = 0;
    this.props.keijibanStore.keijiban_list_condition.no_more_data = false;
    this._getData();
  }

  async _getData() {
    if (this.props.keijibanStore.keijiban_list_condition.result_find) {
      this.props.keijibanAction.getKeijibanList('search');
    } else {
      this.props.keijibanAction.getKeijibanList('all');
    }
  }

  render() {
    const {loading} = this.props.systemStore;
    const {getKeijibanListData} = this.props.keijibanStore;
    const styleEmptyList = getKeijibanListData.length
      ? null
      : style.styleEmptyList;

    return (
      <Container style={{backgroundColor: MyColor.background_color}}>
        {loading && !getKeijibanListData.length ? null : (
          <FlatList
            style={{flex: 1}}
            data={getKeijibanListData}
            renderItem={({item, index}) => (
              <KeijibanItem item={item} index={index} />
            )}
            keyExtractor={(item, index) => index.toString() + item.id}
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={() => {
                  this._onRefresh();
                }}
                title="引っ張って更新"
                tintColor={MyColor.main_color}
                titleColor={MyColor.main_color}
              />
            }
            getItemLayout={null}
            onEndReached={() => {
              this._getData();
            }}
            onEndReachedThreshold={0.1}
            contentContainerStyle={styleEmptyList}
            ListEmptyComponent={this.emptyList}
          />
        )}
        <TouchableOpacity
          style={style.TouchableOpacityStyle}
          activeOpacity={0}
          onPress={() => {
            NavigatorService.navigate('KeijibanPost');
          }}>
          <View style={{height: 30, paddingTop: 7, left: -2}}>
            <Image
              style={{width: 18, height: 18}}
              resizeMode="contain"
              source={AssetImages.ic_pencil}
            />
          </View>
          <Text style={{fontSize: 10, fontWeight: 'bold', color: 'white'}}>
            投稿する
          </Text>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default inject(
  'systemStore',
  'keijibanAction',
  'keijibanStore',
  'socketIO',
)(observer(KeijibanListScreen));

const style = StyleSheet.create({
  styleEmptyList: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
  TouchableOpacityStyle: {
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.5,
    // shadowRadius: 10,
    // elevation: 3,
    flexDirection: 'column',
    position: 'absolute',
    backgroundColor: MyColor.main_color,
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    padding: 3,
    right: 10,
    bottom: 10,
  },
});
