import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  Alert,
  ScrollView,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Button, Container} from 'native-base';
import ReportPopup from '../../components/popupReportUser';
import ImagePopup from '../../components/imageModal';
import PopupActionSheet from '../../components/popupActionSheet';
import AppConfig from '../../../services/constants/app_config';
import * as ShowAlert from '../../../services/show_alert';
import MyColor from '../../../assets/MyColor';
import AssetImages from '../../../services/constants/asset_images';
import {checkCacheImage} from '../../../services/helpers';

const default_image_width =
  ((Dimensions.get('window').width - 20) * 15) / 100 - 5;

class KeijibanDetailScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {image: ''};

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: '掲示板',
      };

      this.getCacheImages();

      this.props.userStore.list_report_popup = {
        user_code: this.props.keijibanStore.keijiban_detail.user_code,
        id: this.props.keijibanStore.keijiban_detail.user_id,
        no_block: true,
        screen: 'keijiban',
        keijiban_id: this.props.keijibanStore.keijiban_detail.id,
      };
    });

    props.navigation.addListener('willBlur', () => {
      this.setState({image: ''});
    });
  }

  async getCacheImages() {
    this.setState({
      image: await checkCacheImage(
        this.props.userStore.user_profile.image[0].path,
      ),
    });
  }

  _onpenChattingPage = () => {
    this.props.systemAction.saveLastScreen('KeijibanDetail');
    this.props.messageAction.openChatting(
      this.props.userStore.user_profile.id,
      this.props.userStore.user_profile.user_code,
      this.props.userStore.user_profile.display_name,
      this.state.image,
    );
  };

  _onPressViewImage = async () => {
    await this.props.pointAction.getPointFee();
    if (
      this.props.pointAction.checkUserEnoughPoint('view_media_discuss_image')
    ) {
      this.props.keijibanAction.viewKeijibanImage(
        this.props.keijibanStore.keijiban_detail.origin,
      );
    } else {
      ShowAlert.showAlertUserNotEnoughPoint();
    }
  };

  _showProfile = async () => {
    await this.props.pointAction.getPointFee();
    if (this.props.pointAction.checkUserEnoughPoint('view_profile')) {
      this.props.systemAction.saveLastScreen('KeijibanDetail');
      this.props.userAction.getUserProfile(
        this.props.keijibanStore.keijiban_detail.user_id,
      );
    } else {
      ShowAlert.showAlertUserNotEnoughPoint();
    }
  };

  _showAlertBuyUnlimitPoint = async () => {
    await this.props.pointAction.getPointFee();
    if (this.props.pointAction.checkUserEnoughPoint('unlimit_point')) {
      Alert.alert(
        AppConfig.APP_NAME,
        '\n' + this.props.pointStore.unlimit_description,
        [
          {
            text: 'はい',
            onPress: () => {
              this.props.pointAction.postUnlimitPoint(
                this.props.keijibanStore.keijiban_detail.user_id,
                this.props.keijibanStore.keijiban_detail.user_code,
              );
            },
          },
          {
            text: 'いいえ',
            onPress: () => console.log('Cancel'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  };

  render() {
    const {keijiban_detail} = this.props.keijibanStore;
    const {user_profile} = this.props.userStore;
    const {server_state} = this.props.systemStore;
    console.log('keijiban_detail');

    return (
      <Container style={{backgroundColor: 'white', flex: 1}}>
        <ReportPopup />
        <ImagePopup />
        <PopupActionSheet />

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            paddingHorizontal: 20,
            paddingVertical: 10,
          }}>
          <Button
            onPress={() => {
              if (user_profile.favorite_status == 1) {
                ShowAlert.favoriteStatus();
              } else {
                this.props.userAction.postFavorite(
                  user_profile.id,
                  user_profile.user_code,
                );
              }
            }}
            style={{
              flex: 3,
              borderTopLeftRadius: 8,
              borderBottomLeftRadius: 8,
              borderColor: MyColor.secondary_color,
              borderWidth: 1,
              backgroundColor: 'white',
              height: 45,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 14,
                color: MyColor.secondary_color,
                fontWeight: 'bold',
              }}>
              お気に入り登録
            </Text>
          </Button>
          <Button
            onPress={this._showAlertBuyUnlimitPoint}
            style={{
              flex: 3,
              height: 45,
              borderColor: MyColor.secondary_color,
              borderWidth: 1,
              borderLeftWidth: 0,
              backgroundColor: 'white',
              justifyContent: 'center',
            }}
            disabled={user_profile.unlimit_point == 1}>
            <Text
              style={{
                fontSize: 14,
                color: MyColor.secondary_color,
                fontWeight: 'bold',
              }}>
              リミット解除
            </Text>
          </Button>
          <Button
            onPress={() => (this.props.userStore.popup_action_sheet = true)}
            style={{
              flex: 3,
              height: 45,
              borderBottomRightRadius: 8,
              borderTopRightRadius: 8,
              borderWidth: 1,
              borderLeftWidth: 0,
              borderColor: MyColor.secondary_color,
              backgroundColor: 'white',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 14,
                color: MyColor.secondary_color,
                fontWeight: 'bold',
              }}>
              通報
            </Text>
          </Button>
        </View>

        <View style={style.container_info_user}>
          <View style={style.user_block}>
            <View style={style.block_avatar}>
              {this.state.image ? (
                <Image
                  style={style.avatar}
                  defaultSource={
                    user_profile.sex == '0'
                      ? AssetImages.default_man
                      : AssetImages.default_woman
                  }
                  source={{uri: this.state.image}}
                />
              ) : null}
            </View>

            <View style={style.block_info}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View
                  style={{
                    flex: 5,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      style.text_style,
                      {color: MyColor.main_color, fontWeight: '700'},
                    ]}>
                    {keijiban_detail.displayname}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 5,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                  }}
                />
              </View>

              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingVertical: 5,
                }}>
                <View style={{flex: 5, justifyContent: 'flex-start'}}>
                  <Text numberOfLines={1} style={style.text_style}>
                    {keijiban_detail.area_name + ' '}
                    {keijiban_detail.age
                      ? server_state.user_profile_list.age[
                          parseInt(keijiban_detail.age)
                        ].name
                      : ''}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 5,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                  }}>
                  <Button
                    style={{
                      padding: 10,
                      flexDirection: 'row',
                      backgroundColor: 'yellow',
                      borderWidth: 1,
                      borderColor: 'red',
                      borderRadius: 15,
                      height: 28,
                    }}
                    onPress={() => this._showProfile()}>
                    <Text
                      style={{color: 'red', fontSize: 11, fontWeight: 'bold'}}>
                      プロフを確認
                    </Text>
                  </Button>
                </View>
              </View>

              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <View style={style.arrow} />
                <View style={style.block_user_status}>
                  <Text
                    numberOfLines={2}
                    style={[style.text_style, {fontSize: 13}]}>
                    {keijiban_detail.content}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>

        <ScrollView>
          <View style={style.container_input}>
            <View
              style={[
                style.containTextInput,
                {marginTop: 5},
                keijiban_detail.thumb ? {} : {paddingBottom: 5},
              ]}>
              <View style={{minHeight: 100}}>
                <Text style={style.textKInput}>{keijiban_detail.content}</Text>
              </View>
            </View>
          </View>

          <View
            style={[
              style.content_button,
              keijiban_detail.thumb ? {justifyContent: 'space-between'} : {},
            ]}>
            <Button
              onPress={this._onpenChattingPage}
              style={[
                style.buttonStyle,
                keijiban_detail.thumb ? {width: '49%'} : {width: '80%'},
              ]}>
              <Image
                source={AssetImages.ic_message}
                style={{height: 20, width: 26}}
                resizeMode="contain"
              />
              <Text style={style.text_button}>メッセージを送る</Text>
            </Button>

            {keijiban_detail.thumb ? (
              <Button
                onPress={this._onPressViewImage}
                disabled={!keijiban_detail.thumb}
                style={[style.buttonStyle, {width: '49%'}]}>
                <Image
                  source={AssetImages.ic_image}
                  style={{height: 20, width: 26}}
                  resizeMode="contain"
                />
                <Text style={style.text_button}> 写真を見る</Text>
              </Button>
            ) : null}
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default inject(
  'systemAction',
  'systemStore',
  'userAction',
  'userStore',
  'pointAction',
  'messageAction',
  'pointStore',
  'keijibanAction',
  'keijibanStore',
)(observer(KeijibanDetailScreen));

let screenWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  container_info_user: {
    borderTopWidth: 1.5,
    borderBottomWidth: 1.5,
    borderColor: '#d3d3d3',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    width: screenWidth,
  },
  user_block: {
    flexDirection: 'row',
    padding: 8,
  },
  block_avatar: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingRight: 20,
    paddingVertical: 5,
  },
  avatar: {
    width: screenWidth / 4,
    height: screenWidth / 4,
    borderRadius: 12,
  },
  block_info: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    paddingVertical: 5,
    paddingRight: 8,
  },
  text_style: {
    fontSize: 13,
    color: 'black',
  },
  arrow: {
    height: 12,
    width: 12,
    borderBottomWidth: 1.5,
    borderLeftWidth: 1.5,
    borderColor: '#d3d3d3',
    transform: [{rotateX: '45deg'}, {rotateZ: '0.785398rad'}],
    position: 'relative',
    zIndex: 8,
    backgroundColor: 'white',
    //top: 12,
  },
  block_user_status: {
    left: -13.5,
    width: '100%',
    borderWidth: 1.5,
    borderColor: '#d3d3d3',
    borderRadius: 6,
    padding: 2,
    marginLeft: 7,
    position: 'relative',
    zIndex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
  },
  container_input: {
    padding: 5,
    paddingTop: 10,
  },
  containTextInput: {
    flexDirection: 'column',
    paddingTop: 5,
  },
  textKInput: {
    textAlign: 'left',
    lineHeight: parseInt(16 * 1.5),
    fontSize: 14,
    color: 'black',
    paddingLeft: 10,
    paddingRight: 10,
  },
  content_button: {
    paddingVertical: 20,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text_button: {
    fontSize: 14,
    color: 'white',
    fontWeight: 'bold',
  },
  buttonStyle: {
    borderRadius: 30,
    height: 45,
    backgroundColor: MyColor.main_color,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
