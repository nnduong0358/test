import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  Text,
  Dimensions,
} from 'react-native';
import {Container, Button} from 'native-base';
import {inject, observer} from 'mobx-react';
import MyColor from '../../../assets/MyColor';
import RNPickerSelect from 'react-native-picker-select';
import AssetImage from '../../../services/constants/asset_images';

let screenWidth = Dimensions.get('window').width;

class KeijibanSearchScreen extends Component {
  constructor(props) {
    super(props);

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: undefined,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: true,
        message_button: undefined,
        message_action: undefined,

        open_profile_button: undefined,
        open_profile_action: undefined,
        title: '掲示板',
      };
    });

    props.navigation.addListener('willBlur', () => {});
  }

  _onPressSearch = async () => {
    this.props.keijibanStore.keijiban_list_condition.result_find = true;
    this.props.systemAction.goBack();
  };

  render() {
    const {server_state} = this.props.systemStore;
    let sex = server_state.user_profile_list.sex;
    let sex_keys = Object.keys(sex);
    let age = server_state.user_profile_list.age;
    let age_keys = Object.keys(age);
    let height = server_state.user_profile_list.height;
    let height_keys = Object.keys(height);
    let body_style = server_state.user_profile_list.style;
    let style_keys = Object.keys(body_style);
    let job = server_state.user_profile_list.job;
    let job_keys = Object.keys(job);
    let income = server_state.user_profile_list.income;
    let income_keys = Object.keys(income);

    return (
      <Container
        style={{backgroundColor: MyColor.background_color, paddingBottom: 20}}>
        <ScrollView>
          <View style={style.picker_container}>
            <View
              style={[
                style.center,
                {paddingVertical: 20, alignItems: 'flex-start'},
              ]}>
              <Text style={style.text_label}>性別</Text>
            </View>
            <View style={[style.center, {flex: 8}]}>
              <RNPickerSelect
                style={pickerStyle}
                placeholder={{
                  label: '指定なし',
                  value: '',
                  color: MyColor.secondary_color,
                }}
                placeholderTextColor={MyColor.secondary_color}
                value={
                  this.props.keijibanStore.keijiban_list_condition.fills.sex
                }
                onValueChange={value =>
                  (this.props.keijibanStore.keijiban_list_condition.fills.sex = value)
                }
                items={sex_keys.map(value => {
                  return {
                    label: sex[value]['name'],
                    value: sex[value]['value'],
                    key: sex[value]['value'],
                  };
                })}
                doneText={'閉じる'}
                hideIcon={true}
                useNativeAndroidPickerStyle={false}
              />
            </View>
            <View style={[style.center, {paddingVertical: 20, right: -20}]}>
              <Image
                source={AssetImage.ic_arrow_right}
                style={{height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>

          <View style={style.picker_container}>
            <View
              style={[
                style.center,
                {paddingVertical: 20, alignItems: 'flex-start'},
              ]}>
              <Text style={style.text_label}>年代</Text>
            </View>
            <View style={[style.center, {flex: 8}]}>
              <RNPickerSelect
                style={pickerStyle}
                placeholder={{
                  label: '指定なし',
                  value: '',
                  color: MyColor.secondary_color,
                }}
                placeholderTextColor={MyColor.secondary_color}
                value={
                  this.props.keijibanStore.keijiban_list_condition.fills.age
                }
                onValueChange={value =>
                  (this.props.keijibanStore.keijiban_list_condition.fills.age = value)
                }
                items={age_keys.map(value => {
                  return {
                    label: age[value]['name'],
                    value: age[value]['value'],
                    key: age[value]['value'],
                  };
                })}
                doneText={'閉じる'}
                hideIcon={true}
                useNativeAndroidPickerStyle={false}
              />
            </View>
            <View style={[style.center, {paddingVertical: 20, right: -20}]}>
              <Image
                source={AssetImage.ic_arrow_right}
                style={{height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>

          <View style={style.picker_container}>
            <View
              style={[
                style.center,
                {paddingVertical: 20, alignItems: 'flex-start'},
              ]}>
              <Text style={style.text_label}>身長</Text>
            </View>
            <View style={[style.center, {flex: 8}]}>
              <RNPickerSelect
                style={pickerStyle}
                placeholder={{
                  label: '指定なし',
                  value: '',
                  color: MyColor.secondary_color,
                }}
                placeholderTextColor={MyColor.secondary_color}
                value={
                  this.props.keijibanStore.keijiban_list_condition.fills.height
                }
                onValueChange={value =>
                  (this.props.keijibanStore.keijiban_list_condition.fills.height = value)
                }
                items={height_keys.map(value => {
                  return {
                    label: height[value]['name'],
                    value: height[value]['value'],
                    key: height[value]['value'],
                  };
                })}
                doneText={'閉じる'}
                hideIcon={true}
                useNativeAndroidPickerStyle={false}
              />
            </View>
            <View style={[style.center, {paddingVertical: 20, right: -20}]}>
              <Image
                source={AssetImage.ic_arrow_right}
                style={{height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>

          <View style={style.picker_container}>
            <View
              style={[
                style.center,
                {paddingVertical: 20, alignItems: 'flex-start'},
              ]}>
              <Text style={style.text_label}>体型</Text>
            </View>
            <View style={[style.center, {flex: 8}]}>
              <RNPickerSelect
                style={pickerStyle}
                placeholder={{
                  label: '指定なし',
                  value: '',
                  color: MyColor.secondary_color,
                }}
                placeholderTextColor={MyColor.secondary_color}
                value={
                  this.props.keijibanStore.keijiban_list_condition.fills.style
                }
                onValueChange={value =>
                  (this.props.keijibanStore.keijiban_list_condition.fills.style = value)
                }
                items={style_keys.map(value => {
                  return {
                    label: body_style[value]['name'],
                    value: body_style[value]['value'],
                    key: body_style[value]['value'],
                  };
                })}
                doneText={'閉じる'}
                hideIcon={true}
                useNativeAndroidPickerStyle={false}
              />
            </View>
            <View style={[style.center, {paddingVertical: 20, right: -20}]}>
              <Image
                source={AssetImage.ic_arrow_right}
                style={{height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>

          <View style={style.picker_container}>
            <View
              style={[
                style.center,
                {paddingVertical: 20, alignItems: 'flex-start'},
              ]}>
              <Text style={style.text_label}>職業</Text>
            </View>
            <View style={[style.center, {flex: 8}]}>
              <RNPickerSelect
                style={pickerStyle}
                placeholder={{
                  label: '指定なし',
                  value: '',
                  color: MyColor.secondary_color,
                }}
                placeholderTextColor={MyColor.secondary_color}
                value={
                  this.props.keijibanStore.keijiban_list_condition.fills.job
                }
                onValueChange={value =>
                  (this.props.keijibanStore.keijiban_list_condition.fills.job = value)
                }
                items={job_keys.map(value => {
                  return {
                    label: job[value]['name'],
                    value: job[value]['value'],
                    key: job[value]['value'],
                  };
                })}
                doneText={'閉じる'}
                hideIcon={true}
                useNativeAndroidPickerStyle={false}
              />
            </View>
            <View style={[style.center, {paddingVertical: 20, right: -20}]}>
              <Image
                source={AssetImage.ic_arrow_right}
                style={{height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>

          <View style={style.picker_container}>
            <View
              style={[
                style.center,
                {paddingVertical: 20, alignItems: 'flex-start'},
              ]}>
              <Text style={style.text_label}>年収</Text>
            </View>
            <View style={[style.center, {flex: 8}]}>
              <RNPickerSelect
                style={pickerStyle}
                placeholder={{
                  label: '指定なし',
                  value: '',
                  color: MyColor.secondary_color,
                }}
                placeholderTextColor={MyColor.secondary_color}
                value={
                  this.props.keijibanStore.keijiban_list_condition.fills.income
                }
                onValueChange={value =>
                  (this.props.keijibanStore.keijiban_list_condition.fills.income = value)
                }
                items={income_keys.map(value => {
                  return {
                    label: income[value]['name'],
                    value: income[value]['value'],
                    key: income[value]['value'],
                  };
                })}
                doneText={'閉じる'}
                hideIcon={true}
                useNativeAndroidPickerStyle={false}
              />
            </View>
            <View style={[style.center, {paddingVertical: 20, right: -20}]}>
              <Image
                source={AssetImage.ic_arrow_right}
                style={{height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>

          <View style={style.picker_container}>
            <View
              style={[
                style.center,
                {paddingVertical: 20, alignItems: 'flex-start'},
              ]}>
              <Text style={style.text_label}>並び順</Text>
            </View>
            <View style={[style.center, {flex: 8}]}>
              <RNPickerSelect
                style={pickerStyle}
                placeholder={{
                  label: '指定なし',
                  value: '',
                  color: MyColor.secondary_color,
                }}
                placeholderTextColor={MyColor.secondary_color}
                value={
                  this.props.keijibanStore.keijiban_list_condition.fills.sort_by
                }
                onValueChange={value =>
                  (this.props.keijibanStore.keijiban_list_condition.fills.sort_by = value)
                }
                items={[
                  {label: '登録順', value: '1'},
                  {label: 'アクセス順', value: '2'},
                ]}
                doneText={'閉じる'}
                hideIcon={true}
                useNativeAndroidPickerStyle={false}
              />
            </View>
            <View style={[style.center, {paddingVertical: 20, right: -20}]}>
              <Image
                source={AssetImage.ic_arrow_right}
                style={{height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 40,
              marginBottom: 10,
            }}>
            <Button
              onPress={this._onPressSearch}
              style={{
                borderRadius: 30,
                width: '80%',
                height: 55,
                backgroundColor: MyColor.main_color,
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: 16, color: 'white', fontWeight: 'bold'}}>
                この条件で表示
              </Text>
            </Button>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default inject('systemAction', 'systemStore', 'keijibanStore')(
  observer(KeijibanSearchScreen),
);

const pickerStyle = {
  inputAndroid: {
    height: '100%',
    width: (screenWidth / 6) * 6,
    backgroundColor: 'transparent',
    fontSize: 14,
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  inputIOS: {
    width: '100%',
    height: '100%',
    fontSize: 14,
    color: MyColor.secondary_color,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  iconContainer: {
    top: 9,
    right: 15,
  },
};

const style = StyleSheet.create({
  picker_container: {
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 10,
    borderBottomWidth: 1,
    borderColor: MyColor.secondary_color,
    justifyContent: 'space-between',
    height: 55,
  },
  text_label: {
    color: MyColor.secondary_color,
    fontSize: 14,
    fontWeight: 'bold',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2,
  },
});
