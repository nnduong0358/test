import React, {Component} from 'react';
import {
  Alert,
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  PermissionsAndroid,
  TouchableWithoutFeedback,
  Keyboard,
  ToastAndroid,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Textarea, Button, Container} from 'native-base';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import AppConfig from '../../../services/constants/app_config';
import * as ShowAlert from '../../../services/show_alert';
import MyColor from '../../../assets/MyColor';
import AssetImages from '../../../services/constants/asset_images';

const default_image_width =
  ((Dimensions.get('window').width - 20) * 15) / 100 - 5;

class KeijibanPostScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: '',
      file: undefined,
      imageWidth: default_image_width,
      imageHeight: default_image_width,
    };

    props.navigation.addListener('willFocus', () => {
      this.props.systemStore.header_app = {
        back_button: true,
        back_to: undefined,
        refresh_button: undefined,
        refresh_action: undefined,
        search_button: undefined,
        search_action: undefined,
        cancel_button: undefined,
        message_button: undefined,
        message_action: undefined,
        open_profile_button: undefined,
        open_profile_action: undefined,
        title: '掲示板',
      };
    });

    props.navigation.addListener('willBlur', () => {
      Keyboard.dismiss();
      this.setState({
        content: '',
        file: undefined,
        imageWidth: default_image_width,
        imageHeight: default_image_width,
      });
    });
  }

  _onSubmitPost = async () => {
    Keyboard.dismiss();
    if (!this.state.content) {
      Alert.alert(
        '',
        'コメントを入力してください。',
        [{text: 'OK', onPress: () => {}}],
        {cancelable: true},
      );
    } else {
      let minus_point = 0;
      if (this.state.content)
        minus_point += this.props.pointStore.getPointSettings.discuss_post;
      if (this.state.file)
        minus_point += this.props.pointStore.getPointSettings
          .discuss_post_image;

      if (minus_point <= this.props.pointStore.getTotalPoint) {
        await this.props.keijibanAction.postKeijiban(
          this.state.content,
          this.state.file,
        );
        this.setState({
          content: '',
          file: undefined,
        });
      } else {
        ShowAlert.alertUserNotEnoughPoint();
      }
    }
    Keyboard.dismiss();
  };

  _onPressChooseFile = async () => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
      title: '画像の読み取りが無効です',
      message:
        '画像の読み取りが無効です。設定から' +
        AppConfig.APP_NAME +
        'を開き、「写真」を「読み出しと書き込み」に変更することで、端末内の画像を送信することができます。',
    })
      .then(permission => {
        if (permission === PermissionsAndroid.RESULTS.GRANTED) {
          ImagePicker.launchImageLibrary(
            {
              rotation: 360,
            },
            async response => {
              if (response.didCancel) {
                console.log('User cancelled image picker');
              } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
              } else {
                console.log('image keijiban', response);
                let new_image = await ImageResizer.createResizedImage(
                  response.uri,
                  940,
                  788,
                  'JPEG',
                  100,
                );
                console.log('new image keijiban', new_image);
                if (new_image.size <= 5e6) {
                  // value file size is bytes
                  response.size = new_image.size;
                  response.uri = new_image.uri;
                  response.name = new_image.name;
                  response.type = 'image/jpeg';

                  this.setState({
                    imageHeight:
                      (response.height / response.width) *
                      this.state.imageWidth,
                    file: response,
                  });
                } else {
                  ToastAndroid.show(
                    ' ファイルサイズ > 5MB ',
                    ToastAndroid.LONG,
                  );
                }
              }
            },
          );
        } else {
          ShowAlert.alertRequestPermissionPhotos();
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  _checkNGWords = text => {
    const ngWords = this.props.systemStore.server_state.ng_words.result;
    ngWords.forEach(ng_word => {
      let regEx = new RegExp(ng_word, 'ig');
      text = text.replace(
        regEx,
        this.props.systemStore.server_state.ng_words_replace,
      );
    });
    this.setState({content: text});
  };

  render() {
    return (
      <Container>
        <TouchableWithoutFeedback onPress={dismissKeyboard}>
          <View>
            <View style={style.header_label}>
              <Text style={style.text_label}>
                書き込み内容({150 - this.state.content.length}文字以内)
              </Text>
            </View>

            <View style={style.containTextInput}>
              <Textarea
                rowSpan={6}
                style={style.textInput}
                inputAccessoryViewID={'input'}
                placeholderTextColor={MyColor.secondary_color}
                placeholder="入力してください"
                ref={ref => {
                  this.input = ref;
                }}
                onChangeText={value => this._checkNGWords(value)}
                value={this.state.content}
                maxLength={150}
              />
              {this.state.file ? (
                <Image
                  style={{
                    alignSelf: 'flex-end',
                    width: this.state.imageWidth,
                    height: this.state.imageHeight,
                  }}
                  source={{uri: this.state.file.uri}}
                />
              ) : null}
            </View>
            <View style={style.containButton}>
              <View style={[style.button, {paddingRight: 5}]}>
                <Button transparent onPress={() => this._onSubmitPost()}>
                  <View style={{height: 45, width: '100%', overflow: 'hidden'}}>
                    <View style={style.background_button}>
                      <Image
                        source={AssetImages.ic_pencil}
                        style={{height: 18, width: 24}}
                        resizeMode="contain"
                      />
                      <Text style={style.text_button}>投稿する</Text>
                    </View>
                  </View>
                </Button>
              </View>

              <View style={[style.button, {paddingLeft: 5}]}>
                <Button transparent onPress={() => this._onPressChooseFile()}>
                  <View style={{height: 45, width: '100%', overflow: 'hidden'}}>
                    <View style={style.background_button}>
                      <Image
                        source={AssetImages.ic_image}
                        style={{height: 18, width: 24}}
                        resizeMode="contain"
                      />
                      <Text style={style.text_button}>写真添付</Text>
                    </View>
                  </View>
                </Button>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}

export default inject('systemStore', 'keijibanAction', 'pointStore')(
  observer(KeijibanPostScreen),
);

const style = StyleSheet.create({
  header_label: {
    width: '100%',
    padding: 5,
    backgroundColor: MyColor.main_color,
  },
  text_label: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
  },
  containTextInput: {
    flexDirection: 'column',
    borderRadius: 10,
    borderWidth: 1.5,
    borderColor: '#d3d3d3',
    marginTop: 15,
    margin: 8,
    padding: 2,
    backgroundColor: 'white',
  },
  textInput: {
    textAlign: 'left',
  },
  containButton: {
    height: 50,
    flexDirection: 'row',
    paddingHorizontal: 8,
  },
  button: {
    flex: 5,
    paddingTop: 20,
    height: 45,
  },
  background_button: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: MyColor.main_color,
  },
  text_button: {
    marginLeft: 5,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
});
