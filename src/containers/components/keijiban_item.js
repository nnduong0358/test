import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Dimensions, Text
} from 'react-native';
import { inject, observer } from "mobx-react";
import { Button } from 'native-base';
import * as ShowAlert from '../../services/show_alert';
import AssetImages from '../../services/constants/asset_images';
import MyColor from '../../assets/MyColor';
import CommonStyle from '../../assets/styles/common.style';
import { checkCacheImage } from '../../services/helpers';

class KeijibanItem extends Component {
  constructor(props) {
    super(props);

    this.state = { image: '' };

    this.getCacheImage();
  }

  async getCacheImage() {
    let image = JSON.parse(this.props.item.avatar_url);
    this.setState({ image: await checkCacheImage(image[0].path) });
  }

  _onClickItem = () => {
    if (this.props.pointAction.checkUserEnoughPoint('discuss_view')) {
      this.props.keijibanAction.getKeijibanDetail(this.props.item);
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  }

  render() {
    const { server_state } = this.props.systemStore;

    return (
      <View style={[CommonStyle.container, this.props.index == 0
        ? { borderTopWidth: 1, borderTopColor: MyColor.secondary_color } : {}]}>
        <View style={CommonStyle.user_block}>
          <View style={CommonStyle.block_avatar}>
            {
              this.state.image
                ?
                <Image style={CommonStyle.avatar}
                  defaultSource={this.props.item.sex == '0'
                    ? AssetImages.default_man
                    : AssetImages.default_woman
                  }
                  source={{ uri: this.state.image }} />
                : null
            }
          </View>

          <View style={CommonStyle.block_info}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Text numberOfLines={1} style={[CommonStyle.text_style,
                { color: MyColor.main_color, fontWeight: '700' }]}>
                  {this.props.item.displayname}
                </Text>
                {
                  this.props.item.origin
                    ? <Image style={{ marginLeft: 5, width: 20, height: 20 }}
                      resizeMode='contain' source={AssetImages.ic_camera} />
                    : null
                }
              </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 5 }}>
              <View style={{ flex: 5, justifyContent: 'flex-start' }}>
                <Text numberOfLines={1} style={CommonStyle.text_style}>
                  {this.props.item.area_name + ' '}
                  {
                    this.props.item.age
                      ? server_state.user_profile_list.age[parseInt(this.props.item.age)].name
                      : ''
                  }
                </Text>
              </View>
              <View style={{ flex: 5, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                <Button style={{
                  padding: 10, flexDirection: 'row', backgroundColor: 'yellow',
                  borderWidth: 1, borderColor: 'red', borderRadius: 15, height: 28
                }}
                  onPress={this._onClickItem}>
                  <Text style={{ color: 'red', fontSize: 11, fontWeight: 'bold' }}>
                    投稿を確認
                  </Text>
                </Button>
              </View>
            </View>

            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
              <View style={CommonStyle.arrow} />
              <View style={CommonStyle.block_user_status}>
                <Text numberOfLines={2} style={[CommonStyle.text_style, { fontSize: 13 }]}>
                  {this.props.item.content}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default inject('systemStore', 'keijibanAction', 'keijibanStore', 'pointAction')(
  observer(KeijibanItem)
);
