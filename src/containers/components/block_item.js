import React, { Component } from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import { Button } from 'native-base';
import { inject, observer } from "mobx-react";
import AssetImages from '../../services/constants/asset_images';
import { checkCacheImage } from '../../services/helpers';

class BlockItem extends Component {
  constructor(props) {
    super(props);

    this.state = { image: '' };

    this.getCacheImage();
  }

  async getCacheImage() {
    this.setState({ image: await checkCacheImage(this.props.item.avatar_url) });
  }

  _clickUnblockUser = (user_code, id) => {
    this.props.userAction.blockUser(id, user_code, 0, this.props.index);
  }

  render() {
    return (
      <View style={styles.seen_content}>
        <View style={{ flex: 1, flexDirection: 'row', marginVertical: 10, alignItems: 'center' }}>
          <View style={styles.avatar_content}>
            {
              this.state.image
                ?
                <Image style={styles.user_avatar}
                  defaultSource={this.props.item.sex == '0'
                    ? AssetImages.default_man
                    : AssetImages.default_woman
                  }
                  source={{ uri: this.state.image }} />
                : null
            }
          </View>
          <View style={styles.user_info}>
            <Text numberOfLines={1} style={{ fontSize: 16 }}>
              {this.props.item.displayname}
            </Text>
          </View>
          <Button transparent style={[styles.buttonUnblock, { height: 30 }]}
            onPress={() => this._clickUnblockUser(this.props.item.user_code, this.props.item.id)}>
            <Text style={{ color: 'red' }}>ブロック解除</Text>
          </Button>
        </View>
      </View>
    )
  }
}

export default inject(
  'userAction',
)(observer(BlockItem));

const styles = StyleSheet.create({
  seen_content: {
    borderBottomWidth: 1,
    borderBottomColor: '#999999',
    paddingRight: 15
  },
  avatar_content: {
    height: 45,
    width: 45,
    marginRight: 10,
    marginLeft: 15,
  },
  user_avatar: {
    height: 45,
    width: 45,
    borderRadius: 45 / 2
  },
  user_info: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonUnblock: {
    padding: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'red',
    height: 30
  }
});
