import React, { Component } from 'react';
import {
  Image, View, Dimensions, ActivityIndicator
} from 'react-native';
import { observer, inject } from "mobx-react";
import { createGetDataFromParamObject } from '../../services/helpers';
import AppConfig from '../../services/constants/app_config';
import MyColor from '../../assets/MyColor';

const windowWidth = Dimensions.get('window').width;
const width = parseInt(windowWidth / 2.5);
const height = parseInt(windowWidth / 3);

class BubbleImage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      uri: this.props.message.type == 'location'
        ?
        `https://maps.googleapis.com/maps/api/staticmap?center=${this.props.message.param.lat},${this.props.message.param.long}&zoom=13&size=${width}x${height}&maptype=roadmap&markers=color:red%7C${this.props.message.param.lat},${this.props.message.param.long}&key=${AppConfig.API_KEY_MAP_STATIC}`
        :
        undefined
    }

    if (this.props.message.type == 'image') this._loadImage();
  }

  _loadImage = () => {
    let headers = this.props.systemStore.getApiHeader;
    let params = {
      id: this.props.message.param.media_id,
      token: this.props.systemStore.login_token,
      size: 'medium',
      thumbnail: true
    };

    fetch(AppConfig.API_URL + 'media/stream' + createGetDataFromParamObject(params),
      {
        method: 'GET',
        headers: headers,
      }
    ).then(response => {
      this.setState({ uri: response.url });
    }).catch(error => {
      console.log(error);
    });
  }

  render() {
    return (
      <View>
        {
          this.state.uri
            ?
            this.props.message.u_id == this.props.systemStore.getUserLoginData.id ||
            this.props.message.u_id == this.props.systemStore.server_state.supporter.id
              ?
              <Image source={{ uri: this.state.uri }} 
                style={{ width: width, height: height }} />
              :
              <Image source={{ uri: this.state.uri }} blurRadius={this.props.message.showed == 1 ? 0 : 1}
                style={{ width: width, height: height }}  />
            :
            <View style={{ width: width, height: height, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="large" color={MyColor.main_color} />
            </View>
        }
      </View>
    );
  }
}

export default inject('systemStore')(observer(BubbleImage));
