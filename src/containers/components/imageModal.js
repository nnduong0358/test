import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Modal,
  SafeAreaView,
  Platform
} from 'react-native';
import { inject, observer } from "mobx-react";
import { Icon } from 'native-base';
import ReportPopup from './popupReportUser';
import ImageZoom from 'react-native-image-pan-zoom';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import * as Helpers from '../../services/helpers';

import MyColor from '../../assets/MyColor';

let screenWidth = Dimensions.get('window').width;
let screenHeight = Dimensions.get('window').height;
const deviceHeight = ExtraDimensions.getRealWindowHeight() - ExtraDimensions.getStatusBarHeight();

class ImageModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    }
  }

  render() {
    const { image_popup } = this.props.systemStore;

    return (
      <Modal transparent={false} visible={image_popup.show}
        onRequestClose={() => this.props.systemStore.image_popup.show = false }>
        <ReportPopup />
        <SafeAreaView style={[{ flex: 1 }, Helpers.isIphoneX() ? { marginBottom: 34 } : {}]}>
          <View style={[styles.modal_container, Platform.OS === 'android'
            ?
            { height: deviceHeight }
            :
            null
            ]}>
            {
              image_popup.screen == 'profile'
                ?
                <TouchableWithoutFeedback 
                  onPress={() => {
                    this.props.userStore.report_user.id = this.props.userStore.user_profile.id;
                    this.props.userStore.report_user.user_code = this.props.userStore.user_profile.user_code;
                    this.props.userStore.report_user.screen = 'avatar';
                    this.props.userStore.report_user.show = true;
                  }}>
                  <Icon style={styles.icon_warning} type='FontAwesome' name='exclamation-triangle' />
                </TouchableWithoutFeedback>
                :
                null
            }
            <TouchableWithoutFeedback onPress={() => 
              this.props.systemStore.image_popup = {
                show: false,
                uri: '',
                screen: '',
                loading: false
              }
            }>
              <Icon style={styles.icon} type='FontAwesome' name='times-circle' />
            </TouchableWithoutFeedback>
            {
              image_popup.loading
                ?
                <ActivityIndicator size='large' color={MyColor.main_color} />
                :
                <ImageZoom cropWidth={screenWidth} cropHeight={screenHeight}
                  imageWidth={screenWidth} imageHeight={screenHeight} enableSwipeDown={true}
                  onSwipeDown={() => this.props.systemStore.image_popup = {
                    show: false,
                    uri: '',
                    screen: '',
                    loading: false
                  }}>
                  {
                    image_popup.uri
                      ? <Image style={{ height: screenHeight, width: screenWidth, resizeMode: 'contain' }}
                          source={{ uri: image_popup.uri }} />
                      : null
                  }
                </ImageZoom>
            }
          </View>
        </SafeAreaView>
      </Modal>
    )
  }
}


export default inject(
  'userAction', 'userStore', 'systemStore'
)(observer(ImageModal))

const styles = StyleSheet.create({
  modal_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    backgroundColor: 'black'
  },
  icon: {
    position: 'absolute',
    top: 5,
    right: 5,
    color: 'white',
    zIndex: 1,
    fontSize: 25
  },
  icon_warning: {
    position: 'absolute',
    top: 7,
    right: 35,
    color: 'white',
    zIndex: 1,
    fontSize: 23
  }
});
