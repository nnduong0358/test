import React, {Component} from 'react';
import {
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  Dimensions,
  View,
} from 'react-native';
import {observer, inject} from 'mobx-react';

const width = Dimensions.get('window').width - 20;
const height = width / 4.32;

class PointItem extends Component {
  render() {
    return (
      <View style={styles.container}>
        {this.props.item.rank_package_img ? (
          <Image
            style={styles.rank_package_img}
            source={{uri: this.props.item.rank_package_img}}
          />
        ) : null}
        <TouchableWithoutFeedback
          style={{flex: 1}}
          onPress={() =>
            this.props.pointAction.buyPoint(
              this.props.item.identifier,
              this.props.item.amount,
            )
          }>
          {this.props.item.package_img ? (
            <Image
              style={{width: width, height: height}}
              resizeMode="contain"
              source={{uri: this.props.item.package_img}}
            />
          ) : (
            <View style={{width: width, height: height}} />
          )}
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    position: 'relative',
    marginTop: 5,
    paddingTop: 5,
  },
  rank_package_img: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 30,
    height: 34,
    zIndex: 999,
  },
});

export default inject('pointAction')(observer(PointItem));
