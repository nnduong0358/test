import React, { Component } from 'react';
import {
  StyleSheet, View, Text
} from 'react-native';
import {
  Button
} from 'native-base';
import { inject, observer } from "mobx-react";
import MyColor from '../../assets/MyColor';

class HeaderMessageTab extends Component {
  render() {
    const {getUnread} = this.props.messageStore;

    return (
      <View style={styles.navigation_container}>

        <View style={{flex: 3, position: 'relative',}}>
          <Button transparent style={[styles.container_button, {
            borderTopLeftRadius: 8, borderBottomLeftRadius: 8,
            borderColor: MyColor.secondary_color, borderWidth: 1,
            backgroundColor: 'white', justifyContent: 'center'}, 
              this.props.navigation.state.index === 0
                ? { backgroundColor: MyColor.main_color }
                : { backgroundColor: 'white' }
            ]}
            active={this.props.navigation.state.index === 0}
            onPress={() => this.props.navigation.navigate('Roster')}>
              <Text style={[styles.button_text, this.props.navigation.state.index === 0
                ? {} : {color: MyColor.secondary_color} ]}>
                チャット
              </Text>
          </Button>
          {
            getUnread.total_unread_message == 0
              ?
              null
              :
              <View style={styles.pop}>
                <Text style={styles.pop_text}>
                  {getUnread.total_unread_message}
                </Text>
              </View>
          }
        </View>

        <View style={{flex: 3, position: 'relative'}}>
          <Button transparent style={[styles.container_button, {
              borderColor: MyColor.secondary_color, borderWidth: 1, borderLeftWidth: 0,
              backgroundColor: 'white', justifyContent: 'center'}, 
              this.props.navigation.state.index === 1
                ? { backgroundColor: MyColor.main_color }
                : { backgroundColor: 'white' }
            ]}
            active={this.props.navigation.state.index === 0}
            onPress={() => this.props.navigation.navigate('Notice')}>
              <Text style={[styles.button_text, this.props.navigation.state.index === 1
                ? {} : {color: MyColor.secondary_color} ]}>
                お知らせ
              </Text>
          </Button>
          {
            getUnread.total_unread_notice == 0
              ?
              null
              :
              <View style={styles.pop}>
                <Text style={styles.pop_text}>
                  {getUnread.total_unread_notice}
                </Text>
              </View>
          }
        </View>

        <View style={{flex: 3}}>
          <Button transparent style={[styles.container_button, {
              borderBottomRightRadius: 8, borderTopRightRadius: 8,
              borderWidth: 1, borderLeftWidth: 0, borderColor: MyColor.secondary_color,
              backgroundColor: 'white', justifyContent: 'center'}, 
              this.props.navigation.state.index === 2
                ? { backgroundColor: MyColor.main_color }
                : { backgroundColor: 'white' }
            ]}
            active={this.props.navigation.state.index === 0}
            onPress={() => this.props.navigation.navigate('Campaign')}>
              <Text style={[styles.button_text, this.props.navigation.state.index === 2
                ? {} : {color: MyColor.secondary_color} ]}>
                キャンペーン
              </Text>
          </Button>
          {
            getUnread.total_unread_campaign == 0
              ?
              null
              :
              <View style={styles.pop}>
                <Text style={styles.pop_text}>
                  {getUnread.total_unread_campaign}
                </Text>
              </View>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_container: {
    height: 62,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15
  },
  container_button: {
    height: 45,
    width: '100%'
  },
  background_button: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
  pop: {
    backgroundColor: 'red',
    minWidth: 22,
    height: 22,
    position: 'absolute',
    borderRadius: 25,
    top: -5,
    right: -2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'white',
    zIndex: 10
  },
  pop_text: {
    color: 'white',
    fontSize: 13
  },
});

export default inject('messageStore')(observer(HeaderMessageTab));
