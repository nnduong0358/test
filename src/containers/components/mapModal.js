import React, { Component } from 'react';
import {
  View, StyleSheet, TouchableWithoutFeedback, ActivityIndicator, SafeAreaView, Modal
} from 'react-native';
import {
  Icon
} from 'native-base';
import { observer, inject } from "mobx-react";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import ExtraDimensions from 'react-native-extra-dimensions-android';
import * as Helpers from '../../services/helpers';
import { toJS } from 'mobx';
import MyColor from '../../assets/MyColor';

const deviceHeight =  ExtraDimensions.getRealWindowHeight() - ExtraDimensions.getStatusBarHeight();

class MapModal extends Component {

  render() {
    return (
      <Modal transparent={false} visible={this.props.systemStore.map_popup.show}
        onRequestClose={() => this.props.systemStore.map_popup.show = false }>
        <SafeAreaView style={[{ flex: 1 }, Helpers.isIphoneX() ? { marginBottom: 34 } : {}]}>
          <View style={styles.modal_container}>
            <TouchableWithoutFeedback onPress={() => {
              this.props.systemStore.map_popup.show = false
            }}>
              <Icon style={styles.icon} type="FontAwesome" name="times-circle" />
            </TouchableWithoutFeedback>
            {
              this.props.systemStore.map_popup.loading
                ?
                <ActivityIndicator size="large" color={MyColor.main_color} />
                :
                <MapView provider={PROVIDER_GOOGLE} style={[styles.map, {height: deviceHeight}]}
                  region={toJS(this.props.systemStore.map_popup.location)}>
                  <Marker coordinate={{
                    latitude: this.props.systemStore.map_popup.location.latitude,
                    longitude: this.props.systemStore.map_popup.location.longitude
                  }} />
                </MapView>
            }
          </View>
        </SafeAreaView>
      </Modal>
    )
  }
}

export default inject( 'systemStore' )(observer(MapModal))

const styles = StyleSheet.create({
  modal_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    position: 'absolute',
    top: 5,
    right: 5,
    color: 'white',
    zIndex: 1,
    fontSize: 25
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
