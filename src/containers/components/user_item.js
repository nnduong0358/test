import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Dimensions, TouchableHighlight, ActivityIndicator, Text
} from 'react-native';
import { inject, observer } from "mobx-react";
import ActionSheet from 'react-native-actionsheet';
import { Button } from 'native-base';
import * as ShowAlert from '../../services/show_alert';
import AssetImages from '../../services/constants/asset_images';
import MyColor from '../../assets/MyColor';
import { checkCacheImage } from '../../services/helpers';
import CommonStyle from '../../assets/styles/common.style';

class UserItem extends Component {
  constructor(props) {
    super(props);

    this.state = { image: '' };

    this.getCacheImage();
  }

  async getCacheImage() {
   this.setState({ image: await checkCacheImage(this.props.item.avatar_url) });
  }

  _onClickItem = () => {
    if (this.props.pointAction.checkUserEnoughPoint('view_profile')) {
      this.props.userAction.getUserProfile(this.props.item.id);
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  }

  _showListChooseReport = () => {
    this.ActionSheet.show();
  }

  _onClickItemSheet = index => {
    if (index == 0) {
      this.props.userStore.report_user = {
        id: this.props.item.id,
        user_code: this.props.item.user_code,
        screen: 'avatar',
        show: true
      };
    } else if (index == 1) {
      this.props.userAction.blockUser(
        this.props.item.id,
        this.props.item.user_code,
      );
    }
  }

  render() {
    const first_item = [0, 1, 2, 3];
    const { server_state } = this.props.systemStore;

    if (this.props.is_list)
      return (
        <View style={[CommonStyle.container, this.props.index == 0
          ? { borderTopWidth: 1, borderTopColor: MyColor.secondary_color } : {}]}>
          <View style={CommonStyle.user_block}>
            <View style={CommonStyle.block_avatar}>
              {
                this.state.image
                  ?
                  <Image style={CommonStyle.avatar}
                    defaultSource={this.props.item.sex == '0'
                      ? AssetImages.default_man
                      : AssetImages.default_woman
                    }
                    source={{ uri: this.state.image }} />
                  : null
              }
            </View>
            <View style={CommonStyle.block_info}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <Text numberOfLines={1} style={[CommonStyle.text_style,
                  { color: MyColor.main_color, fontWeight: '700' }]}>
                    {this.props.item.displayname}
                  </Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 5 }}>
                <View style={{ flex: 5, justifyContent: 'flex-start' }}>
                  <Text numberOfLines={1} style={CommonStyle.text_style}>
                    {this.props.item.area_name + ' '}
                    {
                      this.props.item.age
                        ? server_state.user_profile_list.age[parseInt(this.props.item.age)].name
                        : ''
                    }
                  </Text>
                </View>
                <View style={{ flex: 5, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                  <Button style={{
                    paddingHorizontal: 10, flexDirection: 'row', backgroundColor: 'yellow',
                    borderWidth: 1, borderColor: 'red', borderRadius: 15, height: 28
                  }}
                    onPress={this._onClickItem}>
                    <Text style={{ color: 'red', fontSize: 11, fontWeight: 'bold' }}>
                      プロフを確認
                      </Text>
                  </Button>
                </View>
              </View>

              <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                <View style={CommonStyle.arrow} />
                <View style={CommonStyle.block_user_status}>
                  <Text numberOfLines={2} style={[CommonStyle.text_style, { fontSize: 13 }]}>
                    {this.props.item.user_status}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    else
      return (
        <View style={first_item.includes(this.props.index)
          ? { paddingTop: 10, borderTopWidth: 1.5, borderColor: MyColor.secondary_color }
          : { marginTop: 15 }}>
          <TouchableHighlight underlayColor={'rgba(255, 255, 255, 0)'}
            onPress={() => this._onClickItem()}>
            <View style={style.user_block}>
              <View style={{ position: 'relative', paddingBottom: 8 }}>
                {
                  this.state.image
                    ?
                      <Image style={style.user_image} 
                        source={{ uri: this.state.image }} />
                    : 
                    <ActivityIndicator size="large" color={MyColor.main_color} />
                }

                <Button transparent style={style.buttonImage}
                  onPress={() => this._showListChooseReport()}>
                  <Image source={AssetImages.ic_report}
                    style={{ width: 30, height: 30 }} />
                </Button>
              </View>
              <View>
                <View style={style.user_pr}>
                  <Text style={style.user_text}>
                    {this.props.item.area_name + ' '}
                    {this.props.item.age ? server_state.user_profile_list.age[this.props.item.age].name : ''}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableHighlight>
          <ActionSheet ref={o => this.ActionSheet = o}
            options={['このユーザーを通報する', 'このユーザーをブロックする', 'キャンセル']}
            cancelButtonIndex={2}
            onPress={(index) => this._onClickItemSheet(index)} />
        </View>
      );
  }
}

export default inject('systemStore', 'userAction', 'userStore', 'pointAction')(observer(UserItem));

let screenWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  user_block: {
    width: screenWidth / 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  user_image: {
    height: screenWidth / 4.2,
    width: screenWidth / 4.2,
    borderRadius: 12
  },
  buttonImage: {
    position: 'absolute',
    top: -12,
    right: -4
  },
  user_text: {
    color: MyColor.main_color,
    fontSize: 10,
    fontWeight: '700',
    textAlign: 'center'
  },
  user_pr: {
    backgroundColor: 'white',
    width: screenWidth / 4.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
