import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Dimensions, Text
} from 'react-native';
import { Button } from 'native-base';
import { inject, observer } from "mobx-react";
import MyColor from '../../assets/MyColor';
import AssetImages from '../../services/constants/asset_images';
import CommonStyle from '../../assets/styles/common.style';

class CampaignItem extends Component {
  _onClickItem = () => {
    this.props.messageAction.getCampaignDetail(this.props.item.id);
  }

  render() {
    
    return (
      <View style={[CommonStyle.container, this.props.index == 0
        ? { borderTopWidth: 1, borderTopColor: MyColor.secondary_color } : {}]}>
        <View style={CommonStyle.user_block}>
          <View style={CommonStyle.block_avatar}>
            {
              this.props.item.avatar_url
                ? <Image style={CommonStyle.avatar}
                    source={{ uri: this.props.item.avatar_url }} />
                :
                <Image style={CommonStyle.avatar}
                  source={AssetImages.app_ic} />
            }
            {
              this.props.item.is_read == 0
                ?
                <View style={{position: 'absolute', top: 0, right: 5, paddingVertical: 1, paddingHorizontal: 10, 
                  backgroundColor: 'red', borderRadius: 20,}}>
                  <Text style={{ fontSize: 10, color: 'white', fontWeight: 'bold' }}>
                    未読
                  </Text>
                </View>
                : null
            }
          </View>

          <View style={[CommonStyle.block_info, {justifyContent: 'center'}]}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                {/* <Text style={{ color: MyColor.secondary_color, fontSize: 12 }}>
                  {this.props.item.posted_at}
                </Text> */}
              </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5 }}>
              <View style={{ flex: 6, justifyContent: 'flex-start' }}> 
                <Text style={[CommonStyle.text_style,
                  { color: MyColor.main_color, fontWeight: '700', fontSize: 12 }]}>
                  {this.props.item.header}
                </Text>
              </View>
              <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                <Button style={{ paddingVertical: 8, paddingHorizontal: 5, flexDirection: 'row', backgroundColor: 'yellow',
                  borderWidth: 1, borderColor: 'red', borderRadius: 15, height: 28
                }}
                  onPress={this._onClickItem}>
                  <Text style={{ color: 'red', fontSize: 11, fontWeight: 'bold' }}>
                    メッセージ確認
                  </Text>
                </Button>
              </View>
            </View>

            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
              <View style={CommonStyle.arrow} />
              <View style={CommonStyle.block_user_status}>
                <Text numberOfLines={2} style={[CommonStyle.text_style, { fontSize: 13 }]}>
                {this.props.item.title}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default inject('messageAction')(
  observer(CampaignItem)
);
