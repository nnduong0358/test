import React, { Component } from 'react';
import {
  View, Text, StyleSheet
} from 'react-native';

export default class PointTableItem extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text numberOfLines={1} style={styles.point_name}>
          {this.props.item.name}
        </Text>
        <Text numberOfLines={1} style={styles.point}>
          {this.props.item.point} Pt
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: '#dddddd',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10
  },
  point_name: {
    color: 'black',
    flexGrow: 1
  },
  point: {
    color: 'black'
  }
});
