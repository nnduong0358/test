import React, { PureComponent } from 'react';
import {
  View, Text, Image, StyleSheet, TouchableHighlight, Dimensions
} from 'react-native';
import { observer, inject } from "mobx-react";
import ChattingImage from './bubble_image';
import { timeToString, checkCacheImage } from '../../services/helpers';
import Autolink from 'react-native-autolink';
import * as ShowAlert from '../../services/show_alert';
import MyColor from '../../assets/MyColor';
import AssetImages from '../../services/constants/asset_images';

let screenWidth = Dimensions.get('window').width;

class ChatBubbleItem extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { avatar: '' };
  }

  async componentDidMount() {
    this.setState({ avatar: await checkCacheImage(this.props.messageStore.getChattingUser.avatar_url 
        || this.props.userStore.user_profile.image[0].path
    )});
  }

  _clickViewImage = (media_id, msg_id) => {
    this.props.messageAction.userClickViewImage(media_id, msg_id, false);
  }

  _clickViewImageMinusPoint = async (message) => {
    if (this.props.pointAction.checkUserEnoughPoint('view_media_chat_image')) {
      this.props.socketIO.setOpenImageMessage(message); // call socket khi open image
      this.props.messageAction.userClickViewImage(message.param.media_id, message.msg_id, true);

      this.props.messageStore.message_chatting_list.data[this.props.index].showed = 1;
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }

  }

  _clickViewLocation = (sender_id, msg_id, lat, long) => {
    this.props.messageAction.userClickViewLocation(sender_id, msg_id, lat, long, false);
  }

  _clickViewLocationMinusPoint = async (message) => {
    if (this.props.pointAction.checkUserEnoughPoint('chat_view_location')) {
      this.props.messageAction.userClickViewLocation(message.u_id, message.msg_id, message.param.lat, message.param.long, true);
      this.props.messageStore.message_chatting_list.data[this.props.index].showed = 1;
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  }

  render() {
    const ngWords = this.props.systemStore.server_state.ng_words.result;
    ngWords.forEach(ng_word => {
      let regEx = new RegExp(ng_word, "ig");
      this.props.item.msg = this.props.item.msg.replace(regEx, this.props.systemStore.server_state.ng_words_replace);
    });

    return (
      <View
        style={this.props.item.u_id == this.props.systemStore.getUserLoginData.id 
          ? style.chat_block_me : style.chat_block}>
        {
          this.props.item.u_id == this.props.systemStore.getUserLoginData.id
            ?
            null
            :
            <View style={style.avatar_content}>
              {
                this.state.avatar
                  ?
                  <Image style={style.footer_avatar} 
                    defaultSource={this.props.userStore.user_profile.sex == '0'
                      ? AssetImages.default_man
                      : AssetImages.default_woman
                    }
                    source={{ uri: this.state.avatar }} />
                  : null
              }
            </View>
        }
        <View style={this.props.item.u_id == this.props.systemStore.getUserLoginData.id ? style.flexEnd : style.flexStart}>
          <View style={this.props.item.u_id == this.props.systemStore.getUserLoginData.id ? style.arrow_me : style.arrow} />
          <View style={{ flexDirection: 'row' }}>
            {
              (this.props.systemStore.getUserLoginData.id == this.props.item.u_id && this.props.item.is_read == 0)
                ?
                <View style={style.seen}>
                  <Text style={{ color: '#555555' }}>既読</Text>
                </View>
                :
                null
            }
            <View style={this.props.item.u_id == this.props.systemStore.getUserLoginData.id ? style.message_me : style.message}>
              {
                this.props.item.type == 'text'
                  ?
                  <Autolink text={this.props.item.msg} truncate={0} stripPrefix={false} selectable={true}
                    style={{ color: 'black', lineHeight: parseInt(15 * 1.5), fontSize: 15 }} />
                  :
                  this.props.item.type == 'image'
                    ?
                    <TouchableHighlight underlayColor={'rgba(255, 255, 255, 0.8)'}
                      onPress={() => this._clickViewImage(this.props.item.param.media_id, this.props.item.msg_id)}>
                      <ChattingImage message={this.props.item} />
                    </TouchableHighlight>
                    :
                    <TouchableHighlight underlayColor={'rgba(255, 255, 255, 0.8)'}
                      onPress={() => this._clickViewLocation(this.props.item.u_id, this.props.item.msg_id, this.props.item.param.lat, this.props.item.param.long)}>
                      <ChattingImage message={this.props.item} />
                    </TouchableHighlight>
              }
              {
                this.props.item.u_id == this.props.systemStore.getUserLoginData.id
                  ?
                  null
                  :
                  this.props.item.showed == 0 && this.props.item.type == 'image' &&
                    this.props.systemStore.server_state.supporter.id != this.props.item.u_id
                    ?
                    <TouchableHighlight underlayColor={'rgba(255, 255, 255, 0.8)'}
                      style={style.overlayImage}
                      onPress={() => this._clickViewImageMinusPoint(this.props.item)}>
                      <Image style={{ width: parseInt(screenWidth / 2.5), height: parseInt(screenWidth / 3) }}
                        resizeMode='contain'
                        source={AssetImages.img_cover} />
                    </TouchableHighlight>
                    :
                    this.props.item.showed == 0 && this.props.item.type == 'location' &&
                      this.props.systemStore.server_state.supporter.id != this.props.item.u_id
                      ?
                      <TouchableHighlight underlayColor={'rgba(255, 255, 255, 0.8)'}
                        style={style.overlayImage}
                        onPress={() => this._clickViewLocationMinusPoint(this.props.item)}>
                        <Image style={{ width: parseInt(screenWidth / 2.5), height: parseInt(screenWidth / 3) }}
                          resizeMode='contain'
                          source={AssetImages.img_cover} />
                      </TouchableHighlight>
                      :
                      null
              }
            </View>
          </View>
          <View style={[{ paddingTop: 5, width: '100%' },
          this.props.item.u_id == this.props.systemStore.getUserLoginData.id
            ? { paddingLeft: 5, alignItems: 'flex-start' }
            : { paddingRight: 3, alignItems: 'flex-end' }
          ]}>
            <Text style={{ color: MyColor.secondary_color, fontWeight: 'bold', fontSize: 12 }}>
              {timeToString(this.props.item.time)}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}


export default inject(
  'systemStore', 'messageAction', 'messageStore', 'socketIO', 'userStore', 'pointAction')(
    observer(ChatBubbleItem)
);

const style = StyleSheet.create({
  footer_avatar: {
    height: screenWidth / 10,
    width: screenWidth / 10,
    borderRadius: (screenWidth / 10) / 2
  },
  chat_block: {
    flexDirection: 'row',
    marginBottom: 10
  },
  avatar_content: {
    height: screenWidth / 10,
    width: screenWidth / 10,
    marginRight: 10,
  },
  message: {
    maxWidth: screenWidth / 1.5,
    borderRadius: 8,
    padding: 5,
    marginTop: 10,
    marginLeft: 7,
    position: 'relative',
    zIndex: 1,
    backgroundColor: '#F7D2DD',
    paddingLeft: 10
  },
  message_text: {
    color: 'black',
    fontSize: 16
  },
  arrow: {
    height: 14,
    width: 14,
    transform: [
      { rotateX: '45deg' },
      { rotateZ: '0.785398rad' }
    ],
    position: 'relative',
    zIndex: 8,
    marginBottom: -20,
    backgroundColor: '#F7D2DD',
    top: 12
  },
  chat_block_me: {
    flexDirection: 'row',
    marginBottom: 10,
    justifyContent: 'flex-end'
  },
  arrow_me: {
    height: 14,
    width: 14,
    transform: [
      { rotateX: '45deg' },
      { rotateZ: '0.785398rad' }
    ],
    position: 'relative',
    zIndex: 8,
    marginBottom: -20,
    backgroundColor: MyColor.form_register_color,
    top: 12,
    right: 7
  },
  message_me: {
    maxWidth: screenWidth / 1.5,
    borderRadius: 8,
    padding: 5,
    marginTop: 10,
    marginRight: 14,
    position: 'relative',
    zIndex: 1,
    backgroundColor: MyColor.form_register_color,
    paddingRight: 10,
  },
  avatar_content_me: {
    marginLeft: 5,
    height: screenWidth / 10,
    width: screenWidth / 10,
  },
  seen: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    marginTop: 10
  },
  overlayImage: {
    position: 'absolute',
    top: 5,
    left: 10,
    bottom: 5,
    right: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.7)'
  },
  flexEnd: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  flexStart: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
