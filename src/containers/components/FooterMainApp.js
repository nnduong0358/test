import React, { Component } from 'react';
import {
  StyleSheet, Image, Dimensions, View, Text, ActivityIndicator, Keyboard
} from 'react-native';
import { Button } from 'native-base';
import { inject, observer } from "mobx-react";
import NavigatorService from '../../services/navigator/navigation_services';
import MyColor from '../../assets/MyColor';
import AssetImages from '../../services/constants/asset_images';

class FooterMainApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      footer_hidden: false
    },

      props.navigation.addListener('willFocus', () => {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
      });

    props.navigation.addListener('willBlur', () => {
      this.keyboardDidShowListener.remove();
      this.keyboardDidHideListener.remove();
    });
  }

  _keyboardDidShow = () => {
    this.setState({footer_hidden: true});
  }

  _keyboardDidHide = () => {
    this.setState({footer_hidden: false});
  }

  render() {
    let { getLoading, is_connected_internet } = this.props.systemStore;
    const { getUnread } = this.props.messageStore;
    
    console.log('loading', getLoading);

    const index_active_user = [0];
    const index_active_message = [3, 4];
    const index_active_keijiban = [5, 6, 7];
    const index_active_point = [8];
    const index_active_mypage = [9];
    const index_hide_menu = [1, 2, 10, 11, 12, 13, 14, 15, 16]

    return (
      <View>
        {
          getLoading
            ?
            <View style={[{ flexDirection: 'row' }, styles.loading_no_internet]}>
              <View style={styles.containIndicator}>
                <ActivityIndicator size='large' color={MyColor.main_color} />
              </View>
            </View>
            :
            null
        }
        {
          is_connected_internet
            ?
            null
            :
            <View style={[{ flexDirection: 'column', backgroundColor: 'rgba(255, 255, 255, 0.9)' },
            styles.loading_no_internet]}>
              <Text style={{ color: MyColor.main_color, fontWeight: '700' }}>
                インターネット設定を確認して下さい。
              </Text>
              <Image style={{ height: 25, resizeMode: 'contain' }}
                source={AssetImages.straight_loader} />
            </View>
        }
        <View style={this.state.footer_hidden || index_hide_menu.includes(this.props.navigation.state.index)
          ? { display: 'none' } : {}
        }>

          <View style={styles.footer_tab}>
            {/* USER TAB */}
            <Button transparent activeOpacity={0} disabled={getLoading}
              style={[styles.item_menu,
              index_active_user.includes(this.props.navigation.state.index)
                ? { backgroundColor: '#B5B5B5' } : {}
              ]}
              onPress={() => {
                this.props.systemAction.resetLastScreen('UserList');
                NavigatorService.navigate("UserList");
              }}>
              <Image style={styles.footer_image} source={
                index_active_user.includes(this.props.navigation.state.index)
                  ? AssetImages.user_tab_on
                  : AssetImages.user_tab_off
              } />
            </Button>
            {/* MESSAGE TAB */}
            <Button transparent activeOpacity={0} disabled={getLoading}
              style={[styles.item_menu, 
                index_active_message.includes(this.props.navigation.state.index)
                  ? {backgroundColor: '#B5B5B5'} : {} 
              ]}
              onPress={() => {
                this.props.systemAction.resetLastScreen('MessageNavigator');
                NavigatorService.navigate("MessageNavigator");
              }}>
            <Image style={styles.footer_image} source={
              index_active_message.includes(this.props.navigation.state.index)
                ? AssetImages.message_tab_on
                : AssetImages.message_tab_off
            } />
            { 
              getUnread.total_unread_all == 0
                ? null
                :
                <View style={styles.pop}>
                  <Text style={styles.pop_text}>
                    {getUnread.total_unread_all}
                  </Text>
                </View>
            }
          </Button>
            {/* KEIJIBAN TAB */}
            <Button transparent activeOpacity={0} disabled={getLoading}
              style={[styles.item_menu,
              index_active_keijiban.includes(this.props.navigation.state.index)
                ? { backgroundColor: '#B5B5B5' } : {}
              ]}
              onPress={() => {
                this.props.systemAction.resetLastScreen('KeijibanList');
                NavigatorService.navigate("KeijibanList");
              }}>
              <Image style={styles.footer_image} source={
                index_active_keijiban.includes(this.props.navigation.state.index)
                  ? AssetImages.keijiban_tab_on
                  : AssetImages.keijiban_tab_off
              } />
            </Button>
            {/* POINT TAB */}
            <Button transparent activeOpacity={0} disabled={getLoading}
              style={[styles.item_menu,
              index_active_point.includes(this.props.navigation.state.index)
                ? { backgroundColor: '#B5B5B5' } : {}
              ]}
              onPress={() => {
                this.props.systemAction.resetLastScreen('PointScreen');
                NavigatorService.navigate("PointScreen");
              }}>
              <Image style={styles.footer_image} source={
                index_active_point.includes(this.props.navigation.state.index)
                  ? AssetImages.point_tab_on
                  : AssetImages.point_tab_off
              } />
            </Button>
            {/* MYPAGE TAB */}
            <Button transparent activeOpacity={0} disabled={getLoading}
              style={[styles.item_menu,
              index_active_mypage.includes(this.props.navigation.state.index)
                ? { backgroundColor: '#B5B5B5' } : {}
              ]}
              onPress={() => {
                this.props.systemAction.resetLastScreen('MyPage');
                NavigatorService.navigate("MyPage");
              }}>
              <Image style={styles.footer_image} source={
                index_active_mypage.includes(this.props.navigation.state.index)
                  ? AssetImages.mypage_tab_on
                  : AssetImages.mypage_tab_off
              } />
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

let screenWidth = Dimensions.get('window').width;
let screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  footer_tab: {
    backgroundColor: 'white',
    zIndex: 999,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: '#999999',
  },
  item_menu: {
    borderRadius: 0,
    width: screenWidth / 5,
    height: 55,
    backgroundColor: 'white',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footer_image: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain'
  },
  pop: {
    backgroundColor: 'red',
    minWidth: 25,
    height: 25,
    position: 'absolute',
    borderRadius: 25,
    top: 2,
    right: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  pop_text: {
    color: 'white',
    fontSize: 13
  },
  containIndicator: {
    width: 80,
    height: 80,
    borderRadius: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading_no_internet: {
    zIndex: 10,
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: screenWidth,
    height: screenHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBtn: {
    fontSize: 8,
    fontWeight: 'bold',
    paddingLeft: 2
  }
});

export default inject('systemAction', 'systemStore', 'messageStore')(observer(FooterMainApp));