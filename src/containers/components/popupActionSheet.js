import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Modal, Dimensions
} from 'react-native';
import { inject, observer } from "mobx-react";
import { Button } from 'native-base';

class PopupActionSheet extends Component {
  render() {
    const { popup_action_sheet, list_report_popup } = this.props.userStore;

    return (
      <Modal transparent={true} visible={popup_action_sheet}
        onRequestClose={() => { this.props.userStore.popup_action_sheet = false }}>
        <View style={styles.modal_container}>
          <View style={[styles.modal_content, list_report_popup.no_block ? {} : { height: 100 }]}>
            <Button transparent style={{ height: 50 }} onPress={
              () => {
                // hide this popup
                this.props.userStore.popup_action_sheet = false;
                // show popup report
                this.props.userStore.report_user ={
                  id: list_report_popup.id,
                  user_code: list_report_popup.user_code,
                  screen: list_report_popup.screen,
                  show: true,
                  keijiban_id: list_report_popup.keijiban_id
                };
              }
            }>
              <View style={styles.button}>
                <Text style={styles.text}>このユーザーを通報する</Text>
              </View>
            </Button>
            {
              list_report_popup.no_block
                ?
                <Button transparent style={[{ height: 50 }, styles.mid_button]} onPress={
                  () => {
                    // goi ham block user
                    this.props.userAction.blockUser(
                      list_report_popup.id,
                      list_report_popup.user_code,
                    );
                    // hide popup
                    this.props.userStore.popup_action_sheet = false;
                  }
                }>
                  <View style={styles.button}>
                    <Text style={styles.text}>このユーザーをブロックする</Text>
                  </View>
                </Button>
                :
                null
            }
            <Button transparent style={{ height: 50 }} onPress={
              () => this.props.userStore.popup_action_sheet = false
            }>
              <View style={styles.button}>
                <Text style={[styles.text,{ fontWeight: '500' }]}>キャンセル</Text>
              </View>
            </Button>
          </View>
        </View>
      </Modal>
    )
  }
}

export default inject(
  'userAction', 'userStore'
)(observer(PopupActionSheet));

const widthScreen = Dimensions.get('window').width;

const styles = StyleSheet.create({
  modal_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(85, 85, 85, 0.4)'
  },
  modal_content: {
    paddingLeft: 10,
    paddingRight: 10,
    width: widthScreen * 75 / 100,
    height: 150,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'column'
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mid_button: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#999999'
  },
  text: {
    fontSize: 16
  }
});
