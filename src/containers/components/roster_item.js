import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Dimensions, Text
} from 'react-native';
import { Button } from 'native-base';
import { inject, observer } from "mobx-react";
import AssetImages from '../../services/constants/asset_images';
import MyColor from '../../assets/MyColor';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CommonStyle from '../../assets/styles/common.style';
import { checkCacheImage } from '../../services/helpers';

class RosterItem extends Component {
  constructor(props) {
    super(props);

    this.state = { image: '' };

    this.getCacheImage();
  }

  async getCacheImage() {
    this.setState({ image: await checkCacheImage(this.props.item.avatar_url) });
  }

  _onClickItem = () => {
    this.props.messageAction.openChatting(
      this.props.item.user_id,
      this.props.item.user_code,
      this.props.item.display_name,
      this.props.item.avatar_url
    );
  }

  render() {
    const { server_state } = this.props.systemStore;
    return (
      <View style={[CommonStyle.container, this.props.index == 0
        ? { borderTopWidth: 1, borderTopColor: MyColor.secondary_color } : {}]}>
        <View style={CommonStyle.user_block}>
          <View style={CommonStyle.block_avatar}>
            {
              this.state.image
                ?
                <Image style={CommonStyle.avatar}
                  defaultSource={this.props.item.sex == '0'
                    ? AssetImages.default_man
                    : AssetImages.default_woman
                  }
                  source={{ uri: this.state.image }} />
                : null
            }

            {
              this.props.item.is_read == 2
                ?
                <View style={{
                  position: 'absolute', top: 0, right: 5, paddingVertical: 1, paddingHorizontal: 10,
                  backgroundColor: 'red', borderRadius: 20,
                }}>
                  <Text style={{ fontSize: 10, color: 'white', fontWeight: 'bold' }}>
                    未読
                  </Text>
                </View>
                : null
            }
          </View>

          <View style={CommonStyle.block_info}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 7, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Text numberOfLines={1} style={[CommonStyle.text_style,
                { color: MyColor.main_color, fontWeight: '700' }]}>
                  {this.props.item.displayname}
                </Text>
              </View>
              <View style={{ flex: 3, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                {
                  this.props.item.is_pin_chat == 1
                    ? <Icon size={15} name='pin' color='#000000' />
                    : null
                }
                <Text style={{ color: MyColor.secondary_color, fontSize: 12 }}>
                  {' ' + this.props.item.send_at}
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 5 }}>
              <View style={{ flex: 5, justifyContent: 'flex-start' }}>
                {
                  server_state.supporter.id == this.props.item.user_id
                    ? null
                    :
                    <Text numberOfLines={1} style={CommonStyle.text_style}>
                      {this.props.item.area_name + ' '}
                      {
                        this.props.item.age
                          ? server_state.user_profile_list.age[parseInt(this.props.item.age)].name
                          : ''
                      }
                    </Text>
                }
              </View>
              <View style={{ flex: 5, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                <Button style={{
                  padding: 10, flexDirection: 'row', backgroundColor: 'yellow',
                  borderWidth: 1, borderColor: 'red', borderRadius: 15, height: 28
                }}
                  onPress={this._onClickItem}>
                  <Text style={{ color: 'red', fontSize: 11, fontWeight: 'bold' }}>
                    メッセージ確認
                  </Text>
                </Button>
              </View>
            </View>

            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
              <View style={CommonStyle.arrow} />
              <View style={CommonStyle.block_user_status}>
                <Text numberOfLines={2} style={[CommonStyle.text_style, { fontSize: 13 }]}>
                  {
                    this.props.item.send_type == 'text'
                      ?
                      this.props.item.msg_text
                      :
                      (this.props.item.send_id == this.props.systemStore.user_login_data.id && this.props.item.send_type == 'image')
                        ?
                        "画像を送信しました。"
                        :
                        (this.props.item.send_id == this.props.systemStore.user_login_data.id && this.props.item.send_type == 'location')
                          ?
                          "現在地を送信しました。"
                          :
                          (this.props.item.send_id != this.props.systemStore.user_login_data.id && this.props.item.send_type == 'image')
                            ?
                            "画像を受信しました。"
                            :
                            "現在地を受信しました。"
                  }
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default inject('systemStore', 'messageAction')(
  observer(RosterItem)
);
