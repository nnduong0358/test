import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Modal, Dimensions, TextInput, TouchableWithoutFeedback
} from 'react-native';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { Button } from 'native-base';
import { inject, observer } from "mobx-react";

class PopupReportUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      comment: '',
    };
  }

  render() {
    const { report_user } = this.props.userStore;

    return (
      <Modal transparent={true} visible={report_user.show}
        onRequestClose={() => this.props.userStore.report_user.show = false}>
        <TouchableWithoutFeedback onPress={dismissKeyboard}>
          <View style={style.modal_container}>
            <View style={style.modal_content}>
              <View style={style.modal_title}>
                <Text style={{ fontSize: 18, textAlign: 'center' }}>
                  通報された内容は24時間以内に運用チームが確認します。
              </Text>
              </View>
              <View style={{ padding: 10, marginTop: 10 }}>
                <TextInput placeholder='通報内容を入力してください。'
                  style={{ color: 'black' }}
                  placeholderTextColor={'#999999'}
                  onChangeText={text => this.setState({ comment: text })} />
              </View>
              <View style={style.button_container}>
                <Button transparent style={{ height: 50, width: '50%' }} onPress={() => {
                  this.props.userAction.reportUser(
                    report_user.id,
                    report_user.user_code,
                    report_user.screen,
                    this.state.comment,
                    report_user.keijiban_id
                  );
                  this.setState({ comment: '' });
                  this.props.userStore.report_user.show = false;
                }}>
                  <View style={[style.center, { borderRightColor: '#d9d9d9', borderRightWidth: .5 }]}>
                    <Text style={[style.textButton]}>はい</Text>
                  </View>
                </Button>
                <Button transparent style={{ height: 50, width: '50%' }} onPress={() => {
                  this.props.userStore.report_user.show = false;
                }}>
                  <View style={style.center}>
                    <Text style={[style.textButton, { fontWeight: 'bold' }]}>いいえ</Text>
                  </View>
                </Button>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

export default inject('userStore', 'userAction')(observer(PopupReportUser));

const widthScreen = Dimensions.get('window').width;

const style = StyleSheet.create({
  modal_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(85, 85, 85, 0.4)'
  },
  modal_content: {
    width: widthScreen * 75 / 100,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'column'
  },
  modal_title: {
    padding: 10
  },
  // modal_textarea: {
  //   borderRadius: 5,
  //   marginTop: 5,
  //   borderColor: '#999999',
  //   borderWidth: 1,
  //   backgroundColor: 'white'
  // },
  button_container: {
    marginTop: 10,
    flexDirection: 'row',
    height: 50,
    borderTopWidth: .5,
    borderTopColor: '#d9d9d9',
    width: '100%'
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: '100%'
  },
  modal_textarea: {
    borderRadius: 5,
    marginTop: 10,
    borderColor: '#999999',
    borderWidth: 1,
    backgroundColor: 'white'
  },
  textButton: {
    color: '#3366ff',
    fontSize: 16
  }
});
