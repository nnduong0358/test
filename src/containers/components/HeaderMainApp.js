import React, { Component } from 'react';
import { StyleSheet, Image, Alert, Text } from 'react-native';
import { Title, Button, View } from "native-base";
import { inject, observer } from "mobx-react";
import AssetImage from '../../services/constants/asset_images';
import MyColor from '../../assets/MyColor';
import NavigatorService from '../../services/navigator/navigation_services';
import * as ShowAlert from '../../services/show_alert';

class HeaderMainApp extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { header_app, loading } = this.props.systemStore;
    return (
      <View style={style.header_container}>
        <View style={style.button_back}>
          {
            header_app.refresh_button
              ?
              <Button transparent disabled={loading}
                onPress={() => {
                  header_app.refresh_action.emit('refresh');
                }}>
                <Image style={style.image_header} resizeMode='contain'
                  source={AssetImage.ic_refresh} />
              </Button>
              :
              header_app.back_button
                ?
                <Button transparent disabled={loading}
                  onPress={() => this.props.systemAction.comfirmCancelPopup()}>
                  <Image style={style.image_header} resizeMode='contain'
                    source={AssetImage.ic_back} />
                </Button>
                :
                header_app.cancel_button
                  ?
                  <Button transparent disabled={loading}
                    onPress={() => this.props.systemAction.comfirmCancelPopup()}>
                    <Image style={style.image_header} resizeMode='contain'
                      source={AssetImage.ic_cancel} />
                  </Button>
                  :
                  null
          }
        </View>
        <View style={style.header_title}>
          <Title style={style.header_text}>{header_app.title}</Title>
        </View>

        <View style={style.button_refresh}>
          {
            header_app.search_button
              ?
              <Button style={style.btn_search} disabled={loading}
                onPress={() => {
                  if (header_app.search_action == 'user_search') {
                    NavigatorService.navigate('UserSearch');
                  } else {
                    NavigatorService.navigate('KeijibanSearch');
                  }
                }}>
                <Image style={{ width: 13, height: 13 }} resizeMode='contain'
                  source={AssetImage.ic_search} />
                <Text style={{ color: 'white', fontSize: 12 }}>
                  {` 絞り込み`}
                </Text>
              </Button>
              :
              header_app.message_button
                ?
                <Button style={style.btn_search} disabled={loading}
                  onPress={() => header_app.message_action()}>
                  <Image style={{ width: 13, height: 13 }} resizeMode='contain'
                    source={AssetImage.ic_message} />
                  <Text style={{ color: 'white', fontSize: 11 }}>
                    {`メッセージ`}
                  </Text>
                </Button>
                : header_app.open_profile_button
                  ?
                  <Button style={[style.btn_search, {marginRight: 8, padding: 8}]}
                    disabled={loading}
                    onPress={() => header_app.open_profile_action()}>
                    <Text style={{ color: 'white', fontSize: 11 }}>
                      プロフ確認
                    </Text>
                  </Button>
                  : null
          }
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  header_container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    height: 55,
  },
  header_text: {
    color: 'black',
    fontSize: 16,
    fontWeight: '600'
  },
  header_title: {
    flex: 54,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image_header: {
    width: '80%',
    height: '80%',
    resizeMode: 'contain'
  },
  button_refresh: {
    flex: 23,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  button_back: {
    flex: 23,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain'
  },
  btn_search: {
    padding: 10,
    flexDirection: 'row',
    backgroundColor: MyColor.main_color,
    borderRadius: 15,
    marginRight: 10,
    height: 30,
  }
});

export default inject(
  'systemStore', 'systemAction', 'myPageStore', 'pointAction', 'userAction',
  'messageStore'
)(observer(HeaderMainApp));