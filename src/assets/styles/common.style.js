import {StyleSheet, Dimensions} from 'react-native';
import MyColor from '../MyColor';

let screenWidth = Dimensions.get('window').width;

module.exports = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderBottomWidth: 1.5,
    borderColor: '#d3d3d3',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    width: screenWidth,
  },
  user_block: {
    flex: 1,
    flexDirection: 'row',
    padding: 8,
  },
  block_avatar: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingRight: 15,
    flexDirection: 'row',
    paddingVertical: 5,
    position: 'relative',
  },
  avatar: {
    width: screenWidth / 4,
    height: screenWidth / 4,
    borderRadius: 12,
  },
  block_info: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    paddingVertical: 5,
    paddingRight: 8,
  },
  text_style: {
    fontSize: 13,
    color: 'black',
  },
  arrow: {
    height: 12,
    width: 12,
    borderBottomWidth: 1.5,
    borderLeftWidth: 1.5,
    borderColor: '#d3d3d3',
    transform: [{rotateX: '45deg'}, {rotateZ: '0.785398rad'}],
    position: 'relative',
    zIndex: 8,
    backgroundColor: 'white',
    //top: 12,
  },
  block_user_status: {
    left: -13.5,
    width: '100%',
    borderWidth: 1.5,
    borderColor: '#d3d3d3',
    borderRadius: 6,
    padding: 2,
    marginLeft: 7,
    position: 'relative',
    zIndex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
  },
});
