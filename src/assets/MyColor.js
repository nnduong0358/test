export default (myColor = {
  background_color: 'white',
  main_color: '#E05D94',
  form_register_color: '#DBDBDB',
  secondary_color: '#AAAAAA',
  tertiary_color: '#F9F9F9',
  quaternary_color: '#55CBE1',
  quinary_color: '#5DD3CB',
  message_from_me: '#F8F8F8',
  message_from_user: '#FEECEC',
});
