/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StatusBar,
  BackHandler,
  View,
  StyleSheet,
} from 'react-native';
import {Provider, observer} from 'mobx-react';
import stores from './mobx';
import SplashScreen from 'react-native-splash-screen';
import PushNotification from 'react-native-push-notification';
import firebase from 'react-native-firebase';

import RootNavigation from './services/navigator/root_navigator';
import NavigatorService from './services/navigator/navigation_services';
import {requestGetAPI} from './services/request_api';

import Register from './containers/screens/register.creen';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, {backgroundColor}]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    SplashScreen.hide();
    PushNotification.cancelAllLocalNotifications();
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
    console.log('header apppp:::', stores.systemStore.getApiHeader);
    this.onTokenRefreshListener = firebase
      .messaging()
      .onTokenRefresh(async fcmToken => {
        console.log('[FCM]token renew:::', fcmToken);
        // Process your token as required
        stores.systemStore.api_header['X-PUSH-TOKEN'] = fcmToken;
        await requestGetAPI(
          stores.systemStore.getApiHeader,
          'metadata/push_token',
        );
      });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
    this.onTokenRefreshListener();
  }

  backAction = () => {
    stores.systemAction.comfirmCancelPopup();
    return true;
  };

  setRef = navigatorRef => {
    NavigatorService.setTopLevelNavigator(navigatorRef, stores);
  };

  render() {
    return (
      <Provider {...stores}>
        <SafeAreaView style={{flex: 1}}>
          <MyStatusBar backgroundColor="white" barStyle="dark-content" />
          <RootNavigation ref={this.setRef} />
          {/* <Register /> */}
        </SafeAreaView>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  statusBar: {
    height: StatusBar.currentHeight,
  },
});

export default observer(App);
