import React from 'react';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import HeaderMainApp from '../../containers/components/HeaderMainApp';
import FooterMainApp from '../../containers/components/FooterMainApp';

import Webview from '../../containers/screens/webview.screen';

import UserList from '../../containers/screens/user_page/user_list.screen';
import UserProfile from '../../containers/screens/user_page/user_profile.screen';
import UserSearch from '../../containers/screens/user_page/user_search_screen';

import MessageNavigator from './message_navigatior';
import NoticeDetail from '../../containers/screens/message_page/notice_detail.screen';

import KeijibanList from '../../containers/screens/keijiban_page/keijiban_list.screen';
import KeijibanDetail from '../../containers/screens/keijiban_page/keijiban_detail.screen';
import KeijibanPost from '../../containers/screens/keijiban_page/keijiban_post.screen';
import PointScreen from '../../containers/screens/buy_point_page/point.screen';

import MyPage from '../../containers/screens/mypage_screen/mypage.screen';
import SettingScreen from '../../containers/screens/mypage_screen/setting.screen';
import BlockList from '../../containers/screens/mypage_screen/block_list.screen';
import QaScreen from '../../containers/screens/mypage_screen/QA.screen';
import TransferDevice from '../../containers/screens/mypage_screen/transfer_device.screen';
import Favoritefootprint from '../../containers/screens/mypage_screen/favorite_footprint.screen';
import PointTable from '../../containers/screens/mypage_screen/point_table.screen';
import EditProfile from '../../containers/screens/mypage_screen/edit_profile.screen';
import Chatting from '../../containers/screens/message_page/chatting.screen';
import KeijibanSearch from '../../containers/screens/keijiban_page/keijiban_search.screen';

export default createStackNavigator({
  MainTab: {
    screen: createBottomTabNavigator(
      {
        // main tabs
        UserList: UserList,
        MessageNavigator: MessageNavigator,
        KeijibanList: KeijibanList,
        PointScreen: PointScreen,
        MyPage: MyPage,

        // pages
        UserProfile: UserProfile,
        Webview: Webview,
        UserSearch: UserSearch,
        KeijibanDetail: KeijibanDetail,
        KeijibanPost: KeijibanPost,
        SettingScreen: SettingScreen,
        BlockList: BlockList,
        QaScreen: QaScreen,
        TransferDevice: TransferDevice,
        Favoritefootprint: Favoritefootprint,
        PointTable: PointTable,
        EditProfile: EditProfile,
        NoticeDetail: NoticeDetail,
        Chatting: Chatting,
        KeijibanSearch: KeijibanSearch,
      },
      {
        initialRouteName: 'UserList',
        tabBarPosition: 'bottom',
        order: [
          'UserList',
          'UserProfile',
          'UserSearch',
          'MessageNavigator',
          'NoticeDetail',
          'KeijibanList',
          'KeijibanDetail',
          'KeijibanPost',
          'PointScreen',
          'MyPage',
          'SettingScreen',
          'BlockList',
          'PointTable',
          'Webview',
          'EditProfile',
          'Chatting',
          'KeijibanSearch',
          'QaScreen',
          'TransferDevice',
          'Favoritefootprint',
        ],
        tabBarComponent: props => {
          return <FooterMainApp {...props} />;
        },
      },
    ),
    navigationOptions: {
      // header component
      header: props => <HeaderMainApp {...props} />,
    },
  },
});
