import React from 'react';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import RosterList from '../../containers/screens/message_page/roster_list.screen';
import NoticeList from '../../containers/screens/message_page/notice_list.screen';
import CampaignList from '../../containers/screens/message_page/campaign_list.screen';
import HeaderMessage from '../../containers/components/headerMessageTab';

export default createMaterialTopTabNavigator(
  {
    Roster: {
      screen: RosterList,
      navigationOptions: {
        swipeEnabled: false,
      },
    },
    Notice: {
      screen: NoticeList,
      navigationOptions: {
        swipeEnabled: true,
      },
    },
    Campaign: {
      screen: CampaignList,
      navigationOptions: {
        swipeEnabled: true,
      },
    },
  },
  {
    initialRouteName: 'Roster',
    tabBarPosition: 'top',
    order: ['Roster', 'Notice', 'Campaign'],
    tabBarComponent: props => {
      return <HeaderMessage {...props} />;
    },
  },
);
