import {NavigationActions} from 'react-navigation';

let _navigator;
let _rootStore;

function setTopLevelNavigator(navigatorRef, rootStore) {
  _rootStore = rootStore;
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );

  if (
    routeName != 'Register' &&
    routeName != 'Login' &&
    routeName != 'Webview'
  ) {
    _rootStore.socketIO.sendScreenView(routeName);
  }
}

// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
};
