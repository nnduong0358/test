import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import LoginScreen from '../../containers/screens/login.screen';
import RegisterScreen from '../../containers/screens/register.creen';
import MainAppNavigation from './app_navigator';

export default createAppContainer(
  createSwitchNavigator(
    {
      // You could add another route here for authentication.
      // Read more at https://reactnavigation.org/docs/en/auth-flow.html
      Login: {screen: LoginScreen},
      Register: {screen: RegisterScreen},
      mainApp: {screen: MainAppNavigation},
    },
    {
      initialRouteName: 'Login',
    },
  ),
);
