import {Dimensions} from 'react-native';
import {Adjust, AdjustConfig, AdjustEvent} from 'react-native-adjust';
import AsyncStorage from '@react-native-community/async-storage';
import AppConfig from '../constants/app_config';

// custom Json stringify avoid circular structure
export const customStringify = function(v) {
  const cache = new Map();
  return JSON.stringify(v, function(key, value) {
    if (typeof value === 'object' && value !== null) {
      if (cache.get(value)) {
        // Circular reference found, discard key
        return;
      }
      // Store value in our map
      cache.set(value, true);
    }
    return value;
  });
};

// create form data for ajax/fetch POST requset
export const createFormDataFromParamObject = param_object => {
  let form_data = new FormData();
  for (var key in param_object) {
    form_data.append(key, param_object[key]);
  }
  return form_data;
};

// create form data for ajax/fetch GET requset
export const createGetDataFromParamObject = param_object => {
  let get_data = '?';

  for (let key in param_object) {
    if (param_object[key] || param_object[key] === 0) {
      let add_data = key + '=' + param_object[key];

      if (get_data != '?') {
        // first param
        get_data += '&' + add_data;
      } else {
        // after 2nd param
        get_data += add_data;
      }
    }
  }
  return get_data === '?' ? '' : get_data;
};

export const convertObjectToArray = obj => {
  return Object.keys(obj).map(function(key) {
    return obj[key];
  });
};

export const parseStringArrayNumber = string => {
  return string.substring(1, string.length - 1).split(',');
};

export const randomString = function(length) {
  var text = '';
  var possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};

export const numberWithCommas = x => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

const formatTime = time => {
  if (time.toString().length == 1) return '0' + time;
  else return time;
};

export const timeToString = timestamp => {
  let date = new Date(+timestamp);
  return (
    formatTime(1 + date.getMonth()) +
    '/' +
    formatTime(date.getDate()) +
    ' ' +
    formatTime(date.getHours()) +
    ':' +
    formatTime(date.getMinutes())
  );
};

export const convertTime = timestamp => {
  let date = new Date(timestamp);
  if (date == 'Invalid Date') return '00/00 00:00';
  return (
    formatTime(1 + date.getMonth()) +
    '/' +
    formatTime(date.getDate()) +
    ' ' +
    formatTime(date.getHours()) +
    ':' +
    formatTime(date.getMinutes())
  );
};

export function isIphoneX() {
  let d = Dimensions.get('window');
  const {height, width} = d;

  return (
    // This has to be iOS duh
    Platform.OS === 'ios' &&
    // Accounting for the height in either orientation
    (height === 812 || width === 812)
  );
}

export function callEventAdjust(name) {
  console.log('name adjust event', name);
  let adjustConfig = new AdjustConfig(
    AppConfig.ADJUST_TOKEN,
    AdjustConfig.EnvironmentProduction,
  );
  Adjust.create(adjustConfig);

  switch (name) {
    case 'INSTALL':
      AsyncStorage.getItem('checkInstallApp')
        .then(value => {
          if (value == null) {
            // Adjust tracking event Install
            let adjustEvent = new AdjustEvent(AppConfig.ADJUST_EVENT.INSTALL);
            Adjust.trackEvent(adjustEvent);

            AsyncStorage.setItem('checkInstallApp', 'Installed').catch(error =>
              console.log(error),
            );
          }
        })
        .catch(error => console.log('Async storage error: ', error));
      break;

    case 'REGISTER':
      let adjustEvent = new AdjustEvent(AppConfig.ADJUST_EVENT.REGISTER);
      Adjust.trackEvent(adjustEvent);
      break;

    case 'PAYMENT':
      AsyncStorage.getItem('itemPurchase')
        .then(value => {
          if (value !== null) {
            // Adjust tracking event Purchase
            let adjustEvent = new AdjustEvent(AppConfig.ADJUST_EVENT.PAYMENT);
            adjustEvent.setRevenue(parseInt(value), 'JPY');
            Adjust.trackEvent(adjustEvent);
          }
        })
        .catch(error => {});
      AsyncStorage.removeItem('itemPurchase').catch(error => {});
      break;
  }
  Adjust.componentWillUnmount();
}

export async function checkCacheImage(urlImage) {
  // console.log('urlImage', urlImage);
  if (urlImage) {
    const cache_variable = '?time=';
    try {
      if (urlImage.includes(cache_variable)) {
        let arr_split = urlImage.split(cache_variable);
        await AsyncStorage.setItem(arr_split[0], arr_split[1]);
        // console.log('arr_split', arr_split);
        return urlImage;
      } else {
        let cache_value = await AsyncStorage.getItem(urlImage);

        if (cache_value !== null) {
          // console.log('url with params cache', urlImage + cache_variable + cache_value);
          return urlImage + cache_variable + cache_value;
        }
      }
    } catch (err) {
      // console.log('checkCacheImage err', err);
      return urlImage;
    }
  }
  return urlImage;
}

export async function getArrOrderPaymentID() {
  try {
    let result = await AsyncStorage.getItem('order_payment_id');

    return result !== null ? result.split(',') : null;
  } catch (error) {
    console.log('getArrOrderPaymentID err:', error);
    return null;
  }
}

export async function setArrOrderPaymentID(orderID) {
  try {
    let result = await AsyncStorage.getItem('order_payment_id');
    if (result == null)
      AsyncStorage.setItem('order_payment_id', orderID).catch(error =>
        console.log(error),
      );
    else if (!result.includes(orderID)) {
      result += `,${orderID}`;
      AsyncStorage.setItem('order_payment_id', result).catch(error =>
        console.log(error),
      );
    }
  } catch (error) {
    console.log('setArrOrderPaymentID err:', error);
  }
}

export async function removeOrderPaymentID(orderID) {
  try {
    let result = await AsyncStorage.getItem('order_payment_id');
    if (result !== null) {
      let arr_order_id = result.split(',');
      arr_order_id = arr_order_id.filter(item => item !== orderID);

      if (arr_order_id.length > 0)
        AsyncStorage.setItem('order_payment_id', arr_order_id.toString()).catch(
          error => console.log(error),
        );
      else
        AsyncStorage.removeItem('order_payment_id').catch(error =>
          console.log(error),
        );
    }
  } catch (error) {
    console.log('removeOrderPaymentID err:', error);
  }
}
