import AppConfig from '../constants/app_config';
import {
  createGetDataFromParamObject,
  createFormDataFromParamObject,
} from '../helpers';

export const requestGetAPI = async (headers, path, params) => {
  console.log('headers:', headers);
  console.log('path:', path);
  console.log('params:', params);
  try {
    const getFromParamsObj = params ? createGetDataFromParamObject(params) : '';
    console.log('URL API:', AppConfig.API_URL + path + getFromParamsObj);
    const response = await fetch(AppConfig.API_URL + path + getFromParamsObj, {
      method: 'GET',
      headers: headers,
    }).then(res => res.json());

    console.log('response API method GET:', response);
    return response;
  } catch (err) {
    console.log('request API method GET err:', err);
    return null;
  }
};

export const requestPostAPI = async (headers, path, params) => {
  console.log('headers:', headers);
  console.log('path:', path);
  console.log('params:', params);
  try {
    const postFromParamsObj = createFormDataFromParamObject(params);
    const response = await fetch(AppConfig.API_URL + path, {
      method: 'POST',
      headers: headers,
      body: postFromParamsObj,
    }).then(res => res.json());

    console.log('response API method POST:', response);
    return response;
  } catch (err) {
    console.log('request API method POST err:', err);
    return null;
  }
};
