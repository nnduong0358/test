import {Keyboard, Alert, BackHandler, Platform, Linking} from 'react-native';
import AppConfig from '../constants/app_config';
import NavigatorService from '../navigator/navigation_services';
import AndroidOpenSettings from 'react-native-android-open-settings';

export function alertForceUpdate(title, body, new_version_url) {
  Alert.alert(
    title,
    body,
    [
      {
        text: 'アップデート',
        onPress: () => linkingForceUpdate(new_version_url),
        style: 'cancel',
      },
    ],
    {cancelable: false},
  );
}

function linkingForceUpdate(link) {
  if (link) {
    Linking.canOpenURL(link)
      .then(supported => {
        if (supported) {
          BackHandler.exitApp();
          return Linking.openURL(link);
        }
      })
      .catch(err => console.error('An error occurred', err));
  } else {
    BackHandler.exitApp();
  }
}

export function alertRequestPermissionPhotos() {
  Alert.alert(
    '画像の読み取りが無効です',
    '\n画像の読み取りが無効です。設定から' +
      AppConfig.APP_NAME +
      'を開き、「写真」を「読み出しと書き込み」に変更することで、端末内の画像を送信することができます。',
    [
      {
        text: '設定',
        onPress: () => AndroidOpenSettings.appDetailsSettings(),
      },
      {text: 'OK', onPress: () => {}, style: 'cancel'},
    ],
    {cancelable: true},
  );
}

export function alertRequestPermissionStorage() {
  Alert.alert(
    '',
    AppConfig.APP_NAME + 'にストレージの読み取りを許可しますか？',
    [
      {text: '設定', onPress: () => AndroidOpenSettings.appDetailsSettings()},
      {text: 'OK', onPress: () => BackHandler.exitApp(), style: 'cancel'},
    ],
    {cancelable: true},
  );
}

export function linkingHanle(message) {
  let url_data = message.split("'");
  console.log('data message:::', url_data);
  let url = url_data[1];
  console.log(url);

  if (url) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          Alert.alert(
            '',
            'あなたは電子メールクライアントを持っていませんので、最初にインストールしてください',
            [{text: 'OK', onPress: () => BackHandler.exitApp()}],
            {cancelable: false},
          );
        } else {
          BackHandler.exitApp();
          return Linking.openURL(url);
        }
      })
      .catch(err => console.log('An error occurred', err));
  } else {
    BackHandler.exitApp();
  }
}

export function alertUserNotEnoughPoint() {
  Keyboard.dismiss();
  Alert.alert(
    AppConfig.APP_NAME,
    '\nポイントが不足してます。',
    [
      {
        text: '購入ページ',
        onPress: () => NavigatorService.navigate('PointScreen'),
      },
      {
        text: 'キャンセル',
        onPress: () => console.log('Cancel'),
        style: 'cancel',
      },
    ],
    {cancelable: false},
  );
}

export function favoriteStatus() {
  Alert.alert(
    AppConfig.APP_NAME,
    '\n' + 'お気に入りに追加しました',
    [{text: 'OK', onPress: () => {}}],
    {cancelable: false},
  );
}
