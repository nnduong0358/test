import {Alert, ToastAndroid} from 'react-native';
import {observable, computed, action, decorate, toJS} from 'mobx';
import {requestGetAPI, requestPostAPI} from '../../services/request_api';
import * as Helper from '../../services/helpers';
import AppConfig from '../../services/constants/app_config';
import NavigatorService from '../../services/navigator/navigation_services';
import * as ShowAlert from '../../services/show_alert';

export default class KeijibanAction {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @action async getKeijibanList(action) {
    if (
      !this.rootStore.systemStore.loading &&
      !this.rootStore.keijibanStore.keijiban_list_condition.no_more_data
    ) {
      this.rootStore.systemStore.loading = true;
      this.rootStore.keijibanStore.keijiban_list_condition.page += 1;
      let params = {
        page: this.rootStore.keijibanStore.keijiban_list_condition.page,
        limit: this.rootStore.keijibanStore.keijiban_list_condition.limit,
        order: 'DESC',
        token: this.rootStore.systemStore.login_token,
      };
      if (action == 'search') {
        params = Object.assign(
          params,
          this.rootStore.keijibanStore.keijiban_list_condition.fills,
        );
      }
      await this.rootStore.pointAction.getPointFee();
      let headers = this.rootStore.systemStore.getApiHeader;
      let response = await requestGetAPI(headers, 'keijiban/post', params);
      if (response?.code == 200) {
        if (response.data.result.length > 0) {
          if (this.rootStore.keijibanStore.keijiban_list_condition.page === 1) {
            this.rootStore.keijibanStore.keijiban_list_data =
              response.data.result;
          } else {
            this.rootStore.keijibanStore.keijiban_list_data = this.rootStore.keijibanStore.keijiban_list_data.concat(
              response.data.result,
            );
          }
        }
        this.rootStore.keijibanStore.keijiban_list_condition.no_more_data =
          response.data.result.length <
          this.rootStore.keijibanStore.keijiban_list_condition.limit;
      }
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async getKeijibanDetail(value) {
    this.rootStore.systemStore.loading = true;
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      token: this.rootStore.systemStore.login_token,
      keijiban_id: value.id,
    };
    await this.rootStore.pointAction.getPointFee();
    let data = await requestGetAPI(
      headers,
      'keijiban/show_keijiban_detail',
      params,
    );

    if (data.code == 200) {
      data.data.thumb = value.thumb ? value.thumb.path : undefined;
      data.data.origin = value.origin ? value.origin.path : undefined;
      this.rootStore.keijibanStore.keijiban_detail = data.data;
      await this.rootStore.userAction.getUserProfile(
        data.data.user_id,
        '',
        1,
        false,
      );
      NavigatorService.navigate('KeijibanDetail');
    } else {
      Alert.alert(
        AppConfig.APP_NAME,
        '\n' + response.message,
        [{text: 'OK', onPress: () => console.log('Cancel')}],
        {cancelable: false},
      );
    }
    this.rootStore.systemStore.loading = false;
  }

  @action async viewKeijibanImage(url) {
    let params = {
      token: this.rootStore.systemStore.login_token,
      media_id: url,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    this.rootStore.systemStore.image_popup.show = true;
    this.rootStore.systemStore.image_popup.loading = true;
    let response = await requestGetAPI(
      headers,
      'keijiban/view_keijiban_image',
      params,
    );

    if (response.code == 200) {
      this.rootStore.systemStore.image_popup = {
        show: true,
        uri: url,
        loading: false,
        screen: 'KeijibanView',
      };
      this.rootStore.pointStore.total_point =
        this.rootStore.pointStore.getTotalPoint -
        this.rootStore.pointStore.getPointSettings.view_media_discuss_image;
    } else if (response.code == 402) {
      ShowAlert.alertUserNotEnoughPoint();
    } else {
      ToastAndroid.show(response.message, ToastAndroid.LONG);
    }
    this.rootStore.systemStore.image_popup.loading = false;
  }

  @action async postKeijiban(content, file) {
    this.rootStore.systemStore.loading = true;
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      token: this.rootStore.systemStore.login_token,
      content: content,
    };
    if (file) {
      headers['Content-Type'] = 'multipart/form-data';
      params.image = {
        uri: file.uri,
        size: file.size,
        type: file.type,
        name: file.name,
        tmp_name: file.name,
      };
    }

    let response = await requestPostAPI(
      headers,
      'keijiban/post/create',
      params,
    );
    this.rootStore.systemStore.loading = false;
    if (response?.code == 200) {
      Alert.alert(
        '',
        response.message,
        [
          {
            text: '閉じる',
            onPress: () => NavigatorService.navigate('KeijibanList'),
          },
        ],
        {cancelable: false},
      );
    } else if (response?.code == 402) {
      Alert.alert(
        '',
        response.message,
        [
          {text: 'キャンセル', onPress: () => console.log('Cancel')},
          //{ text: '購入ページ', onPress: () => NavigatorService.navigate('Point') },
        ],
        {cancelable: false},
      );
    } else {
      ToastAndroid.show(response.message, ToastAndroid.LONG);
    }
  }
}
