import {observable, computed, action, decorate, toJS} from 'mobx';

export default class KeijibanStore {
  @observable keijiban_list_data = [];
  @computed get getKeijibanListData() {
    return toJS(this.keijiban_list_data);
  }

  @observable keijiban_list_condition = {
    page: 0,
    limit: 32,
    no_more_data: false,
    result_find: false,
    fills: {
      sex: '',
      age: '',
      height: '',
      style: '',
      job: '',
      income: '',
      sort_by: '',
      sex_interest: '',
      relationship_status: '',
      real_time: '',
    },
  };
  @observable keijiban_detail = {};
}
