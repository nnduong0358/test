import {Alert, ToastAndroid} from 'react-native';
import {observable, computed, action, decorate, toJS} from 'mobx';
import {requestGetAPI, requestPostAPI} from '../../services/request_api';
import * as Helper from '../../services/helpers';
import AppConfig from '../../services/constants/app_config';
import NavigatorService from '../../services/navigator/navigation_services';
import * as ShowAlert from '../../services/show_alert';

export default class UserAction {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @action async getUserList(action) {
    if (!this.rootStore.userStore.user_list_condition.no_more_data) {
      this.rootStore.systemStore.loading = true;
      this.rootStore.userStore.user_list_condition.page += 1;
      let params = {
        page: this.rootStore.userStore.user_list_condition.page,
        action: action,
        limit: this.rootStore.userStore.user_list_condition.limit,
        token: this.rootStore.systemStore.login_token,
      };
      if (action == 'search') {
        params = Object.assign(
          params,
          this.rootStore.userStore.user_list_condition.fills,
        );
      }
      await this.rootStore.pointAction.getPointFee();

      try {
        const response = await requestGetAPI(
          this.rootStore.systemStore.getApiHeader,
          'user/find',
          params,
        );

        this.rootStore.systemStore.loading = false;
        if (response?.code == 200) {
          if (this.rootStore.userStore.user_list_condition.page === 1) {
            this.rootStore.userStore.user_list_data = response.data.result;
          } else {
            this.rootStore.userStore.user_list_data = this.rootStore.userStore.user_list_data.concat(
              response.data.result,
            );
          }
          this.rootStore.userStore.user_list_condition.no_more_data =
            response.data.result.length <
            this.rootStore.userStore.user_list_condition.limit;
        }
      } catch (err) {
        console.log('getUserList err', err);
        this.rootStore.systemStore.loading = false;
      }
    }
  }

  @action async blockUser(id, user_code, type = 1, index) {
    this.rootStore.systemStore.loading = true;
    let params = {
      token: this.rootStore.systemStore.login_token,
      lock_user_code: user_code,
      id: id,
      type: type,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    try {
      const response = await requestPostAPI(headers, 'user/lock', params);
      this.rootStore.systemStore.loading = false;
      Alert.alert(
        AppConfig.APP_NAME,
        '\n' + response.message || '',
        [{text: 'OK', onPress: () => console.log('Cancel')}],
        {cancelable: false},
      );
      if (response.code == 200) {
        console.log(
          'last screen blockuser',
          this.rootStore.systemStore.nav_list.last_screen,
        );
        if (
          this.rootStore.systemStore.nav_list.last_screen[
            this.rootStore.systemStore.nav_list.last_screen.length - 1
          ] !== 'SettingScreen' &&
          this.rootStore.systemStore.nav_list.current_screen !== 'UserList'
        ) {
          if (
            ['UserProfile', 'Chatting'].includes(
              this.rootStore.systemStore.nav_list.last_screen[
                this.rootStore.systemStore.nav_list.last_screen.length - 1
              ],
            )
          ) {
            this.rootStore.systemAction.resetLastScreen('UserList');
            NavigatorService.navigate('UserList');
          } else this.rootStore.systemAction.goBack();
        }

        if (type == 0) {
          // unblock user
          if (index || index == 0)
            this.rootStore.myPageStore.block_list.splice(index, 1);

          let findIndex = this.rootStore.userStore.array_id_block_list.findIndex(
            item => item == id,
          );
          if (findIndex != -1)
            this.rootStore.userStore.array_id_block_list.splice(findIndex, 1);
        } else {
          // block user
          this.rootStore.userStore.array_id_block_list.push(id);

          let findIndex = this.rootStore.userStore.user_list_data.findIndex(
            item => item.id == id,
          );
          if (findIndex != -1)
            this.rootStore.userStore.user_list_data.splice(findIndex, 1);
        }
      }
    } catch (err) {
      console.log('blockUser err', err);
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async reportUser(id, user_code, screen, input, keijiban_id) {
    this.rootStore.systemStore.loading = true;
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      token: this.rootStore.systemStore.login_token,
      comment: input,
    };
    let response;
    if (screen == 'keijiban') {
      params.id = keijiban_id;
      response = await requestPostAPI(headers, 'keijiban/report', params);
    } else {
      params.user_code = user_code;
      params.id = id;
      params.screen = screen;
      params.comment = input;

      response = await requestPostAPI(headers, 'user/report', params);
    }
    if (response?.code == 200) {
      Alert.alert(
        AppConfig.APP_NAME,
        '\n' + response.message,
        [{text: 'はい', onPress: () => {}}],
        {cancelable: false},
      );
    }
    this.rootStore.systemStore.loading = false;
  }

  @action async getUserProfile(
    id,
    screen = 'profile',
    exclude_point_action = 0,
    isNavigate = true,
  ) {
    this.rootStore.systemStore.loading = true;
    const headers = this.rootStore.systemStore.getApiHeader;
    const params = {
      footprint: 'true',
      screen: screen,
      id: id,
      token: this.rootStore.systemStore.login_token,
      exclude_point_action: exclude_point_action,
    };
    let response = await requestGetAPI(headers, 'user/show', params);
    if (response?.code == 200) {
      this.rootStore.userStore.user_profile = response.data;
      if (response.data.image) {
        this.rootStore.userStore.user_profile.image = JSON.parse(
          response.data.image,
        );
      }
      if (isNavigate) NavigatorService.navigate('UserProfile');
    } else if (response?.code == 411) {
      Alert.alert(
        '',
        response.message,
        [
          {
            text: '設定',
            onPress: () => {
              NavigatorService.navigate('VariousSetting');
            },
          },
          {text: 'OK', onPress: () => console.log('Cancel'), style: 'cancel'},
        ],
        {cancelable: false},
      );
    } else {
      this.rootStore.userStore.user_profile = {};
      ToastAndroid.show(response.message, ToastAndroid.LONG);
    }

    this.rootStore.systemStore.loading = false;
  }

  @action async viewImageProfile(index, image) {
    await this.rootStore.pointAction.getPointFee();
    let isEnoughPoint = await this.rootStore.pointAction.checkUserEnoughPoint(
      'view_image_profile_large_' + (index + 1),
    );

    if (isEnoughPoint || image.default == 1) {
      let headers = this.rootStore.systemStore.getApiHeader;
      let params = {
        img_index: index,
        my_user_id: this.rootStore.systemStore.user_login_data.id,
        user_id_footprint: this.rootStore.userStore.user_profile.id,
      };
      this.rootStore.systemStore.image_popup.show = true;
      this.rootStore.systemStore.image_popup.loading = true;
      let response = await requestGetAPI(headers, 'user/profile_image', params);
      if (response?.code == 200) {
        if (response.data == true) {
          switch (index) {
            case 1:
              this.rootStore.pointStore.total_point =
                this.rootStore.pointStore.getTotalPoint -
                this.rootStore.pointStore.getPointSettings
                  .view_image_profile_large_1;
              break;
            case 2:
              this.rootStore.pointStore.total_point =
                this.rootStore.pointStore.getTotalPoint -
                this.rootStore.pointStore.getPointSettings
                  .view_image_profile_large_2;
              break;
            case 3:
              this.rootStore.pointStore.total_point =
                this.rootStore.pointStore.getTotalPoint -
                this.rootStore.pointStore.getPointSettings
                  .view_image_profile_large_3;
              break;
          }
        }
      }
      this.rootStore.systemStore.image_popup = {
        show: true,
        uri: image,
        screen: 'profile',
        loading: false,
      };
    } else {
      ShowAlert.alertUserNotEnoughPoint();
    }
  }

  @action async postFavorite(id, user_code) {
    this.rootStore.systemStore.loading = true;

    let params = {
      token: this.rootStore.systemStore.login_token,
      favorites_user_code: user_code,
      favorites_id: id,
      type: 1,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestPostAPI(headers, 'favorites/index', params);
    this.rootStore.systemStore.loading = false;
    this.getUserProfile(id, '', 1, false);

    Alert.alert(
      AppConfig.APP_NAME,
      '\n' + response.data,
      [{text: 'OK', onPress: () => console.log('Cancel')}],
      {cancelable: false},
    );
  }
}
