import {observable, computed, action, decorate, toJS} from 'mobx';
import AppConfig from '../../services/constants/app_config';
import DeviceInfo from 'react-native-device-info';

export default class UserStore {
  @observable popup_action_sheet = false;
  @observable list_report_popup = {
    user_code: '',
    id: '',
    no_block: '',
    screen: '',
    keijiban_id: '',
  };

  @observable report_user = {
    id: '',
    user_code: '',
    screen: '',
    show: false,
  };
  @observable user_list_condition = {
    page: 0,
    limit: 32,
    no_more_data: false,
    result_find: false,
    fills: {
      sex: '',
      age: '',
      height: '',
      style: '',
      job: '',
      income: '',
      sort_by: '',
      sex_interest: '',
      relationship_status: '',
      real_time: '',
    },
  };
  @observable user_list_data = [];
  @observable array_id_block_list = [];
  @observable user_profile = {};

  @computed get getUserListData() {
    return toJS(this.user_list_data);
  }
}
