import {Alert, ToastAndroid} from 'react-native';
import {observable, computed, action, decorate, toJS} from 'mobx';
import RNFetchBlob from 'rn-fetch-blob';
import {requestGetAPI, requestPostAPI} from '../../services/request_api';
import * as Helper from '../../services/helpers';
import AppConfig from '../../services/constants/app_config';

export default class MyPageAction {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @action async getMyPage() {
    this.rootStore.systemStore.loading = true;
    let params = {token: this.rootStore.systemStore.login_token};
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestGetAPI(headers, 'user', params);

    if (response?.code == 200) {
      this.rootStore.pointStore.total_point = response.data.point;
      if (response.data.image) {
        response.data.image = JSON.parse(response.data.image);
      }
      this.rootStore.myPageStore.my_profile = response.data;
      this.rootStore.myPageStore.user_status_tmp =
        response.data.user_status_tmp || response.data.user_status;
    }
    this.rootStore.systemStore.loading = false;
  }

  @action async getBlockList() {
    this.rootStore.systemStore.loading = true;
    let params = {token: this.rootStore.systemStore.login_token};
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestGetAPI(headers, 'user/lock/list', params);
    if (response?.code == 200) {
      this.rootStore.myPageStore.block_list = response.data.result || [];
    }
    this.rootStore.systemStore.loading = false;
  }

  @action async getFavoriteFootprint(isFavorite) {
    if (
      !this.rootStore.systemStore.loading &&
      !this.rootStore.myPageStore.list_condition.no_more_data
    ) {
      this.rootStore.systemStore.loading = true;
      this.rootStore.myPageStore.list_condition.page += 1;

      let params = {
        token: this.rootStore.systemStore.login_token,
        page: this.rootStore.myPageStore.list_condition.page,
        limit: this.rootStore.myPageStore.list_condition.limit,
      };
      let headers = this.rootStore.systemStore.getApiHeader;
      let path = isFavorite ? 'favorites/index/lists' : 'footprint/index/list';
      let response = await requestGetAPI(headers, path, params);

      if (response?.code == 200) {
        if (this.rootStore.myPageStore.list_condition.page === 1) {
          this.rootStore.myPageStore.list_condition.data =
            response.data.result || [];
        } else {
          this.rootStore.myPageStore.list_condition.data = this.rootStore.myPageStore.list_condition.data.concat(
            response.data.result,
          );
        }
        this.rootStore.myPageStore.list_condition.no_more_data = true;
        /*(response.data.result.length < this.rootStore.myPageStore.list_condition.limit);*/
      }
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async updateProfile() {
    this.rootStore.systemStore.loading = true;
    let params = this.rootStore.myPageStore.getEditProfile;
    params.token = this.rootStore.systemStore.login_token;
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestPostAPI(headers, 'user/update', params);

    if (response?.code == 200) {
      let alert = this.rootStore.myPageStore.edit_image_status
        ? '\nアバター、自己PRは運営者が審査後に公開されます。'
        : '';
      ToastAndroid.show(
        'プロフィールを変更しました。' + alert,
        ToastAndroid.LONG,
      );
      this.rootStore.systemAction.goBack();
      this.rootStore.myPageStore.edit_image_status = false;
    } else {
      ToastAndroid.show(response.message, ToastAndroid.LONG);
    }
    this.rootStore.systemStore.loading = false;
  }

  @action updateAvatar() {
    this.rootStore.systemStore.loading = true;
    let headers = this.rootStore.systemStore.getApiHeader;
    headers['Content-Type'] = 'multipart/form-data';

    let body = [];
    body.push({name: 'token', data: this.rootStore.systemStore.login_token});

    toJS(this.rootStore.myPageStore.files).forEach((file, index) => {
      if (file) {
        this.callApiUpdateAvatar(file, body, index, headers);
      }
    });
  }

  @action callApiUpdateAvatar(file, body, index, headers) {
    body.push({
      name: 'images[' + index + ']',
      filename: file.name,
      data: file.data,
      type: file.type,
      size: file.size,
      uri: file.uri,
    });
    console.log('up avatar body', body);

    if (
      index === 2 ||
      (index === 1 &&
        this.rootStore.myPageStore.files[index + 1] == undefined) ||
      (index === 0 &&
        this.rootStore.myPageStore.files[index + 1] == undefined &&
        this.rootStore.myPageStore.files[index + 2] == undefined)
    ) {
      RNFetchBlob.fetch(
        'POST',
        AppConfig.API_URL + 'user/images/update',
        headers,
        body,
      )
        .then(res => {
          console.log('upload avatar response:::', res);
          this.rootStore.systemStore.loading = false;
          this.getMyPage();
          this.rootStore.myPageStore.files = new Array(3);
        })
        .catch(err => {
          this.rootStore.systemStore.loading = false;
          console.log('Fail: ', err);
        });
    }
  }
}
