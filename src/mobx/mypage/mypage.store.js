import {observable, computed, action, decorate, toJS} from 'mobx';

export default class MyPageStore {
  @observable my_profile = {};
  @observable user_status_tmp = '';
  @observable block_list = [];
  @observable edit_image_status = false;
  @observable edit_popup = false;
  @observable edit_status_popup = false;
  @observable updating_profile = false;
  @observable updating_status = false;
  @observable edit_profile = {
    user_status: '',
    height: '',
    style: '',
    job: '',
    income: '',
    relationship_status: '',
    sex_interest: '',
  };
  @observable popup_images = null;
  @observable files = new Array(3);
  @observable change_edit_profile = {
    result: false,
    income: false,
    job: false,
    style: false,
    height: false,
    status: false,
    image: false,
    relationship_status: false,
    sex_interest: false,
  };

  @observable list_condition = {
    page: 0,
    limit: 32,
    no_more_data: false,
    data: [],
  };

  @computed get getMyProfile() {
    return toJS(this.my_profile);
  }

  @computed get getBlockListData() {
    return toJS(this.block_list);
  }

  @computed get getListFavoriteFootprint() {
    return toJS(this.list_condition.data);
  }

  @computed get getChangeEditProfile() {
    return toJS(this.change_edit_profile);
  }
  @computed get getEditProfile() {
    return toJS(this.edit_profile);
  }
}
