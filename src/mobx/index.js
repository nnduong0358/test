import SystemStore from './system/system.store';
import SystemAction from './system/system.actions';
import PointStore from './point/point.store';
import PointAction from './point/point.actions';
import SocketIO from './socket/socketIO';
import UserStore from './user/user.store';
import UserAction from './user/user.actions';
import MessageStore from './message/message.store';
import MessageAction from './message/message.actions';
import KeijibanStore from './keijiban/keijiban.store';
import KeijibanAction from './keijiban/keijiban.actions';
import MyPageStore from './mypage/mypage.store';
import MyPageAction from './mypage/mypage.actions';

class RootStore {
  constructor() {
    this.systemStore = new SystemStore(this);
    this.systemAction = new SystemAction(this);

    this.userStore = new UserStore(this);
    this.userAction = new UserAction(this);

    this.keijibanStore = new KeijibanStore(this);
    this.keijibanAction = new KeijibanAction(this);

    this.messageStore = new MessageStore(this);
    this.messageAction = new MessageAction(this);

    this.pointStore = new PointStore(this);
    this.pointAction = new PointAction(this);

    this.myPageStore = new MyPageStore(this);
    this.myPageAction = new MyPageAction(this);

    this.socketIO = new SocketIO(this);
  }
}

const rootStore = new RootStore();
export default rootStore;
