import {Alert, ToastAndroid} from 'react-native';
import {observable, computed, action, decorate, toJS} from 'mobx';
import RNFetchBlob from 'rn-fetch-blob';
import {requestGetAPI, requestPostAPI} from '../../services/request_api';
import * as Helper from '../../services/helpers';
import AppConfig from '../../services/constants/app_config';
import ShortcutBadge from 'react-native-shortcut-badge';
import NavigatorService from '../../services/navigator/navigation_services';
import ImageResizer from 'react-native-image-resizer';
import {createGetDataFromParamObject} from '../../services/helpers';

export default class MessageAction {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  // load more roster list
  @action async getRosterList() {
    if (
      !this.rootStore.systemStore.loading &&
      !this.rootStore.messageStore.roster_list.no_more_data
    ) {
      this.rootStore.systemStore.loading = true;
      // plus page and call api load new page data
      this.rootStore.messageStore.roster_list.page += 1;

      let params = {
        page: this.rootStore.messageStore.roster_list.page,
        limit: this.rootStore.messageStore.roster_list.limit,
        token: this.rootStore.systemStore.login_token,
      };
      let headers = this.rootStore.systemStore.getApiHeader;
      let response = await requestGetAPI(headers, 'roster/get', params);

      if (response?.code == 200) {
        let result = response.data;
        if (result.total_unread_all != undefined) {
          this.rootStore.messageStore.number_unread = {
            total_unread_campaign: result.total_unread_campaign,
            total_unread_message: result.total_unread_message,
            total_unread_notice: result.total_unread_notice,
            total_unread_all: result.total_unread_all,
          };
          ShortcutBadge.setCount(parseInt(result.total_unread_all));
        }
        //merge new data with old data = current roster_list
        if (result.result != undefined) {
          if (this.rootStore.messageStore.roster_list.page == 0) {
            this.rootStore.messageStore.roster_list.data = result.result;
          } else {
            this.rootStore.messageStore.roster_list.data = this.rootStore.messageStore.roster_list.data.concat(
              result.result,
            );
          }
          this.rootStore.messageStore.roster_list.no_more_data =
            result.result.length <
            this.rootStore.messageStore.roster_list.limit;
        }
      }
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async getNoticeList() {
    if (
      !this.rootStore.systemStore.loading &&
      !this.rootStore.messageStore.notice_list.no_more_data
    ) {
      this.rootStore.systemStore.loading = true;
      this.rootStore.messageStore.notice_list.page += 1;

      let params = {
        page: this.rootStore.messageStore.notice_list.page,
        limit: this.rootStore.messageStore.roster_list.limit,
        order: 'DESC',
        sort: 'created_at',
        token: this.rootStore.systemStore.login_token,
      };
      let headers = this.rootStore.systemStore.getApiHeader;
      let response = await requestGetAPI(
        headers,
        'discuss/post/notice_list',
        params,
      );
      if (response?.code == 200) {
        let result = response.data;
        if (result.total_unread_all !== undefined) {
          this.rootStore.messageStore.number_unread = {
            total_unread_campaign: result.total_unread_campaign,
            total_unread_message: result.total_unread_message,
            total_unread_notice: result.total_unread_notice,
            total_unread_all: result.total_unread_all,
          };
          ShortcutBadge.setCount(parseInt(result.total_unread_all));
        }
        //merge new data with old data = current notice_list
        if (result.result != undefined) {
          this.rootStore.messageStore.notice_list.data =
            this.rootStore.messageStore.notice_list.page === 1
              ? result.result
              : this.rootStore.messageStore.notice_list.data.concat(
                  result.result,
                );
          this.rootStore.messageStore.notice_list.no_more_data =
            result.result.length <
            this.rootStore.messageStore.notice_list.limit;
        }
      }
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async getCampaignList() {
    if (
      !this.rootStore.systemStore.loading &&
      !this.rootStore.messageStore.campaign_list.no_more_data
    ) {
      this.rootStore.systemStore.loading = true;
      this.rootStore.messageStore.campaign_list.page += 1;
      let params = {
        page: this.rootStore.messageStore.campaign_list.page,
        limit: this.rootStore.messageStore.campaign_list.limit,
        token: this.rootStore.systemStore.login_token,
      };
      let headers = this.rootStore.systemStore.getApiHeader;
      let response = await requestGetAPI(headers, 'campaign/lists', params);

      if (response?.code == 200) {
        let result = response.data;
        if (result.total_unread_all !== undefined) {
          this.rootStore.messageStore.number_unread = {
            total_unread_campaign: result.total_unread_campaign,
            total_unread_message: result.total_unread_message,
            total_unread_notice: result.total_unread_notice,
            total_unread_all: result.total_unread_all,
          };
          ShortcutBadge.setCount(parseInt(result.total_unread_all));
        }
        //merge new data with old data = current rootStore.messageStore.campaign_list
        if (result.result != undefined) {
          this.rootStore.messageStore.campaign_list.data =
            this.rootStore.messageStore.campaign_list.page == 0
              ? result.result
              : this.rootStore.messageStore.campaign_list.data.concat(
                  result.result,
                );
          this.rootStore.messageStore.campaign_list.no_more_data =
            result.result.length <
            this.rootStore.messageStore.campaign_list.limit;
        }
      }
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async getNoticeDetail(notice) {
    this.rootStore.systemStore.loading = true;
    this.rootStore.messageStore.notice_details.images = notice.images;
    this.rootStore.messageStore.notice_details.display_name =
      notice.display_name;

    let params = {
      token: this.rootStore.systemStore.login_token,
      notice_id: notice.id,
      user_id: this.rootStore.systemStore.getUserLoginData.id,
      notice_done_id: notice.notice_done_id,
      schedule_send_id: notice.schedule_send_id,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestGetAPI(
      headers,
      'discuss/notice_user_detail',
      params,
    );

    this.rootStore.messageStore.notice_details.content =
      response.data.notice.content;
    this.rootStore.messageStore.notice_details.title =
      response.data.notice.title;
    this.rootStore.messageStore.notice_details.created_at =
      response.data.notice.created_at;
    NavigatorService.navigate('NoticeDetail', {
      detail: toJS(this.rootStore.messageStore.notice_details),
    });

    this.rootStore.systemStore.loading = false;
  }

  @action async getCampaignDetail(id) {
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      token: this.rootStore.systemStore.login_token,
      campaign_id: id,
    };
    let response = await requestGetAPI(headers, 'campaign/get_detail', params);
    if (response?.code == 200 && response?.data) {
      NavigatorService.navigate('Webview', {
        uri: response.data,
        title: 'メッセージ',
      });
    }
  }

  @action async pinRoster(user_id, is_unpin) {
    this.rootStore.systemStore.loading = true;
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      token: this.rootStore.systemStore.login_token,
      user_id: user_id,
    };
    if (is_unpin !== 0) {
      params.is_unpin = true;
    }
    let response = await requestPostAPI(headers, 'roster/pin_chat', params);
    if (response?.code == 200) {
      this.rootStore.systemStore.loading = false;
      this.rootStore.messageStore.roster_list.page = -1;
      this.rootStore.messageStore.roster_list.no_more_data = false;
      this.getRosterList();
    }
    this.rootStore.systemStore.loading = false;
  }

  @action async deleteRoster(user_id) {
    this.rootStore.systemStore.loading = true;
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      token: this.rootStore.systemStore.login_token,
      user_id: user_id,
    };
    let response = await requestPostAPI(headers, 'roster/delete_chat', params);
    if (response?.code == 200) {
      this.rootStore.systemStore.loading = false;
      this.rootStore.messageStore.roster_list.page = -1;
      this.rootStore.messageStore.roster_list.no_more_data = false;
      this.getRosterList();
    } else {
      this.rootStore.systemStore.loading = false;
      ToastAndroid.show(response.message, ToastAndroid.LONG);
    }
  }

  @action openChatting(
    target_user_id,
    target_user_code,
    target_user_display_name,
    target_user_avatar_url,
  ) {
    this.rootStore.messageStore.message_chatting_list.no_more_history = false;
    this.rootStore.messageStore.message_chatting_list.user = {
      id: target_user_id,
      user_code: target_user_code,
      display_name: target_user_display_name,
      avatar_url: target_user_avatar_url,
    };
    // reset message chatting data
    this.rootStore.messageStore.message_chatting_list.data = [];
    this.rootStore.messageStore.message_chatting_list.last_msg = null;
    // send message neu co message param truyen vao (dung cho viec gui message tu man hinh khac message chatting)
    this.loadMessageHistory(
      target_user_code,
      target_user_id,
      target_user_display_name,
      target_user_avatar_url,
      '',
      true,
    );
  }

  @action async loadMessageHistory(
    target_user_code,
    target_user_id,
    target_user_display_name,
    target_user_avatar_url,
    msg_id = '',
    is_open_chatting,
  ) {
    if (
      !this.rootStore.messageStore.message_chatting_list.no_more_history &&
      !this.rootStore.systemStore.loading
    ) {
      this.rootStore.systemStore.loading = true;
      // msg_id : last id of message for load more history
      // message history
      let params = {
        token: this.rootStore.systemStore.login_token,
        user_code: target_user_code,
        id: target_user_id,
        seq: msg_id,
        limit: 20,
      };
      let headers = this.rootStore.systemStore.getApiHeader;
      await this.rootStore.pointAction.getPointFee();
      let response = await requestGetAPI(
        headers,
        'user/message_history',
        params,
      );
      if (response?.code == 200) {
        let result = response.data.result;

        result.forEach((item, index) => {
          if (
            item.u_id == target_user_id &&
            !this.rootStore.messageStore.message_chatting_list.last_msg
          ) {
            console.log('message_chatting_list.last_msg', item);
            this.rootStore.messageStore.message_chatting_list.last_msg = item;
            return;
          }
        });

        // // set da doc message cuoi cung
        if (
          this.rootStore.messageStore.message_chatting_list.last_msg &&
          this.rootStore.systemStore.app_state_active
        ) {
          this.rootStore.socketIO.setReadMessage(
            toJS(this.rootStore.messageStore.message_chatting_list.last_msg),
          );
        }

        // Neu msg_id == '' la dang refresh, gan lai data, con khong thi them data vao
        this.rootStore.messageStore.message_chatting_list.data =
          msg_id == ''
            ? result
            : this.rootStore.messageStore.getChattingList.concat(result);
        if (is_open_chatting) {
          await this.rootStore.userAction.getUserProfile(
            target_user_id,
            '',
            1,
            false,
          );
          let is_show_btn_profile =
            this.rootStore.systemStore.server_state.supporter.id !=
            this.rootStore.messageStore.getChattingUser.id;
          NavigatorService.navigate('Chatting', {
            username: target_user_display_name,
            avatar_url: target_user_avatar_url,
            show_btn_profile: is_show_btn_profile,
          });
        }
        // Neu data tra ve < 20 nghia la da het du lieu tren server, khong can load them nua
        this.rootStore.messageStore.message_chatting_list.no_more_history =
          result.length < 20;
      }
      this.rootStore.systemStore.loading = false;
    }
  }

  @action loadMoreMessageHistory(
    target_user_id,
    target_user_code,
    target_user_display_name,
    target_user_avatar_url,
    msg_id,
  ) {
    this.rootStore.messageStore.message_chatting_list.user = {
      id: target_user_id,
      user_code: target_user_code,
      display_name: target_user_display_name,
      avatar_url: target_user_avatar_url,
    };
    this.loadMessageHistory(
      target_user_code,
      target_user_id,
      target_user_display_name,
      target_user_avatar_url,
      msg_id,
    );
  }

  @action updateNewMessage(message) {
    this.rootStore.messageStore.new_message_comming =
      message.u_id !== this.rootStore.systemStore.getUserLoginData.id;
    if (message.type === 'image' || message.type === 'location') {
      message.showed = 0;
    } else {
      const ng_words = this.rootStore.systemStore.server_state.ng_words;
      ng_words.result.forEach(ng_word => {
        let regEx = new RegExp(ng_word, 'ig');
        message.msg = message.msg.replace(
          regEx,
          this.rootStore.systemStore.server_state.ng_words_replace,
        );
      });
    }

    this.rootStore.messageStore.message_chatting_list.data.unshift(message);
  }

  @action sendMessage(message_string) {
    this.rootStore.messageStore.can_send_message = false;
    this.rootStore.socketIO.sendMessage(
      message_string,
      this.rootStore.messageStore.getChattingUser.id,
    );
  }

  @action async sendMessageImage(file) {
    let file_split = file.fileName.split('.');
    let file_extension = file_split[file_split.length - 1].toLowerCase();
    const file_type_valid = ['png', 'jpeg', 'jpg', 'bmp'];

    if (file_type_valid.includes(file_extension)) {
      this.rootStore.messageStore.upload_image_process = {
        show: true,
        process: 0,
      };
      // let newImage = await ImageResizer.createResizedImage(file.uri, 940, 788, 'JPEG', 100);
      // let fileData = await RNFetchBlob.fs.readFile(newImage.path, 'base64');
      this.sendImage(file, file_extension, file.data);
    } else {
      ToastAndroid.show(
        ' このファイルタイプは送信できません。 ',
        ToastAndroid.LONG,
      );
    }
  }

  @action sendImage(file, file_extension, data) {
    this.rootStore.messageStore.can_send_message = false;

    let file_type = 'image/' + file_extension;
    let headers = this.rootStore.systemStore.getApiHeader;
    headers['Content-Type'] = 'multipart/form-data';

    RNFetchBlob.fetch('POST', AppConfig.API_URL + 'media/upload', headers, [
      {name: 'token', data: this.rootStore.systemStore.login_token},
      {name: 'type', data: '1'},
      {name: 'sector', data: 'chat'},
      {
        name: 'file',
        filename: file.fileName,
        data: data,
        type: file_type,
        size: file.fileSize,
        uri: file.uri,
      },
    ])
      .uploadProgress((written, total) => {
        this.rootStore.messageStore.upload_image_process.process =
          written / total;
      })
      .then(res => {
        console.log('reponse image:::', res);
        let result = JSON.parse(res.data);
        if (result.code == 200) {
          let send_file = {
            type: 1,
            file_id: result.data.media_id,
            file_name: result.data.name,
            ext: file_extension,
            size: file.fileSize,
            height: file.height,
            width: file.width,
            mime: file_type,
          };
          this.rootStore.messageStore.upload_image_process.process = 1;
          this.rootStore.socketIO.sendImageMessage(
            send_file,
            this.rootStore.messageStore.getChattingUser.id,
          );
        } else if (result.code == 400) {
          this.rootStore.messageStore.can_send_message = true;
          ToastAndroid.show(' ファイルサイズ > 10MB ', ToastAndroid.LONG);
        }

        setTimeout(() => {
          this.rootStore.messageStore.upload_image_process.show = false;
        }, 500);
      })
      .catch(() => {
        this.rootStore.messageStore.can_send_message = true;
        this.rootStore.messageStore.upload_image_process.show = false;
      });
  }

  @action sendMessageLocation(lat, long, accuracy) {
    console.log('[store] sendMessageLocation');

    let location = {
      lat: lat,
      long: long,
      accuracy: accuracy,
    };

    this.rootStore.socketIO.sendLocationMessage(
      location,
      this.rootStore.messageStore.getChattingUser.id,
    );
  }

  @action async userClickViewImage(media_id, msg_id, is_minus_point) {
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      id: media_id,
      token: this.rootStore.systemStore.login_token,
      size: 'large',
      thumbnail: false,
      stream: true,
      seq: msg_id,
    };

    this.rootStore.systemStore.image_popup.show = true;
    this.rootStore.systemStore.image_popup.loading = true;

    let response = await fetch(
      AppConfig.API_URL + 'media/stream' + createGetDataFromParamObject(params),
      {
        method: 'GET',
        headers: headers,
      },
    )
      .then(res => res)
      .catch(err => console.log('media/stream', err));

    console.log('media/stream success,', response);
    this.rootStore.systemStore.image_popup = {
      show: true,
      uri: response.url,
      screen: 'chat',
      loading: false,
    };
    if (is_minus_point) {
      this.rootStore.pointStore.total_point =
        this.rootStore.pointStore.getTotalPoint -
        this.rootStore.pointStore.getPointSettings.view_media_chat_image;
    }
    console.log(
      'userClickViewImage total_point',
      this.rootStore.pointStore.total_point,
    );
  }

  @action async userClickViewLocation(
    sender_id,
    msg_id,
    lat,
    long,
    is_minus_point,
  ) {
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      token: this.rootStore.systemStore.login_token,
      sender_id: sender_id,
      seq_id: msg_id,
    };

    this.rootStore.systemStore.map_popup.show = true;
    this.rootStore.systemStore.map_popup.loading = true;
    await requestGetAPI(headers, 'media/stream_map', params);
    this.rootStore.systemStore.map_popup.location = {
      latitude: lat * 1,
      longitude: long * 1,
      latitudeDelta: 0.0122,
      longitudeDelta: 0.0122,
    };

    this.rootStore.systemStore.map_popup.loading = false;
    if (is_minus_point) {
      this.rootStore.pointStore.total_point =
        this.rootStore.pointStore.getTotalPoint -
        this.rootStore.pointStore.getPointSettings.chat_view_location;
      console.log(
        'userClickViewLocation total_point',
        this.rootStore.pointStore.total_point,
      );
    }
  }
}
