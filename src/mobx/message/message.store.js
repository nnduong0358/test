import {observable, computed, action, decorate, toJS} from 'mobx';

export default class MessageStore {
  @observable upload_image_process = {
    show: false,
    process: 0,
  };

  @observable roster_list = {
    page: -1,
    limit: 10,
    data: [],
    no_more_data: false,
  };

  @observable campaign_list = {
    page: -1,
    limit: 10,
    data: [],
    no_more_data: false,
  };

  @observable message_chatting_list = {
    data: [],
    user: {},
    last_msg: null,
    typing: 0,
    no_more_history: false,
  };

  @observable notice_list = {
    page: -1,
    limit: 10,
    data: [],
    no_more_data: false,
  };

  @observable notice_details = {
    content: '',
    avatar: '',
  };

  @observable number_unread = {
    total_unread_campaign: 0,
    total_unread_message: 0,
    total_unread_notice: 0,
    total_unread_all: 0,
  };

  @observable is_in_chatting = false;

  @observable new_message_comming = false;

  @observable can_send_message = true;

  @computed get getRosterData() {
    return toJS(this.roster_list.data);
  }

  @computed get getNoticeData() {
    return toJS(this.notice_list.data);
  }

  @computed get getCampaignData() {
    return toJS(this.campaign_list.data);
  }

  @computed get getNoticeDetails() {
    return toJS(this.notice_details);
  }

  @computed get getChattingList() {
    return toJS(this.message_chatting_list.data);
  }

  @computed get getChattingUser() {
    return toJS(this.message_chatting_list.user);
  }

  @computed get isTargetUserTyping() {
    return this.message_chatting_list.typing == 1;
  }

  @computed get getUnread() {
    return toJS(this.number_unread);
  }
}
