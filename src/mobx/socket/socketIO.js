import NetInfo from '@react-native-community/netinfo';
import io from 'socket.io-client';
import AppConfig from '../../services/constants/app_config';
import {observable} from 'mobx';

export default class SocketIO {
  constructor(rootStore) {
    this._rootStore = rootStore;
  }

  @observable no_connection_server = false;
  @observable current_action = undefined;
  @observable login_at = new Date().getTime();

  // whene api login is ready -> process socket login
  initSocket(socket_jwt) {
    // connect to socket io server and set 'this' manager socket client
    this._socket_client = io(AppConfig.SOCKET_URL, {
      query: {
        token: socket_jwt,
        debug: 1,
      },
      port: 3125,
      //secure: true,
      // transports: ['websocket'], // you need to explicitly tell it to use websockets
      // reconnection:true, //allow reconnect
      // reconnectionDelay: 3000, //reconnect after 3s after lost connect
      // reconnectionAttempts: 10, //reconnect 10 time before send error connect
      // timeout: 2000, //when timeout -> event connect_error + connect_timeout
      // agent: false, // set true -> return timeout no error ???
      // upgrade: false,
      // rejectUnauthorized: null
    });

    // (custom event) error message from socketio server
    this._socket_client.on('error', error =>
      console.warn('[SOCKET] custom error message:', error),
    );

    // (system event) connect timeout
    this._socket_client.on('connect_timeout', error =>
      console.warn('[SOCKET] timeout message:', error),
    );

    // (system event) connect false
    this._socket_client.on('connect_error', error => {
      this.no_connection_server = true;

      NetInfo.fetch().then(state => {
        if (state.type == 'unknown' || state.type == 'none') {
          this._rootStore.systemStore.is_connected_internet = false;
          this._rootStore.systemStore.loading = false;
        }
      });
    });

    // (system event) reconnect success
    this._socket_client.on('reconnect', error => {
      this.no_connection_server = false;
      this._rootStore.systemStore.is_connected_internet = true;
      if (this.current_action) this.current_action.emit('refresh');
    });

    // (custom event) login success
    this._socket_client.once('Server-login-ok', id => {
      this._rootStore.systemStore.is_connected_internet = true;

      // check socket login user and api login user is matching
      if (id == this._rootStore.systemStore.getUserLoginData.id) {
        // setup chat event
        _setupSocketChattingEvent(this._rootStore, this._socket_client);
      } else {
        console.warn('[SOCKET] login user not match api login user!');
      }
    });
  }

  sendMessage(message_string, user_target_id) {
    console.log('[SOCKET - EMIT] sendMessage:', message_string, user_target_id);
    this._socket_client.emit('Client-chat-message', {
      msg: message_string,
      r_id: user_target_id,
    });
  }

  setReadMessage(message) {
    console.log('[SOCKET - EMIT] setReadMessage', message);
    this._socket_client.emit('Client-chat-receive-ok', {
      time: message.time,
      u_id: message.u_id,
    });
  }

  setOpenImageMessage(message) {
    console.log('[SOCKET - EMIT] setOpenImageMessage', message);
    this._socket_client.emit('Client-chat-open-ok', {
      msg_id: message.msg_id,
      r_id: message.r_id,
      u_id: message.u_id,
      time: message.time,
      showed: 1,
    });
  }

  typingMessage(user_target_id, typing_status = 0) {
    this._socket_client.emit('Client-chat-message-typing', {
      typing: typing_status,
      r_id: user_target_id,
    });
  }

  sendImageMessage(file, user_target_id) {
    this._socket_client.emit('Client-chat-image', {
      msg: '',
      r_id: user_target_id,
      file: file,
    });
  }

  sendLocationMessage(location, user_target_id) {
    this._socket_client.emit('Client-chat-location', {
      msg: '',
      r_id: user_target_id,
      location: location,
    });
  }

  sendScreenView(screen) {
    this._socket_client.emit('Client-change-screen', screen);
  }

  sendPaymentLog(log) {
    let payment_log = `user ${
      this._rootStore.systemStore.user_login_data.id
    } | ${this.login_at} -> ${log}`;
    this._socket_client.emit('Client-logs', {
      slug: 'payment',
      log: payment_log,
    });
  }
}

// register socketio chat event after success socketio login
const _setupSocketChattingEvent = (_rootStore, socket_client) => {
  socket_client.on('Server-message-is-read', message => {
    console.log('[SOCKET - ON] Server-message-is-read :', message);
    if (_rootStore.messageStore.getChattingUser.id == message.r_id) {
      let len_chatting = _rootStore.messageStore.getChattingList.length;
      for (var key = 0; key < len_chatting; key++) {
        // neu item[i] la tin nhan minh gui
        if (_rootStore.systemStore.user_login_data.id == message.u_id) {
          // neu item[i] chua doc
          if (_rootStore.messageStore.getChattingList[key].is_read != 0) {
            // set da doc
            _rootStore.messageStore.message_chatting_list.data[key].is_read = 0;
          }
          // neu gap tin nhan minh gui da dc xem thi dung
          else if (_rootStore.messageStore.getChattingList[key].is_read == 0) {
            break;
          }
        }
      }
    }
  });
  socket_client.on('Server-chat-receive', message => {
    console.log('[SOCKET - ON] Server-chat-receive', message);
    //check chatting status
    if (
      message.u_id == _rootStore.messageStore.getChattingUser.id &&
      _rootStore.messageStore.is_in_chatting
    ) {
      // user is online and open target user chatting screen -> update message in room
      // message.is_read = message.received;
      _rootStore.messageAction.updateNewMessage(message);
      // neu message toi cua nguoi khac khong phai minh gui thi goi socket cap nhat lai trang thai da doc cua message do
      if (
        message.u_id != _rootStore.systemStore.user_login_data.id &&
        _rootStore.systemStore.app_state_active
      ) {
        socket_client.emit('Client-chat-receive-ok', {
          time: message.time,
          u_id: message.u_id,
        });
      }
    } else {
      // notice incoming message
      console.log('[SOCKET] incoming message: ', message);
    }
  });

  socket_client.on('Server-chat-send-ok', message => {
    console.log('[SOCKET - ON] Server-chat-send-ok: ', message);
    // update message in room
    message.is_read = message.received;
    _rootStore.messageAction.updateNewMessage(message);
    _rootStore.messageStore.can_send_message = true;

    if (message.type == 'text') {
      // nếu k trong free chat thì trừ point
      if (
        _rootStore.userStore.user_profile.free_chat != '1' &&
        _rootStore.userStore.user_profile.unlimit_point != 1
      ) {
        _rootStore.pointStore.total_point =
          parseInt(_rootStore.pointStore.getTotalPoint) -
          parseInt(_rootStore.pointStore.getPointSettings.chat_message_text);
      }
    } else if (message.type == 'image') {
      _rootStore.pointStore.total_point =
        parseInt(_rootStore.pointStore.getTotalPoint) -
        parseInt(_rootStore.pointStore.getPointSettings.chat_media_image);
    } else {
      _rootStore.pointStore.total_point =
        parseInt(_rootStore.pointStore.getTotalPoint) -
        parseInt(_rootStore.pointStore.getPointSettings.chat_media_location);
    }
  });

  socket_client.on('Server-chat-message-typing', typing_data => {
    if (typing_data.u_id == _rootStore.messageStore.getChattingUser.id) {
      _rootStore.messageStore.message_chatting_list.typing = typing_data.typing;
      console.log('[SOCKET] target user update typing: ', typing_data);
    }
  });
};
