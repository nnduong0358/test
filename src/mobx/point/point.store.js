import {observable, computed, action, decorate, toJS} from 'mobx';

export default class PointStore {
  @observable total_point = 0;
  @observable point_settings = {};
  @observable unlimit_description =
    '4000Pt でお相手へのメッセージ送信が永久無料で行えます。';
  @observable point_data = {};
  @observable point_table = [];

  @computed get getTotalPoint() {
    return toJS(this.total_point);
  }

  @computed get getPointSettings() {
    return toJS(this.point_settings);
  }

  @computed get getPointData() {
    return toJS(this.point_data);
  }

  @computed get getPointTableData() {
    return toJS(this.point_table);
  }
}
