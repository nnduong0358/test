import {Alert, Platform} from 'react-native';
import {observable, computed, action, decorate, toJS} from 'mobx';
import {requestGetAPI, requestPostAPI} from '../../services/request_api';
import * as Helper from '../../services/helpers';
import AppConfig from '../../services/constants/app_config';
import AsyncStorage from '@react-native-community/async-storage';
import RNIap from 'react-native-iap';

export default class PointAction {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  async getPointFee() {
    try {
      const data = await requestGetAPI(
        this.rootStore.systemStore.getApiHeader,
        'point/fee',
        {token: this.rootStore.systemStore.login_token},
      );
      this.rootStore.pointStore.total_point = parseInt(data.data.balance);
      this.rootStore.pointStore.unlimit_description =
        data.data.unlimit_description;
      let settings = {};
      data.data.result.forEach(point => {
        settings[point.slug] = point.point;
      });
      this.rootStore.pointStore.point_settings = settings;
    } catch (err) {
      console.log('getPointFee err', err);
    }
  }

  checkUserEnoughPoint(type) {
    let is_enough_point = true;
    let total_point = this.rootStore.pointStore.getTotalPoint;
    let point_settings = this.rootStore.pointStore.getPointSettings;

    switch (type) {
      case 'view_profile':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.view_profile);
        break;
      case 'view_image_profile_large':
        is_enough_point =
          parseInt(total_point) >=
          parseInt(point_settings.view_image_profile_large);
        break;
      case 'view_image_profile_large_1':
        is_enough_point =
          parseInt(total_point) >=
          parseInt(point_settings.view_image_profile_large_1);
        break;
      case 'view_image_profile_large_2':
        is_enough_point =
          parseInt(total_point) >=
          parseInt(point_settings.view_image_profile_large_2);
        break;
      case 'view_image_profile_large_3':
        is_enough_point =
          parseInt(total_point) >=
          parseInt(point_settings.view_image_profile_large_3);
        break;
      case 'chat_message_text':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.chat_message_text);
        break;
      case 'chat_media_image':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.chat_media_image);
        break;
      case 'view_media_chat_image':
        is_enough_point =
          parseInt(total_point) >=
          parseInt(point_settings.view_media_chat_image);
        break;
      case 'chat_media_location':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.chat_media_location);
        break;
      case 'chat_view_location':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.chat_view_location);
        break;
      case 'discuss_view':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.discuss_view);
        break;
      case 'discuss_post_image':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.discuss_post_image);
        break;
      case 'discuss_post':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.discuss_post);
        break;
      case 'view_media_discuss_image':
        is_enough_point =
          parseInt(total_point) >=
          parseInt(point_settings.view_media_discuss_image);
        break;
      case 'unlimit_point':
        is_enough_point =
          parseInt(total_point) >= parseInt(point_settings.unlimit_point);
        break;
    }

    return is_enough_point;
  }

  @action async postUnlimitPoint(id, user_code) {
    this.rootStore.systemStore.loading = true;
    let params = {
      token: this.rootStore.systemStore.login_token,
      friend_code: user_code,
      friend_id: id,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestPostAPI(headers, 'point/unlimit', params);
    this.rootStore.systemStore.loading = false;
    this.rootStore.userAction.getUserProfile(id, '', 1, false);
    Alert.alert(
      AppConfig.APP_NAME,
      '\n' + response.message,
      [{text: 'OK', onPress: () => console.log('Cancel')}],
      {cancelable: false},
    );

    if (response?.code == 200) {
      this.rootStore.pointStore.total_point =
        this.rootStore.pointStore.getTotalPoint -
        this.rootStore.pointStore.getPointSettings.unlimit_point;
    }
  }

  @action async getPoint() {
    this.rootStore.systemStore.loading = true;
    let params = {
      token: this.rootStore.systemStore.login_token,
      type:
        Platform.OS === 'android'
          ? AppConfig.TYPE_PAYMENT.Android
          : AppConfig.TYPE_PAYMENT.iOS,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestGetAPI(headers, 'point/package', params);
    if (response?.code == 200) {
      this.rootStore.pointStore.point_data = response.data;
      // set point item IAP
      let itemSkus = [];
      response.data.result.map(item => {
        itemSkus.push(item.identifier);
      });
      console.log('itemSkus', itemSkus);
      let valid_products = await RNIap.getProducts(itemSkus);
      console.log('valid_products', valid_products);
    }
    this.rootStore.systemStore.loading = false;
  }

  @action async checkAvailablePurchases() {
    this.rootStore.systemStore.loading = true;
    try {
      const purchases = await RNIap.getAvailablePurchases();
      console.log('available purchase:::', purchases);

      if (purchases && purchases.length > 0) {
        for (let purchaseItem of purchases) {
          if (purchaseItem.transactionReceipt) {
            this.rootStore.socketIO.sendPaymentLog(
              `restore purchase:: ${JSON.stringify(purchaseItem)}`,
            );
            await this.finishPurchase(purchaseItem, true);
          }
        }
      } else
        AsyncStorage.removeItem('order_payment_id').catch(error =>
          console.log(error),
        );
      this.rootStore.systemStore.loading = false;
    } catch (err) {
      console.log('checkAvailablePurchases err:::', err);
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async finishPurchase(purchase, isRestored = false) {
    this.rootStore.socketIO.sendPaymentLog(
      `validate ${isRestored ? 'restore' : ''} purchase: ${JSON.stringify(
        purchase,
      )}`,
    );

    this.rootStore.systemStore.loading = true;
    let params = {
      token: this.rootStore.systemStore.login_token,
      'receipt-data': JSON.stringify(purchase),
    };
    let headers = this.rootStore.systemStore.getApiHeader;

    let data = await requestPostAPI(
      headers,
      'payment/validate_receipt',
      params,
    );
    this.rootStore.socketIO.sendPaymentLog(
      `server respond:: ${JSON.stringify(data)}`,
    );
    if (data?.code == 200 && data.data.point > 0) {
      this.rootStore.pointStore.total_point = data.data.balance;

      Alert.alert(
        isRestored ? '購入情報を復元しました。' : 'ポイントを購入しました',
        `\n購入したポイント：${data.data.point}\n合計ポイント：${
          data.data.balance
        }`,
        [{text: '閉じる', onPress: () => console.log('Payment success')}],
      );

      // Finish a purchase both iOS and Android
      let finishPurchase = await RNIap.finishTransaction(purchase, true); // true is mean consumable , false is not consumable
      await Helper.removeOrderPaymentID(purchase.transactionId); // delete order_id khi validate-comsume success
      this.rootStore.socketIO.sendPaymentLog(
        `finish ${isRestored ? 'restore' : ''} purchase:: ${JSON.stringify(
          finishPurchase,
        )}`,
      );

      // Adjust tracking event Purchase
      Helper.callEventAdjust('PAYMENT');
    }
    this.rootStore.systemStore.loading = false;
  }

  @action async buyPoint(identifier, amount) {
    this.rootStore.systemStore.loading = true;
    this.rootStore.socketIO.sendPaymentLog(`click buy point: ${identifier}`);

    try {
      await AsyncStorage.setItem('itemPurchase', amount.toString());
      await RNIap.requestPurchase(identifier, false);
      this.rootStore.socketIO.sendPaymentLog(
        `request purchase with store: ${identifier}`,
      );
    } catch (err) {
      console.log('request purchase err:::', err);
      this.rootStore.systemStore.loading = false;
      this.rootStore.socketIO.sendPaymentLog(
        `request purchase with store failed: ${identifier} || error: ${err}`,
      );
    }
  }

  @action async getPointTable() {
    let params = {token: this.rootStore.systemStore.login_token};
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestGetAPI(headers, 'point/point_setting', params);

    if (response?.code == 200) {
      this.rootStore.pointStore.point_table = response.data;
    }
  }
}
