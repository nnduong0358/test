import {Platform} from 'react-native';
import {observable, computed, action, decorate, toJS} from 'mobx';
import AppConfig from '../../services/constants/app_config';
import DeviceInfo from 'react-native-device-info';

export default class SystemStore {
  @observable pwd_login = '';
  @observable loading = false;
  @observable api_header = {
    'X-DEVICE-ID': DeviceInfo.getUniqueId(),
    'X-OS-TYPE': Platform.OS,
    'X-OS-VERSION': DeviceInfo.getSystemVersion(),
    'X-APP-VERSION': DeviceInfo.getVersion(),
    'X-API-ID': AppConfig.API_ID,
    'X-API-KEY': AppConfig.API_KEY,
    'X-PUSH-TOKEN': '',
    'X-CARRIER-NAME': '',
    'X-CARRIER-CODE': '',
  };
  @observable server_state = {};
  @observable is_connected_internet = true;
  @observable user_login_data = {};
  @observable login_token = '';
  @observable register_data = {
    carriername: '',
    mobilenetworkcode: '',
    name: '',
    area: '',
    city: '',
    gender: '',
    age: '',
    checked_age_18: true,
    checked_term: true,
    password: this.randomPassword(),
    cities: [],
  };
  @observable nav_list = {
    last_screen: [],
    current_screen: '',
  };
  @observable header_app = {
    back_button: undefined,
    back_to: undefined,
    refresh_button: undefined,
    refresh_action: undefined,
    search_button: undefined,
    search_action: undefined,
    cancel_button: undefined,
    message_button: undefined,
    message_action: undefined,

    title: '',
  };
  @observable ng_words_alert = false;
  @observable image_popup = {
    show: false,
    screen: '',
    uri: '',
    loading: false,
  };
  @observable map_popup = {
    show: false,
    loading: false,
    location: {},
  };

  @observable app_state_active = true;

  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  randomPassword = () => {
    return Math.random()
      .toString(36)
      .slice(-8);
  };

  @computed get getApiHeader() {
    return toJS(this.api_header);
  }

  @computed get getUserLoginData() {
    return toJS(this.user_login_data);
  }

  @computed get getLoading() {
    return toJS(this.loading);
  }

  @action setLoading(value) {
    this.loading = value;
  }
}

// decorate(SystemStore, {
//   api_header: observable,
//   getApiHeader: computed,
//   server_state: observable,
// });

// export default new SystemStore();
