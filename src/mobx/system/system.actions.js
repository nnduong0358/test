import {
  PermissionsAndroid,
  Alert,
  BackHandler,
  Keyboard,
  ToastAndroid,
} from 'react-native';
import {observable, computed, action, decorate, toJS} from 'mobx';
import {requestGetAPI, requestPostAPI} from '../../services/request_api';
import * as Helper from '../../services/helpers';
import CarrierInfo from 'react-native-carrier-info';
import RNFetchBlob from 'rn-fetch-blob';
import DeviceInfo from 'react-native-device-info';
import AppConfig from '../../services/constants/app_config';
import striptags from 'striptags';
import NavigatorService from '../../services/navigator/navigation_services';
import * as ShowAlert from '../../services/show_alert';

export default class SystemAction {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  async startApp() {
    Helper.callEventAdjust('INSTALL');
    await this.getCarrierInfo();
    //await requestGetAPI(this.rootStore.systemStore.getApiHeader, 'metadata/push_token');
    await this.getMetadata();
  }

  async callRosterApi() {
    let params = {
      page: 0,
      limit: 10,
      token: this.rootStore.systemStore.login_token,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestGetAPI(headers, 'roster/get', params);

    if (response?.code == 200) {
      let result = response.data;
      if (result.total_unread_all != undefined) {
        this.rootStore.messageStore.number_unread = {
          total_unread_campaign: result.total_unread_campaign,
          total_unread_message: result.total_unread_message,
          total_unread_notice: result.total_unread_notice,
          total_unread_all: result.total_unread_all,
        };
      }
      //merge new data with old data = current roster_list
      if (result.result != undefined) {
        if (this.rootStore.messageStore.roster_list.page == 0) {
          this.rootStore.messageStore.roster_list.data = result.result;
        } else {
          this.rootStore.messageStore.roster_list.data = this.rootStore.messageStore.roster_list.data.concat(
            result.result,
          );
        }
        this.rootStore.messageStore.roster_list.no_more_data =
          result.result.length < this.rootStore.messageStore.roster_list.limit;
      }
    }
  }

  async getCarrierInfo() {
    try {
      const carrier_name = await CarrierInfo.carrierName();
      this.rootStore.systemStore.api_header['X-CARRIER-NAME'] = carrier_name;
      this.rootStore.systemStore.register_data.carriername = carrier_name;
      const carrier_code = await CarrierInfo.mobileNetworkCode();
      this.rootStore.systemStore.api_header['X-CARRIER-CODE'] = carrier_code;
      this.rootStore.systemStore.register_data.mobilenetworkcode = carrier_code;
    } catch (err) {
      console.log('get carrier err:', err);
    }
  }

  resetLastScreen(screen) {
    this.rootStore.systemStore.nav_list.last_screen = [screen];
  }

  saveLastScreen(screen) {
    const last_screen = toJS(this.rootStore.systemStore.nav_list.last_screen);
    if (!last_screen.includes(screen)) {
      this.rootStore.systemStore.nav_list.last_screen.push(screen);
    }
  }

  @action goBack() {
    Keyboard.dismiss();
    if (this.rootStore.systemStore.ng_words_alert) {
      Alert.alert(
        '',
        '投稿内容にNGワードが含まれているため、書き込めません。もう一度入力してください。',
        [{text: 'OK', onPress: () => console.log('Cancel Pressed')}],
        {cancelable: false},
      );
    } else {
      const last_screen = toJS(this.rootStore.systemStore.nav_list.last_screen);
      let last_index = last_screen.length;
      if (
        Platform.OS === 'android' &&
        (!this.rootStore.systemStore.header_app.back_button &&
          !this.rootStore.systemStore.header_app.cancel_button)
      ) {
        Alert.alert('', 'アプリを終了してよろしいでしょうか？', [
          {
            text: 'いいえ',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'はい', onPress: () => BackHandler.exitApp()},
        ]);
      } else {
        console.log('goBack', last_screen);
        NavigatorService.navigate(last_screen[last_index - 1], {reload: true});
        if (last_index >= 2) {
          this.rootStore.systemStore.nav_list.last_screen.pop();
        }
      }
    }
  }

  @action comfirmCancelPopup() {
    if (this.rootStore.myPageStore.change_edit_profile.result == true) {
      Alert.alert(
        '',
        '入力内容を破棄しますか？',
        [
          {
            text: 'はい',
            onPress: () => {
              this.rootStore.myPageStore.edit_image_status = false;
              this.rootStore.myPageStore.change_edit_profile.result = false;
              this.goBack();
            },
          },
          {
            text: 'いいえ',
            onPress: () => console.log('Cancel'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      this.goBack();
    }
  }

  async checkUserRegister() {
    const user_data = {
      user_code: '',
      password: '',
    };

    const dirs = RNFetchBlob.fs.dirs;
    const folder_path = dirs.SDCardDir + '/' + DeviceInfo.getBundleId() + '/';
    const file_path = folder_path + 'user';

    const is_granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: '',
        message: AppConfig.APP_NAME + 'にストレージの読み取りを許可しますか？',
      },
    );
    if (
      is_granted === PermissionsAndroid.RESULTS.GRANTED ||
      is_granted === true
    ) {
      const is_file = await RNFetchBlob.fs.exists(file_path);
      if (is_file) {
        const data = await RNFetchBlob.fs.readFile(file_path, 'base64');
        const user = RNFetchBlob.base64.decode(data);
        console.log(user);
        const array_data = user.split(' ');

        this.rootStore.systemStore.api_header['X-DEVICE-ID'] = array_data[0];
        user_data.user_code = array_data[1];
        user_data.password = array_data[2];
        this.rootStore.systemStore.pwd_login = array_data[2];
      }
    } else {
      ShowAlert.alertRequestPermissionStorage();
    }
    this.userLogin(user_data);
  }

  async getMetadata() {
    try {
      const response = await requestGetAPI(
        this.rootStore.systemStore.getApiHeader,
        'metadata',
      );
      this.rootStore.systemStore.is_connected_internet = true;

      if (response.code == 200) {
        if (response.data.function_off.force_update_version) {
          ShowAlert.alertForceUpdate(
            response.data.function_off.force_update_title,
            response.data.function_off.force_update_description,
            response.data.function_off.new_version_url,
          );
        } else {
          this.rootStore.systemStore.server_state = response.data;
          this.checkUserRegister();
        }
      } else throw 'getMetadata failed';
    } catch (err) {
      console.log('getMetadata err: ', err);
      this.rootStore.systemStore.is_connected_internet = false;
      setTimeout(() => {
        this.getMetadata();
      }, 2000);
    }
  }

  @action async userLogin(user_data) {
    try {
      const login_data = await requestPostAPI(
        this.rootStore.systemStore.getApiHeader,
        'login/index',
        user_data,
      );
      this.rootStore.systemStore.is_connected_internet = true;

      if (login_data.code == 200) {
        this.rootStore.systemStore.user_login_data = login_data.data;
        this.rootStore.systemStore.login_token = login_data.data.token;
        //set point
        await this.rootStore.pointAction.getPointFee();
        if (login_data.data.is_bonus) {
          Alert.alert(
            login_data.data.bonus_title,
            '\n' + login_data.data.bonus_body,
            [{text: 'OK', onPress: () => console.log('OK')}],
            {cancelable: false},
          );
        }
        // initSocket
        if (login_data.data.socket_jwt) {
          this.rootStore.socketIO.initSocket(login_data.data.socket_jwt);
          await this.rootStore.messageAction.getRosterList();
          this.resetLastScreen('UserList');
          NavigatorService.navigate('mainApp');
        }
      } else if (login_data.code == 400) {
        NavigatorService.navigate('Register');
      } else if (login_data.code == 550) {
        Alert.alert(
          '',
          striptags(login_data.message, [], '\n'),
          [
            {
              text: 'メールを送信する',
              onPress: () => ShowAlert.linkingHanle(login_data.message),
            },
            {
              text: '閉じる',
              onPress: () => BackHandler.exitApp(),
              style: 'cancel',
            },
          ],
          {cancelable: false},
        );
      }
    } catch (err) {
      console.log('userLogin err: ', err);
      this.rootStore.systemStore.is_connected_internet = false;
      setTimeout(() => {
        this.userLogin(user_data);
      }, 2000);
    }
  }

  async getCities(value) {
    if (value === '') return [];
    else {
      try {
        const data = await requestGetAPI(
          this.rootStore.systemStore.getApiHeader,
          'region/Cities?area_id=' + value,
        );
        if (data.code == 200) return data.data;
        return [];
      } catch (err) {
        console.log('getCities err:', err);
        return [];
      }
    }
  }

  async registerUser(params) {
    try {
      const response = await requestPostAPI(
        this.rootStore.systemStore.getApiHeader,
        'register/index',
        params,
      );
      if (response.code == 200) {
        Helper.callEventAdjust('REGISTER');

        this.rootStore.systemStore.pwd_login = params.password;

        // save data login
        const dirs = RNFetchBlob.fs.dirs;
        const folder_path =
          dirs.SDCardDir + '/' + DeviceInfo.getBundleId() + '/';
        const file_path = folder_path + 'user';
        console.log('file_path', file_path);
        const data_save =
          this.rootStore.systemStore.api_header['X-DEVICE-ID'] +
          ' ' +
          response.data.user_code +
          ' ' +
          params.password;
        const base64_data_save = RNFetchBlob.base64.encode(data_save);
        console.log('base64_data_save', base64_data_save);
        const user_data = {
          user_code: response.data.user_code,
          password: params.password,
        };
        RNFetchBlob.fs
          .mkdir(folder_path)
          .then(mkdir => {
            console.log('mkdir', mkdir);
            this.saveDataLogin(file_path, base64_data_save);
          })
          .catch(err => {
            console.log('mkdir err', err);
            this.saveDataLogin(file_path, base64_data_save);
          });

        this.userLogin(user_data);
      }
    } catch (err) {
      console.log('registerUser err', err);
    }
  }

  saveDataLogin(file_path, base64_data_save) {
    RNFetchBlob.fs
      .exists(file_path)
      .then(is_file => {
        console.log('is_file', is_file);
        if (is_file) {
          RNFetchBlob.fs.writeFile(file_path, base64_data_save, 'base64');
        } else {
          RNFetchBlob.fs
            .createFile(file_path, base64_data_save, 'base64')
            .catch(err => {
              console.log('createFile err', err);
            });
        }
      })
      .catch(err => {
        console.log('Check file exist error: ', err);
      });
  }

  @action async prepareChangeDevice(data_change) {
    this.rootStore.systemStore.loading = true;
    let headers = this.rootStore.systemStore.getApiHeader;
    let params = {
      email: data_change.email,
      password: data_change.password,
      token: this.rootStore.systemStore.login_token,
      device_password: this.rootStore.systemStore.pwd_login,
    };
    try {
      let response = await requestPostAPI(
        headers,
        'user/device/prepare',
        params,
      );
      this.rootStore.systemStore.loading = false;
      Alert.alert(
        '',
        response.message,
        [
          {
            text: 'OK',
            onPress: () => {
              if (response.code == 200) {
                BackHandler.exitApp();
              } else {
                console.log('F');
              }
            },
          },
        ],
        {cancelable: false},
      );
    } catch (err) {
      console.log('prepareChangeDevice err', err);
      this.rootStore.systemStore.loading = false;
    }
  }

  @action async tranferChangeDevice(email, password) {
    let params = {
      email: email,
      password: password,
    };
    let headers = this.rootStore.systemStore.getApiHeader;
    let response = await requestPostAPI(
      headers,
      'user/device/transfer',
      params,
    );

    if (response?.data) {
      ToastAndroid.show(response.message, ToastAndroid.LONG);
      let user_data = {
        user_code: response.data.user_info.user_code,
        password: response.data.user_info.device_password,
      };

      // set data return to app & move to login
      const dirs = RNFetchBlob.fs.dirs;
      const folder_path = dirs.SDCardDir + '/' + DeviceInfo.getBundleId() + '/';
      const file_path = folder_path + 'user';
      const data_save =
        this.rootStore.systemStore.api_header['X-DEVICE-ID'] +
        ' ' +
        response.data.user_info.user_code +
        ' ' +
        response.data.user_info.device_password;
      const base64_data_save = RNFetchBlob.base64.encode(data_save);

      RNFetchBlob.fs
        .mkdir(folder_path)
        .then(mkdir => {
          console.log('mkdir', mkdir);
          this.saveDataLogin(file_path, base64_data_save);
        })
        .catch(err => {
          console.log('mkdir err', err);
          this.saveDataLogin(file_path, base64_data_save);
        });

      this.userLogin(user_data);
    } else {
      // fail
      Alert.alert(
        '',
        response.message,
        [
          {
            text: 'OK',
            onPress: () => {
              console.log('Fail');
            },
          },
        ],
        {cancelable: false},
      );
    }
  }
}
